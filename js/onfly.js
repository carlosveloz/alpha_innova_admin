(function($) {
  root = $("#ruta").val()

  $.fn.navbarTitle = function(menu){ 
    $("#navbar-title").text(menu);
  }

  $.fn.navbarBtn = function(html){ 
    $("#navbar-options-btn").html(html);
  }

  $.fn.sidebarContent = function(type){ 
    if (type == "hide") {
      $(".nav-settings").attr('style','display:none !important');
    }else{

    }
  }







  
  $('body').on('submit', '.form-ajax', function(){

    action = $(this).attr("action");
    notif = $(this).attr("notif");
    reset = $(this).attr("reset");

    formData = new FormData($(this)[0]);

    form = $(this);
    event.preventDefault();

    $.ajax({
     cache:false,
     type: "POST",
     url: root+"/php/forms/"+action,
     data: formData,
     contentType: false,
     processData: false,
     success:function(result){
      console.log(result);
      if (result.trim() == "ok") {
        showSuccessToast(notif);

        if (reset == 1) {
          $(form).trigger("reset");
        }

        doWhenSubmited(action);

      }else{
        showWarningToast(result);
      }
    },
    error: function(result){
     alert("No se completo el proceso");
   }
 });

  });



  $('body').on('click', '.btn-status', function(){

    var id = $(this).attr('data-id');
    var type = $(this).attr('data-type');

    var the_btn = $(this);

    $.ajax({
      cache:false,
      type: "post",
            //dataType:'json',
            url: root+"/php/forms/general/change_status.php",
            data:{
              'id' : id,
              'type' : type
            },
            success:function(result){
              console.log(result);
              if (result == "ok") {
                showSuccessToast("Registro actualizado con éxito.");
                $(the_btn).addClass("disabled");
                
              } else {
                // console.log(result);
                showDangerToast();
              };
            },

            error: function(data_er){
              alert("No se completo el proceso");

            }
          });

  });




  $('body').on('click', '.get_data', function(){

    var table = $(this).attr('data-table');
    var code = $(this).attr('data-code');


    $.ajax({
      cache:false,
      type: "post",
      dataType:'json',
      url: root+"/php/forms/general/get_data.php",
      data:{
        'table' : table,
        'code' : code
      },
      success:function(result){
        console.log(result);

        doWhenGetData(table,result);

      },

      error: function(data_er){
        alert("No se completo el proceso");

      }
    });

  });





  $('body').on('click', '.delete_data', function(){

    var table = $(this).attr('data-table');
    var code = $(this).attr('data-code');


    $.ajax({
      cache:false,
      type: "post",
      dataType:'json',
      url: root+"/php/forms/general/delete_data.php",
      data:{
        'table' : table,
        'code' : code
      },
      success:function(result){
        // console.log(result);
        doWhenDeleteData(table,result);

      },

      error: function(data_er){
        console.log("ERROR "+data_er);

        alert("No se completo el proceso");

      }
    });

  });





  
  $('body').on('click', '.toggler', function(){

    show = $(this).attr("data-toggle");
    datathis = $(this).attr("data-this");
    datathat = $(this).attr("data-that");

    // if (datathis == "disabled") {
    //   $(this).addClass("toggler-disabled");
    // }

    // if (datathat) {
    //   $(datathat).removeClass("toggler-disabled");
    // }


    $(".toggle").hide();
    $(show).fadeIn(1000);

  });

  
  $('body').on('click', '.sidebar-bg-options', function(){

    theme = $(this).attr("id");

    $.ajax({
      cache:false,
      type: "post",
            //dataType:'json',
            url: root+"/php/forms/general/edit_estilo_sidebar.php",
            data:{
              'theme' : theme
            },
            success:function(result){
              console.log(result);

            },

            error: function(data_er){
              alert("No se completo el proceso");

            }
          });



  });

  
  $('body').on('click', '.tiles', function(){

    theme = $(this).attr("data-theme");

    $.ajax({
      cache:false,
      type: "post",
            //dataType:'json',
            url: root+"/php/forms/general/edit_estilo_navbar.php",
            data:{
              'theme' : theme
            },
            success:function(result){
              console.log(result);

            },

            error: function(data_er){
              alert("No se completo el proceso");

            }
          });



  });


  $('body').on('click', '.reset-img', function(){

    src = $(this).attr("data-src");
    target = $(this).attr("data-target");

    $(target)
    .attr('src', src)
    .width(100)
    .height(100);


  });

  ///////////// TAREAS






  $('body').on('click', '#add-task', function(){


    var tarea = $("#input-tarea").val();
    var usuario_id = 7;

    if (tarea == "" || usuario_id == "") {
      $("#input-tarea").focus();
      return false;
    }

    $.ajax({
      cache:false,
      type: "post",
            //dataType:'json',
            url: root+"/php/forms/general/add_tarea.php",
            data:{
              'usuario_id' : usuario_id,
              'tarea' : tarea
            },
            success:function(result){

              $("#input-tarea").val("");

              html ="";

              html += '<li class="" id="tarea_'+result+'">'
              html += '   <div class="form-check">'
              html += '     <label class="form-check-label">'
              html += '       <input class="checkbox" type="checkbox" onchange="doneTarea('+result+')">'
              html +=  tarea
              html += '     </label>'
              html += '   </div>'
              html += '   <i class="remove mdi mdi-close-circle-outline delete-tarea" data-id="'+result+'" onclick="deleteTarea('+result+')"></i>'
              html += '</li>'

              $(".todo-list").append(html);

              console.log(result);
            },

            error: function(data_er){
              alert("No se completo el proceso");

            }
          });


  });

//   $('body').on('click', '.delete-tarea', function(){

//     var id = $(this).attr("data-id");
// alert(id);
// return false;
//     $.ajax({
//       cache:false,
//       type: "post",
//             //dataType:'json',
//             url: root+"/php/forms/general/delete_tarea.php",
//             data:{
//               'id' : id
//             },
//             success:function(result){
//               $("#tarea_"+id).fadeOut();
//               console.log(result);
//             },

//             error: function(data_er){
//               alert("No se completo el proceso");

//             }
//           });


//   });


























$(document).on('change', '.file_upload', function() {

  var the_id = $(this).attr('data-id');
  var target = $(this).attr('data-target');
  var action = $(this).attr('data-action');
  var f_this = this;
  var filename = $(f_this).val().split('\\').pop();
  var limit = parseInt($(f_this).attr("data-limit"));

  if (limit == null || isNaN(limit)) {
    limit = 24;
  }


  limit_msg = limit+1;
    /*alert(limit);

    return false;*/
    
    if ($(this)[0].files[0]) {
      //console.log($(this)[0].files);
      console.log($('.input_gallery').length);
      uploading_files = 0;
      $.each($(this)[0].files, function(index, value) {

        uploading_files++;


        var dataForm = new FormData();
            //dataForm.append('file_image', $(this)[0].files[index]);
            dataForm.append('file_image', value);
            dataForm.append('the_id', uploading_files);


            $.ajax({
              type: 'post',
              url: root+"/php/forms/"+action,
              data: dataForm,
              // dataType: 'json',
              cache: false,
              contentType: false,
              processData: false,
              beforeSend: function() {

              },
              error: function(result) {
                console.log(result);

              },
              success: function(result) {

                // console.log(result);

                $(target).append(result);
                $(".file_upload").val('');

                if (result) {
                  //$('#' + the_id).val(response.target);
                  if ($('.input_gallery').length > limit) {
                    alert("limite alcanzado");
                  }
                  else {
                    // $('#upload_files').append(response.buffer);

                  }
                }
                else {
                  alert(response.response);
                }
                
                $(f_this).replaceWith($(f_this).clone());
                

              }
            });


          });
    }
  });








$('body').on('change', '#lista_estados', function(){

  var estado_id = $(this).val();
      // alert(estado_id);
      $.ajax({
        cache:false,
        type: "post",
        //dataType:'json',
        url: root+"/php/forms/general/get_municipios.php",
        data:{
          'estado_id' : estado_id
        },
        success:function(result){
          console.log(result);
          $("#lista_municipios").html(result);
        },

        error: function(data_er){
          alert("No se completo el proceso");

        }
      });

    });







$('body').on('change', '#select_plaza', function(){

  var plaza_id = $(this).val();
      // alert(estado_id);
      $.ajax({
        cache:false,
        type: "post",
        //dataType:'json',
        url: root+"/php/forms/general/get_autores_by_plaza.php",
        data:{
          'plaza_id' : plaza_id
        },
        success:function(result){
          console.log(result);
          $("#select_autor").html(result);
        },

        error: function(data_er){
          alert("No se completo el proceso");

        }
      });

    });












})(jQuery);


function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    var target = $(input).attr("data-target");

            // alert(target);

            reader.onload = function (e) {
              $('#'+target)
              .attr('src', e.target.result)
              .width(100)
              .height(100);
            };

            reader.readAsDataURL(input.files[0]);
          }
        }






//What to do when submited form

doWhenSubmited = function(action){


  if (action == "usuarios/add_user.php") {
   $("#foto_avatar").attr("src", "images/avatars/avatar.png");
   $("#add_user_modal").modal("hide");
 }

 if (action == "usuarios/edit_password_by_user.php") {
  $("#edit_password_modal").modal("hide"); 
}

if (action == "usuarios/add_plaza.php") {
  $("#add_plaza_modal").modal("hide"); 
  location.reload();
}

if (action == "noticias/edit_etiqueta.php") {
  $("#edit_etiqueta_modal").modal("hide"); 
}


if (action == "empresas/edit_giro.php") {
  $("#edit_giro_modal").modal("hide"); 
}

if (action == "empresas/add_empresa.php") {
  setTimeout(
    function() 
    {
      location.reload();
    }, 2000); 
}

if (action == "empresas/edit_empresa.php") {
  setTimeout(
    function() 
    {
      location.reload();
    }, 2000); 
}

};


doWhenGetData = function(table,$result){

  if (table == "etiquetas") {
   $("#edit_nombre_es").val($result['nombre_es']);
   $("#edit_nombre_en").val($result['nombre_en']);
   $("#edit_code").val($result['code']);
   $("#edit_etiqueta_modal").modal("show");

 }


 if (table == "empresas_giros") {
   $("#edit_nombre").val($result['nombre']);
   // $("#edit_nombre_en").val($result['nombre_en']);
   $("#edit_code").val($result['code']);
   $("#edit_img").attr('src', "../../images/empresas/giros/"+$result['imagen'])
   $("#edit_giro_modal").modal("show");

 }

 if (table == "autores") {
      // alert("ok");
      $("#edit_nombre").val($result['nombre']);
   // $("#edit_nombre_en").val($result['nombre_en']);
   $("#edit_code").val($result['code']);
   $("#edit_foto_avatar").attr('src', "../../images/autores/"+$result['avatar'])
   $("#edit_autor_modal").modal("show");


   var select = [];
   // $('#edit_plazas option[value="5"]').attr('selected', 'selected');
   $.each($result['plazas'], function(index, value) {
    select.push("'"+value['plaza_id']+"'");
  });

alert(select);

$('#edit_plazas').select2(); $('#edit_plazas').val(select).trigger('change');
  

 }



};

doWhenDeleteData = function(table,$result){

  if (table == "notas_imagenes" || table == "empresas_imagenes") {
   $("#img_"+$result['code']).hide();

 }

 if (table == "versiones_impresas") {
   $("#vimp_"+$result['code']).fadeOut();

 }


};



deactivateUser = function(n){
  $("#d_"+n).hide();
  $("#"+n).appendTo("#usuarios-inactivos");
};

activateUser = function(n){
  $("#a_"+n).hide();
  $("#"+n).appendTo("#usuarios-activos");
};


// OTHER FUNCTIONS

deleteTarea = function(id){

  $.ajax({
    cache:false,
    type: "post",
            //dataType:'json',
            url: root+"/php/forms/general/delete_tarea.php",
            data:{
              'id' : id
            },
            success:function(result){
              $("#tarea_"+id).fadeOut();
              console.log(result);
            },

            error: function(data_er){
              alert("No se completo el proceso");

            }
          });

};

doneTarea = function(id){


  $.ajax({
    cache:false,
    type: "post",
            //dataType:'json',
            url: root+"/php/forms/general/done_tarea.php",
            data:{
              'id' : id
            },
            success:function(result){
              console.log(result);
            },

            error: function(data_er){
              alert("No se completo el proceso");

            }
          });

};