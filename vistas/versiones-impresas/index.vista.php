

<script>
  $(document).ready(function(){

    $.fn.navbarTitle("Versiones Impresas");

    html = $("#navbar-html").html()
    $.fn.navbarBtn(html);

    // $.fn.sidebarContent("hide");

  });
</script>

<div id="navbar-html" style="display: none;">
  <ul class="navbar-nav">
    <li class="nav-item dropdown d-none d-lg-flex">
      <a class="nav-link dropdown-toggle nav-btn" id="actionDropdown" href="#" data-toggle="dropdown">
        <span class="btn">Opciones</span>
      </a>
      <div class="dropdown-menu navbar-dropdown dropdown-left" aria-labelledby="actionDropdown">

        <a class="dropdown-item" data-toggle="modal" data-target="#add_version_modal">
          <i class="ti-book text-primary"></i>
          Agregar Version Impresa
        </a>



      </div>
    </li>
  </ul>
</div>





<div class="content-wrapper">
  <div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">Plazas</h4>
          <div class="accordion" id="accordion" role="tablist">
            
            <?php foreach ($plazas as $key => $value): ?>
              
            <div class="card">
              <div class="card-header" role="tab" id="headingThree">
                <h6 class="mb-0">
                  <a class="collapsed" data-toggle="collapse" href="#<?php echo $value['slug'] ?>" aria-expanded="false" aria-controls="<?php echo $value['slug'] ?>">
                    <?php echo $value['nombre'] ?>
                  </a>
                </h6>
              </div>
              <div id="<?php echo $value['slug'] ?>" class="collapse" role="tabpanel" aria-labelledby="headingThree" data-parent="#accordion">
                <div class="card-body">
                  

                  <div class="table-responsive table-striped">
                       <?php 
                        $versiones = getimpresasbyplaza($value['id']);
                         ?>
                    <table class="table">
                      <thead>
                        <tr>
                          <th>Portada</th>
                          <th>URL</th>
                          <th>Edición/Año</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php foreach ($versiones as $k => $v): ?>
                          
                        <tr id="vimp_<?= $v['code']?>">
                          <td>
                              <div class="row lightGallery lightgallery-without-thumb">
                            <a href="../../images/versiones-impresas/<?= $v['portada']?>" class="image-tile"><img src="../../images/versiones-impresas/<?= $v['portada']?>" alt="IMG <?= $v['portada']?>"></a>
                          </div>

                          </td>
                          <td><a href="<?php echo $v['url'] ?>" target="_blank"><?php echo $v['url'] ?></a></td>
                           <td><?= mes_espanol($v['edicion_mes'])?>/<?= $v['edicion_ano']?></td>
                          <td><button type="button" class="btn btn-danger mb-1 btn-xs delete_data" data-table="versiones_impresas" data-code="<?= $v['code']?>">Eliminar</button></td>
                        </tr>
                        <?php endforeach ?>
                        
                      </tbody>
                    </table>
                  </div>


                </div>
              </div>
            </div>
            <?php endforeach ?>


          </div>
        </div>
      </div>
    </div>
    
    
    
  </div>
</div>


<link rel="stylesheet" href="../../node_modules/lightgallery/dist/css/lightgallery.min.css">

<script src="../../node_modules/lightgallery/dist/js/lightgallery-all.min.js"></script>

<script src="../../js/light-gallery.js"></script>










<div class="modal fade" id="add_version_modal" tabindex="-1" role="dialog" aria-labelledby="add_version_modal" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><i class="ti-book text-primary"></i>&nbsp; Agregar Versión Impresa</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
   <!--      <p class="card-description">
          Ingrese los datos del nuevo usuario.
        </p> -->

        <form class="forms-sample form-ajax" method="POST" action="versiones-impresas/add_version.php" enctype="multipart/form-data" notif="Version agregada con éxito" reset="1">

          <label for="input_portada" class="label-avatar">
            <div class="plus_foto_perfil"><i class="fa fa-plus" aria-hidden="true"></i></div>
            <input type="file" onchange="readURL(this);" accept="image/*" data-target="foto_avatar" id="input_portada" class="input-img" name="portada" accept="image/*">
            <img id="foto_avatar" src="../../images/versiones-impresas/default.png" alt="Portada" class="preview-img">
          </label>


          <div class="form-group">
            <label for="exampleInputName1">URL</label>
            <input type="text" class="form-control" id="exampleInputName1" placeholder="URL" name="url" required="">
          </div>

            <div class="form-group">
            <label for="exampleFormControlSelect2">Edición</label>
            <select class="form-control" id="" name="edicion_mes">
            <option value="1" <?=($month+1==13) ? 'selected=""' : ''?>>Enero</option>
                    <option value="2" <?=($month+1==2) ? 'selected=""' : ''?>>Febrero</option>
                    <option value="3" <?=($month+1==3) ? 'selected=""' : ''?>>Marzo</option>
                    <option value="4" <?=($month+1==4) ? 'selected=""' : ''?>>Abril</option>
                    <option value="5" <?=($month+1==5) ? 'selected=""' : ''?>>Mayo</option>
                    <option value="6" <?=($month+1==6) ? 'selected=""' : ''?>>Junio</option>
                    <option value="7" <?=($month+1==7) ? 'selected=""' : ''?>>Julio</option>
                    <option value="8" <?=($month+1==8) ? 'selected=""' : ''?>>Agosto</option>
                    <option value="9" <?=($month+1==9) ? 'selected=""' : ''?>>Septiembre</option>
                    <option value="10" <?=($month+1==10) ? 'selected=""' : ''?>>Octubre</option>
                    <option value="11" <?=($month+1==11) ? 'selected=""' : ''?>>Noviembre</option>
                    <option value="12" <?=($month+1==12) ? 'selected=""' : ''?>>Diciembre</option>
            </select>
          </div>

            <div class="form-group">
            <label for="exampleFormControlSelect2">Año</label>
            <select class="form-control" id="" name="edicion_ano">
              <?php 
                    if ($month == 12) {
                      $year = $year+1;
                    }
                    ?>
                    <option value="2018" <?=($year==2018) ? 'selected=""' : ''?>>2018</option>                      
                    <option value="2019" <?=($year==2019) ? 'selected=""' : ''?>>2019</option>
                    <option value="2020" <?=($year==2020) ? 'selected=""' : ''?>>2020</option>
                    <option value="2021" <?=($year==2021) ? 'selected=""' : ''?>>2021</option>
                    <option value="2022" <?=($year==2022) ? 'selected=""' : ''?>>2022</option>
                    <option value="2023" <?=($year==2023) ? 'selected=""' : ''?>>2023</option>
            </select>
          </div>

            <div class="form-group">
            <label for="exampleFormControlSelect2">Plaza</label>
            <select class="form-control" id="" name="plaza_id">
                <option disabled="">Selecciona la plaza</option>
                    <?php foreach ($plazas as $key => $value): ?>
                      <option value="<?= $value['id']?>"><?= $value['nombre']?></option>
                    <?php endforeach ?>
            </select>
          </div>

          

          

          


          <div class="text-right">
            <button type="submit" class="btn btn-success mr-2">Aceptar</button>
            <button type="button" class="btn btn-light" data-dismiss="modal" onClick="this.form.reset()">Cancelar</button>
          </div>
        </form>

      </div>

    </div>
  </div>
</div>
