
<script>
  $(document).ready(function(){

    $.fn.navbarTitle("Webinar: (<?= $webinar['titulo'] ?>) ");

    // html = $("#navbar-html").html()
    // $.fn.navbarBtn(html);

    // $.fn.sidebarContent("hide");

  });
</script>














<div class="content-wrapper">
  <div class="row user-profile">
    <div class="col-lg-3 side-left d-flex align-items-stretch">
      <div class="row">
        <div class="col-12 grid-margin stretch-card">
          <div class="card">
            <div class="card-body avatar">
              <h4 class="card-title">Info</h4>
              
              <p class="name"><?= $webinar['titulo'] ?></p>
              <p class="designation"><?= date("d-m-Y h:i:s a", strtotime($webinar['fecha'])) ?></p>
              <p class="designation"><?= $webinar['conferencista'] ?></p>
              <div class="row text-center">
              <a class="btn btn-info" style="margin: auto;" href="<?= $webinar['webpage'] ?>" target="_blank">Ir a landing</a>
              
              </div>

              <div class="row">
                <hr>
                <h1 class="text-center" style="display: block;
    margin: auto;
    margin-top: 21px;
    background: gainsboro;
    width: 100%;
    border-radius: 6px;
    padding: 5px;">
                <?= count($registros) ?> <br>
                <small class="text-muted">Registros</small>
              </h1>
              </div>
              <!-- <a class="d-block text-center text-dark" href="#">+1 9438 934089</a> -->
            </div>
          </div>
        </div>

        <div class="col-12 stretch-card">
          <div class="card">
            <div class="card-body overview">
              
            </div>
          </div>
        </div>


      </div>
    </div>
    <div class="col-lg-9 side-right stretch-card">
      <div class="card">
        <div class="card-body">
          <div class="wrapper d-block d-sm-flex align-items-center justify-content-between">
            <h4 class="card-title mb-0">Registros</h4>
            
          </div>
          <div class="wrapper">
            <hr>
            <div class="tab-content" id="myTabContent" style="padding: 10px;">


<div class="tab-pane fade show active" id="impresiones" role="tabpanel" aria-labelledby="impresiones">
                <div class="col-md-12">
                  <div class="table-responsive">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>
                            Nombre
                          </th>
                          <th>
                            Correo
                          </th>
                          <th>
                            Teléfono
                          </th>
                          <th>
                            Cargo
                          </th>
                          <th>
                            Empresa
                          </th>

                          <th>
                            Horario
                          </th>
                          
                        </tr>
                      </thead>
                      <tbody>
                        <?php foreach ($registros as $a => $v): ?>
                        <tr>
                          <td>
                            <?= $v['nombre'] ?> 
                          </td>
                          <td>
                            <?= $v['correo'] ?> 
                          </td>
                          <td>
                            <?= $v['telefono'] ?>                             
                          </td>
                          <td>
                            <?= $v['cargo'] ?>                             
                          </td>
                          <td>
                            <?= $v['empresa'] ?>                             
                          </td>

                          <td>
                            <?= $v['horario'] ?>                             
                          </td>
                          
                        </tr>
                        <?php endforeach ?>
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td><strong>Total</strong></td>
                          <td><?= count($registros) ?> </td>

                        </tr>
                      </tbody>
                    </table>
                  </div>

                  <!-- <div class="card">
                    <div class="card-bodyy">

                    
                      <div class="w-100 mx-auto">
                        <div class="d-flex justify-content-between text-center mb-5">
                          <div class="wrapper">
                            <h4>6,256</h4>
                            <small class="text-muted">Impresiones</small>
                          </div>
                          <div class="wrapper">
                            <h4>8569</h4>
                            <small class="text-muted">Clicks</small>
                          </div>
                        </div>
                      </div>
                      <div id="grafica_clicks" style="height:250px;"></div>

                    
                       
              


              





                     </div>
                  </div> -->
                </div>


              </div>
              

              <!-- tab content ends -->









                      <!-- <div class="tab-pane fade" id="security" role="tabpanel" aria-labelledby="security-tab">
                        <form class="form-ajax" method="POST" action="usuarios/edit_password_by_admin.php" notif="Contraseña actualizada con éxito" reset="1">
                          <div class="form-group">
                            <label for="change-password">Cambiar contaseña:</label>
                            <input type="text" class="form-control" name="password" id="change-password" placeholder="Ingresar contraseña nueva." required="">
                          </div>
                          <div class="form-group">
                            <input type="text" class="form-control" name="re_password" id="new-password" placeholder="Reingresar contraseña" required="">
                          </div>
                          <div class="form-group mt-5">
                            <input type="hidden" name="code" value="<?= $usuario['code']?>">

                            <button type="submit" class="btn btn-success mr-2">Actualizar</button>
                            <input class="btn btn-outline-danger" type="reset" value="Cancelar">
                          </div>
                        </form>
                      </div> -->
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>


        <link rel="stylesheet" href="../../node_modules/icheck/skins/all.css" />

        <link rel="stylesheet" href="../../node_modules/dropify/dist/css/dropify.min.css">


        <script src="../../node_modules/dropify/dist/js/dropify.min.js"></script>
        <script src="../../node_modules/icheck/icheck.min.js"></script>

        <script src="../../js/iCheck.js"></script>

        <script>
          (function($) {
            'use strict';
            $('.dropify').dropify();
          })(jQuery);

        </script>





        









        <link rel="stylesheet" href="../../node_modules/select2/dist/css/select2.min.css" />

        <link rel="stylesheet" href="../../node_modules/jquery-tags-input/dist/jquery.tagsinput.min.css">
        <!-- Plugin js for this page-->
        <script src="../../node_modules/jquery-tags-input/dist/jquery.tagsinput.min.js"></script>
        <script src="../../js/formpickers.js"></script>
        <script src="../../js/form-addons.js"></script>
<!-- <link rel="stylesheet" href="node_modules/icheck/skins/all.css" />
<link rel="stylesheet" href="node_modules/select2/dist/css/select2.min.css" />
<link rel="stylesheet" href="node_modules/select2-bootstrap-theme/dist/select2-bootstrap.min.css" /> -->
<link rel="stylesheet" href="../../node_modules/lightgallery/dist/css/lightgallery.min.css">
<link rel="stylesheet" href="../../node_modules/flag-icon-css/css/flag-icon.min.css" />
<script src="../../node_modules/lightgallery/dist/js/lightgallery-all.min.js"></script>
<script src="../../js/light-gallery.js"></script>
<!-- endinject -->
<!-- Plugin js for this page-->
<script src="../../node_modules/icheck/icheck.min.js"></script>
<script src="../../node_modules/typeahead.js/dist/typeahead.bundle.min.js"></script>
<script src="../../node_modules/select2/dist/js/select2.min.js"></script>
<!-- End plugin js for this page-->
<!-- inject:js -->
<!-- endinject -->
<!-- Custom js for this page-->
<!-- <script src="js/file-upload.js"></script> -->
<script src="../../js/iCheck.js"></script>
<script src="../../js/typeahead.js"></script>
<script src="../../js/select2.js"></script>