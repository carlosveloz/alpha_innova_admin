
<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title><?= getConfig('title')?></title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="node_modules/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="node_modules/simple-line-icons/css/simple-line-icons.css">
  <link rel="stylesheet" href="node_modules/flag-icon-css/css/flag-icon.min.css">
  <link rel="stylesheet" href="node_modules/perfect-scrollbar/dist/css/perfect-scrollbar.min.css">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="css/style.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="images/<?= getConfig('logo') ?>" />
  <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@100&display=swap" rel="stylesheet">

</head>











<body>
  <div class="container-scroller">
    <div class="container-fluid page-body-wrapper">
      <div class="row">
        <div class="content-wrapper full-page-wrapper d-flex align-items-center auth login-full-bg" style="background: url('images/<?= getConfig('background') ?>'); background-size: cover; background-position: center;">
          <div class="row w-100">
            <div class="col-lg-4 mx-auto">
              <div class="auth-form-dark text-left p-5">
                <!-- <h4 class="font-weight-light">Bienvenido</h4> -->
                  <img src="images/<?= getConfig('logo') ?>" alt="" style="width: 30%; margin: auto; display: block;">
                <h5 class="text-center" style="font-family: 'Raleway', sans-serif;"><?= getConfig('title')?></h5> 

                <form class="pt-5">
          <div id="login_aviso"></div>

                  <div class="form-group">
                    <label for="exampleInputEmail1">Usuario</label>
                    <input type="email" class="form-control" id="login_usuario" placeholder="email" value="">
                    <i class="mdi mdi-account"></i>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Password</label>
                    <input type="password" class="form-control" id="login_pass" placeholder="Password" value="">
                    <i class="mdi mdi-eye"></i>
                  </div>
                  <div class="mt-5">
                    <a class="btn btn-block btn-default btn-lg font-weight-medium" id="login_btn">Entrar</a>
                  </div>
                  <div class="mt-3 text-center">
                    <a href="#" class="auth-link text-white">Ir a sitio</a>
                  </div>                 
                </form>
              </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->
      </div>
      <!-- row ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>











  <!-- container-scroller -->
  <!-- plugins:js -->
  <script src="node_modules/jquery/dist/jquery.min.js"></script>
  <script src="node_modules/popper.js/dist/umd/popper.min.js"></script>
  <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
  <script src="node_modules/perfect-scrollbar/dist/js/perfect-scrollbar.jquery.min.js"></script>
  <!-- endinject -->
  <!-- inject:js -->
  <script src="js/off-canvas.js"></script>
  <script src="js/hoverable-collapse.js"></script>
  <script src="js/misc.js"></script>
  <script src="js/settings.js"></script>
  <script src="js/todolist.js"></script>

<style> 
#login_btn{
      background: white;
    color: black;
}

#login_btn:hover{
      background: whitesmoke;
    color: black;
}

.auth .auth-form-dark {
    background: rgb(0 0 0);
    color: #ffffff;
}

</style>
<script>
(function($) {

    $('#login_pass').keyup(function(e) {
    if (e.which == 13) {
      $("#login_btn").trigger("click");
    }
});

      $('body').on('click', '#login_btn', function(){
    //alert(ruta); return false;
    var correo = $("#login_usuario").val();
    var password = $("#login_pass").val();

    $.ajax({
      cache:false,
      type: "post",
            //dataType:'json',
            url: "<?php echo $root ?>/php/forms/general/login.php",
            data:{
              'correo': correo,
              'password': password
            },
            success:function(result){
              console.log(result);

              if (result == "ok") {
                $("#login_aviso").html("");

                $(location).attr('href', '<?php echo $root ?>/app/dashboard')
              } else {
                // $("#login_aviso").html("<div class='alert alert-danger alert-dismissible fade in' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span></button><strong>Error!</strong> Usuario o contraseña incorrectos.</div>");
                  $("#login_aviso").html('<div class="alert alert-fill-danger" role="alert"><i class="mdi mdi-alert-circle"></i>'+result+'</div>');
              };

            },

            error: function(data_er){
              alert("No se completo el proceso");
            }
          });
  });



  })(jQuery);
</script>


  <!-- endinject -->
</body>

</html>
