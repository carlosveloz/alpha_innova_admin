
<script>
  $(document).ready(function(){

    $.fn.navbarTitle("Etiquetas");

    html = $("#navbar-html").html()
    $.fn.navbarBtn(html);

    // $.fn.sidebarContent("hide");

  });
</script>

<div id="navbar-html" style="display: none;">
  <ul class="navbar-nav">
    <li class="nav-item dropdown d-none d-lg-flex">
      <a class="nav-link dropdown-toggle nav-btn" id="actionDropdown" href="#" data-toggle="dropdown">
        <span class="btn">Opciones</span>
      </a>
      <div class="dropdown-menu navbar-dropdown dropdown-left" aria-labelledby="actionDropdown">

        <a class="dropdown-item" data-toggle="modal" data-target="#add_plaza_modal">
          <i class="fa fa-tag text-primary"></i>
          Agregar Etiqueta
        </a>

    <!--     <div class="dropdown-divider"></div>

        <a class="dropdown-item toggler" href="#" data-toggle="#usuarios-inactivos" data-this="disabled" data-that="#usuarios-activos-btn" id="usuarios-activos-btn">
          <i class="icon-location-pin text-warning"></i>
          Plazas Inactivas
        </a>
        <div class="dropdown-divider"></div>

        <a class="dropdown-item toggler" href="#" data-toggle="#usuarios-activos" data-this="disabled" data-that="#usuarios-inactivos-btn" id="usuarios-activos-btn">
          <i class="icon-location-pin text-info"></i>
          Plazas Activas
        </a> -->



      </div>
    </li>
  </ul>
</div>















<!-- partial -->
<div class="content-wrapper">



  <div class="" id="">
    <div class="row">
      <div class="col-md-12">
        <!-- <h3 class="text-primary">Plazas Activas</h3> -->
        <div class="space15"></div>
      </div>
      
      <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">

            <div class="table-responsive">
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th>
                      Nombre Español
                    </th>
                    <th>
                      Nombre Ingles
                    </th>
                    <th>
                      Notas
                    </th>
                        <th>
                      Status
                    </th>
                
                    <th>
                      *
                    </th>
                  </tr>
                </thead>
                <tbody>



                  <?php foreach ($etiquetas as $key => $value): ?>

                    <tr>
                      <td>
                        <?= $value['nombre_es']?>
                      </td>
                      <td>
                        <?= $value['nombre_en']?>
                      
                      </td>
                       <td>
                         *
                       </td>
                       <td>
                        <?php if ($value['status'] == 1): ?>
                         <label class="badge badge-success mb-1">Activo</label>
                          <?php else: ?>
                         <label class="badge badge-warning mb-1">Inactivo</label>                            
                        <?php endif ?>
                       </td>
                    <td>
                      <button type="button" class="btn btn-primary mb-1 btn-xs get_data" data-table="etiquetas" data-code="<?= $value['code']?>"><i class="fa fa-pencil"></i>Editar</button>


                       <?php if ($value['status'] == 1): ?>
                      <button type="button" class="btn btn-warning mb-1 btn-xs btn-status" data-id="<?= $value['code'] ?>" data-type="4"><i class="fa fa-times"></i>Desactivar</button>
                          <?php else: ?>
                         <button type="button" class="btn btn-info mb-1 btn-xs btn-status" data-id="<?= $value['code'] ?>" data-type="5"><i class="fa fa-check"></i>Activar</button>                         
                        <?php endif ?>


                    </td>
                  </tr>
                <?php endforeach ?>

              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>



  </div>
</div>




</div>











<div class="modal fade" id="add_plaza_modal" tabindex="-1" role="dialog" aria-labelledby="add_plaza_modal" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><i class="fa fa-tag text-primary"></i> Agregar Etiqueta</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
   <!--      <p class="card-description">
          Ingrese los datos del nuevo usuario.
        </p> -->

        <form class="forms-sample form-ajax" method="POST" action="noticias/add_etiqueta.php" notif="Etiqueta agregada con éxito <br>Refresque la página para actualizar la lista." reset="1">

          <div class="row">
            <div class="col-md-12">

              <div class="form-group">
                <label for="nombre_es">Nombre <small>(Español)</small></label>
                <input type="text" class="form-control" id="nombre_es" placeholder="Ingresa en nombre en español" name="nombre_es" required="">
              </div>

                <div class="form-group">
                <label for="nombre_en">Nombre <small>(Ingles)</small></label>
                <input type="text" class="form-control" id="nombre_en" placeholder="Ingresa en nombre en ingles" name="nombre_en" required="">
              </div>


            </div>

          </div>

          <div class="text-right">
            <button type="submit" class="btn btn-success mr-2">Aceptar</button>
            <button type="button" class="btn btn-light" data-dismiss="modal" onClick="this.form.reset()">Cancelar</button>
          </div>
        </form>

      </div>

    </div>
  </div>
</div>




<div class="modal fade" id="edit_etiqueta_modal" tabindex="-1" role="dialog" aria-labelledby="edit_etiqueta_modal" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><i class="fa fa-tag text-info"></i> Editar Etiqueta</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
   <!--      <p class="card-description">
          Ingrese los datos del nuevo usuario.
        </p> -->

        <form class="forms-sample form-ajax" method="POST" action="noticias/edit_etiqueta.php" notif="Etiqueta actualizada con éxito <br>Refresque la página para actualizar la lista." reset="0">

          <div class="row">
            <div class="col-md-12">

              <div class="form-group">
                <label for="edit_nombre_es">Nombre <small>(Español)</small></label>
                <input type="text" class="form-control" id="edit_nombre_es" placeholder="Ingresa en nombre en español" name="nombre_es" required="">
              </div>

                <div class="form-group">
                <label for="edit_nombre_en">Nombre <small>(Ingles)</small></label>
                <input type="text" class="form-control" id="edit_nombre_en" placeholder="Ingresa en nombre en ingles" name="nombre_en" required="">
              </div>

        <input type="hidden" name="code" id="edit_code">
            </div>

          </div>

          <div class="text-right">
            <button type="submit" class="btn btn-success mr-2">Aceptar</button>
            <button type="button" class="btn btn-light" data-dismiss="modal" onClick="this.form.reset()">Cancelar</button>
          </div>
        </form>

      </div>

    </div>
  </div>
</div>





<link rel="stylesheet" href="../../node_modules/jquery-tags-input/dist/jquery.tagsinput.min.css">
<!-- Plugin js for this page-->
<script src="../../node_modules/jquery-tags-input/dist/jquery.tagsinput.min.js"></script>
<script src="../../js/formpickers.js"></script>
<script src="../../js/form-addons.js"></script>
<!-- <link rel="stylesheet" href="node_modules/icheck/skins/all.css" />
<link rel="stylesheet" href="node_modules/select2/dist/css/select2.min.css" />
<link rel="stylesheet" href="node_modules/select2-bootstrap-theme/dist/select2-bootstrap.min.css" /> -->
<link rel="stylesheet" href="../../node_modules/lightgallery/dist/css/lightgallery.min.css">
<link rel="stylesheet" href="../../node_modules/flag-icon-css/css/flag-icon.min.css" />
<script src="../../node_modules/lightgallery/dist/js/lightgallery-all.min.js"></script>
<script src="../../js/light-gallery.js"></script>
<!-- endinject -->
<!-- Plugin js for this page-->
<script src="../../node_modules/icheck/icheck.min.js"></script>
<script src="../../node_modules/typeahead.js/dist/typeahead.bundle.min.js"></script>
<script src="../../node_modules/select2/dist/js/select2.min.js"></script>
<!-- End plugin js for this page-->
<!-- inject:js -->
<!-- endinject -->
<!-- Custom js for this page-->
<!-- <script src="js/file-upload.js"></script> -->
<script src="../../js/iCheck.js"></script>
<script src="../../js/typeahead.js"></script>
<script src="../../js/select2.js"></script>