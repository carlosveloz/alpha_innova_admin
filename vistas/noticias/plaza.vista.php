
<script>
  $(document).ready(function(){

    $.fn.navbarTitle("Detalles de Plaza");

    // html = $("#navbar-html").html()
    // $.fn.navbarBtn(html);

    // $.fn.sidebarContent("hide");

  });
</script>






<div class="content-wrapper">
  <div class="row user-profile">
    <div class="col-lg-4 side-left d-flex align-items-stretch">
      <div class="row">
        <div class="col-12 grid-margin stretch-card">
          <div class="card">
            <div class="card-body avatar">
              <h4 class="card-title">Info</h4>

              <a class="toggler" data-toggle=".info-mx"> <i class="flag-icon flag-icon-mx" title="mx" id="mx" style="    font-size: 30px;
              margin: auto;
              margin-bottom: 20px;"></i></a>

              <a class="toggler" data-toggle=".info-us"> <i class="flag-icon flag-icon-us" title="us" id="us" style="    font-size: 30px;
              margin: auto;
              margin-bottom: 20px;"></i></a>

              <div class="toggle info-mx">
                <img src="../../images/plazas/<?= $plaza['img_es'] ?>" alt="">
                <p class="name"><i class="flag-icon flag-icon-mx" title="mx" id="mx" style="font-size: 15px;
                margin: auto;
                margin-bottom: 0px;"></i><br><?= $plaza['nombre'] ?> 
              </p>
            </div>

            <div class="toggle info-us" style="display: none;">
              <img src="../../images/plazas/<?= $plaza['img_en'] ?>" alt="">
              <p class="name"><i class="flag-icon flag-icon-us" title="us" id="us" style="font-size: 15px;
              margin: auto;
              margin-bottom: 0px;"></i><br><?= $plaza['nombre'] ?> 
            </p>
          </div>

        </div>
      </div>
    </div>

    <div class="col-12 stretch-card">
      <div class="card">
        <div class="card-body overview">

          <div class="wrapper about-user">
            <div class="toggle info-mx">

              <h4 class="card-title mt-4 mb-3">Descripción</h4>
              <p><?= $plaza['descripcion_es'] ?></p>

              <h4 class="card-title mt-4 mb-3">Palabras Clave</h4>
              <?php 
              $keys_es = explode(",",$plaza['keywords_es']);
              ?>
              <?php foreach ($keys_es as $k_k => $k_v): ?>
                <label class="badge badge-info mb-1"><?= $k_v ?></label>
              <?php endforeach ?>

            </div>

            <div class="toggle info-us" style="display: none;">

              <h4 class="card-title mt-4 mb-3">Descripción <small>(Ingles)</small></h4>
              <p><?= $plaza['descripcion_en'] ?></p>

              <h4 class="card-title mt-4 mb-3">Palabras Clave <small>(Ingles)</small></h4>
              <?php 
              $keys_en = explode(",",$plaza['keywords_en']);
              ?>
              <?php foreach ($keys_en as $k_k => $k_v): ?>
                <label class="badge badge-info mb-1"><?= $k_v ?></label>
              <?php endforeach ?>


            </div>

            <h4 class="card-title mt-4 mb-3">Status</h4>
            <?php if ($plaza['status'] == 1): ?>
             <label class="badge badge-primary mb-1">Activo</label>
             <?php else: ?>
               <label class="badge badge-warning mb-1">Inactivo</label>                  
             <?php endif ?>

           </div>


         </div>
       </div>
     </div>


   </div>
 </div>








 <div class="col-lg-8 side-right stretch-card">
  <div class="card">
    <div class="card-body">
      <div class="wrapper d-block d-sm-flex align-items-center justify-content-between">
        <h4 class="card-title mb-0">Detalles</h4>
        
        <ul class="nav nav-tabs tab-solid tab-solid-primary mb-0" id="myTab" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" id="noticias-tab" data-toggle="tab" href="#noticias" role="tab" aria-controls="noticias" aria-expanded="true">Notas</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" id="configuracion-tab" data-toggle="tab" href="#configuracion" role="tab" aria-controls="configuracion">Configuración</a>
          </li>
        </ul>

      </div>

      <div class="wrapper">
        <hr>
        <div class="tab-content" id="myTabContent">
          <div class="tab-pane fade show active" id="noticias" role="tabpanel" aria-labelledby="noticias">
           <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>
                    Imagen
                  </th>
                  <th>
                    Titulo
                  </th>
                  <th>
                    Contenido
                  </th>
                  <th>
                    Edición/Año
                  </th>
                  <th>
                    Vistas
                  </th>
                  <th>
                    Status
                  </th>
                  <th>
                    *
                  </th>
                </tr>
              </thead>
              <tbody>




                <tr>
                  <td>
                   <img src="<?= $root ?>/images/avatars/avatar.png" alt="image" class="img-lg mr-3 rounded-circle">
                 </td>
                 <td>
                  Titulo
                </td>
                <td>
                  Contenido Lorem ipsum dolor sit amet, consectetur adipisicing elit. Soluta incidunt nostrum ullam minus velit itaque sint quos.
                </td>
                <td>
                  Noviembre/2019
                </td>
                <td>
                  48 Visitas
                </td>
                <td>
                  <?php if ($plaza['status'] == 1): ?>
                   <label class="badge badge-success mb-1">Activo</label>
                   <?php else: ?>
                     <label class="badge badge-warning mb-1">Inactivo</label>                  
                   <?php endif ?>
                 </td>

                 <td>
                  <a href="<?= $root ?>/app/noticias/plaza.php?c=<?= $value['code'] ?>" class="btn btn-primary mb-1 btn-xs"><i class="icon-options-vertical"></i>Detalles</a>
                  <button type="button" class="btn btn-warning mb-1 btn-xs btn-status" data-id="<?= $value['code'] ?>" data-type="6"><i class="fa fa-times"></i>Desactivar</button>
                </td>
              </tr>

            </tbody>
          </table>
        </div>
      </div><!-- tab content ends -->

      <div class="tab-pane fade" id="configuracion" role="tabpanel" aria-labelledby="configuracion-tab">

   <!--      <p class="card-description">
          Ingrese los datos del nuevo usuario.
        </p> -->

        <form class="forms-sample form-ajax" method="POST" action="noticias/add_plaza.php" enctype="multipart/form-data" notif="Plaza creada con éxito" reset="1">

          <div class="row">
            <div class="col-md-10 offset-md-1">
              <div class="form-group">
                <label for="exampleInputName1">Nombre de la plaza</label>
                <input type="text" class="form-control" id="exampleInputName1" placeholder="Nombre" name="nombre" required="" value="<?= $plaza['nombre'] ?>">
              </div>
              <br><br>  
            </div>
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="row text-center">
                <i class="flag-icon flag-icon-mx" title="mx" id="mx" style="    font-size: 30px;
                margin: auto;
                margin-bottom: 20px;"></i>
              </div>
              <label for="input_foto_es" class="label-logo" style="width: 100%;">
                <div class="plus_foto_square"><i class="fa fa-plus" aria-hidden="true"></i></div>
                <input type="file" onchange="readURL(this);" accept="image/*" data-target="foto_plaza_es" id="input_foto_es" class="input-img" name="img_es" accept="image/*">
                <img id="foto_plaza_es" src="../../images/plazas/<?= $plaza['img_es'] ?>" alt="Foto perfil" class="preview-img-square">
              </label>



              <div class="form-group">
                <label for="exampleInputName1">Descripción</label>
                <input type="text" class="form-control" id="exampleInputName1" placeholder="Descripción" name="desc_es" required="" value="<?= $plaza['descripcion_es'] ?>">
              </div>

              <div class="form-group">
                <label for="exampleInputEmail3">Palabras Clave <small>(Separadas por coma)</small></label>
                <input name="keywords_es" class="tags" value="<?= $plaza['keywords_es'] ?>" required=""/>
              </div>

            </div>

            <div class="col-md-6">
              <div class="row text-center">
                <i class="flag-icon flag-icon-us" title="us" id="us" style="    font-size: 30px;
                margin: auto;
                margin-bottom: 20px;"></i>
              </div>
              <label for="input_foto_en" class="label-logo" style="width: 100%;">
                <div class="plus_foto_square"><i class="fa fa-plus" aria-hidden="true"></i></div>
                <input type="file" onchange="readURL(this);" accept="image/*" data-target="foto_plaza_en" id="input_foto_en" class="input-img" name="img_en" accept="image/*">
                <img id="foto_plaza_en" src="../../images/plazas/<?= $plaza['img_en'] ?>" alt="Foto perfil" class="preview-img-square">
              </label>


              
              <div class="form-group">
                <label for="exampleInputName1">Descripción</label>
                <input type="text" class="form-control" id="exampleInputName1" placeholder="Descripcion (Ingles)" name="desc_en" required="" value="<?= $plaza['descripcion_en'] ?>">
              </div>

              <div class="form-group">
                <label for="exampleInputEmail3">Palabras Clave <small>(Separadas por coma)</small></label>
                <input name="keywords_en" class="tags" value="<?= $plaza['keywords_en'] ?>" required=""/>
              </div>

            </div>
          </div>

          <div class="text-right">
            <button type="submit" class="btn btn-success mr-2">Aceptar</button>
            <button type="button" class="btn btn-light" data-dismiss="modal" onClick="this.form.reset()">Cancelar</button>
          </div>
        </form>


      </div>
    </div>
  </div>

</div>
</div>
</div>
</div>
</div>


<link rel="stylesheet" href="../../node_modules/icheck/skins/all.css" />

<link rel="stylesheet" href="../../node_modules/dropify/dist/css/dropify.min.css">


<script src="../../node_modules/dropify/dist/js/dropify.min.js"></script>
<script src="../../node_modules/icheck/icheck.min.js"></script>

<script src="../../js/iCheck.js"></script>


<!-- tags -->
<link rel="stylesheet" href="../../node_modules/jquery-tags-input/dist/jquery.tagsinput.min.css">
<script src="../../node_modules/jquery-tags-input/dist/jquery.tagsinput.min.js"></script>
<script src="../../js/form-addons.js"></script>




<script>
  (function($) {
    'use strict';
    $('.dropify').dropify();
  })(jQuery);

</script>