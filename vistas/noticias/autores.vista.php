
<script>
  $(document).ready(function(){

    $.fn.navbarTitle("Autores");

    html = $("#navbar-html").html()
    $.fn.navbarBtn(html);

    // $.fn.sidebarContent("hide");

  });
</script>

<div id="navbar-html" style="display: none;">
  <ul class="navbar-nav">
    <li class="nav-item dropdown d-none d-lg-flex">
      <a class="nav-link dropdown-toggle nav-btn" id="actionDropdown" href="#" data-toggle="dropdown">
        <span class="btn">Opciones</span>
      </a>
      <div class="dropdown-menu navbar-dropdown dropdown-left" aria-labelledby="actionDropdown">

        <a class="dropdown-item" data-toggle="modal" data-target="#add_autor_modal">
          <i class="fa fa-tag text-primary"></i>
          Agregar Autor
        </a>

        <div class="dropdown-divider"></div>

        <a class="dropdown-item toggler" href="#" data-toggle="#autores_inactivos" data-this="disabled" data-that="#usuarios-activos-btn" id="usuarios-activos-btn">
          <i class="icon-location-pin text-warning"></i>
          Autores Inactivos
        </a>
        <div class="dropdown-divider"></div>

        <a class="dropdown-item toggler" href="#" data-toggle="#autores_activos" data-this="disabled" data-that="#usuarios-inactivos-btn" id="usuarios-activos-btn">
          <i class="icon-location-pin text-info"></i>
          Autores Activos
        </a>



      </div>
    </li>
  </ul>
</div>















<!-- partial -->
<div class="content-wrapper">



  <div class="toggle" id="autores_activos">
    <div class="row">
      <div class="col-md-12">
        <h3 class="text-primary">Autores Activos</h3>
        <div class="space15"></div>
      </div>
      
      <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">

            <div class="table-responsive">
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th>
                      Avatar
                    </th>
                    <th>
                      Nombre
                    </th>
                    <th>
                      Plazas
                    </th>
                    <th>
                      Status
                    </th>

                    <th>
                      *
                    </th>
                  </tr>
                </thead>
                <tbody>



                  <?php foreach ($autores as $key => $value): ?>
                    <?php if ($value['status'] == 1): ?>

                      <tr>
                        <td>

                          <div class="row lightGallery lightgallery-without-thumb">
                            <a href="../../images/autores/<?= $value['avatar']?>" class="image-tile"><img src="../../images/autores/<?= $value['avatar']?>" alt="Avatar <?= $value['nombre']?>"></a>
                          </div>

                        </td>
                        <td>
                          <?= $value['nombre']?>

                        </td>
                        <td>
                         <?php foreach ($value['plazas'] as $a => $b): ?>
                          <label class="badge badge-info mb-1"><?= $b['plaza_nombre'] ?></label>
                        <?php endforeach ?>
                      </td>
                      <td>
                        <?php if ($value['status'] == 1): ?>
                         <label class="badge badge-success mb-1">Activo</label>
                         <?php else: ?>
                           <label class="badge badge-warning mb-1">Inactivo</label>                            
                         <?php endif ?>
                       </td>
                       <td>
                        <button type="button" class="btn btn-primary mb-1 btn-xs get_data" data-table="autores" data-code="<?= $value['code']?>"><i class="fa fa-pencil"></i>Editar</button>


                        <?php if ($value['status'] == 1): ?>
                          <button type="button" class="btn btn-warning mb-1 btn-xs btn-status" data-id="<?= $value['code'] ?>" data-type="4"><i class="fa fa-times"></i>Desactivar</button>
                          <?php else: ?>
                           <button type="button" class="btn btn-info mb-1 btn-xs btn-status" data-id="<?= $value['code'] ?>" data-type="8"><i class="fa fa-check"></i>Activar</button>                         
                         <?php endif ?>


                       </td>
                     </tr>
                   <?php endif ?>
                 <?php endforeach ?>

               </tbody>
             </table>
           </div>
         </div>
       </div>
     </div>



   </div>
 </div>






  <div class="toggle" id="autores_inactivos" style="display: none;">
    <div class="row">
      <div class="col-md-12">
        <h3 class="text-warning">Autores inactivos</h3>
        <div class="space15"></div>
      </div>
      
      <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">

            <div class="table-responsive">
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th>
                      Avatar
                    </th>
                    <th>
                      Nombre
                    </th>
                    <th>
                      Plazas
                    </th>
                    <th>
                      Status
                    </th>

                    <th>
                      *
                    </th>
                  </tr>
                </thead>
                <tbody>



                  <?php foreach ($autores as $key => $value): ?>
                    <?php if ($value['status'] == 0): ?>
                      
                      <tr>
                        <td>
                          
                          <div class="row lightGallery lightgallery-without-thumb">
                            <a href="../../images/autores/<?= $value['avatar']?>" class="image-tile"><img src="../../images/autores/<?= $value['avatar']?>" alt="Avatar <?= $value['nombre']?>"></a>
                          </div>
                          
                        </td>
                        <td>
                          <?= $value['nombre']?>

                        </td>
                        <td>
                         <?php foreach ($value['plazas'] as $a => $b): ?>
                          <label class="badge badge-info mb-1"><?= $b['plaza_nombre'] ?></label>
                        <?php endforeach ?>
                      </td>
                      <td>
                        <?php if ($value['status'] == 1): ?>
                         <label class="badge badge-success mb-1">Activo</label>
                         <?php else: ?>
                           <label class="badge badge-warning mb-1">Inactivo</label>                            
                         <?php endif ?>
                       </td>
                       <td>
                        <button type="button" class="btn btn-primary mb-1 btn-xs get_data" data-table="autores" data-code="<?= $value['code']?>"><i class="fa fa-pencil"></i>Editar</button>


                        <?php if ($value['status'] == 1): ?>
                          <button type="button" class="btn btn-warning mb-1 btn-xs btn-status" data-id="<?= $value['code'] ?>" data-type="4"><i class="fa fa-times"></i>Desactivar</button>
                          <?php else: ?>
                           <button type="button" class="btn btn-info mb-1 btn-xs btn-status" data-id="<?= $value['code'] ?>" data-type="8"><i class="fa fa-check"></i>Activar</button>                         
                         <?php endif ?>


                       </td>
                     </tr>
                   <?php endif ?>
                 <?php endforeach ?>

               </tbody>
             </table>
           </div>
         </div>
       </div>
     </div>



   </div>
 </div>

</div>











<div class="modal fade" id="add_autor_modal" tabindex="-1" role="dialog" aria-labelledby="add_autor_modal" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><i class="icon-user-follow text-primary"></i> Agregar Autor</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
   <!--      <p class="card-description">
          Ingrese los datos del nuevo usuario.
        </p> -->

        <form class="forms-sample form-ajax" method="POST" action="noticias/add_autor.php" notif="Autor agregado con éxito <br>Refresque la página para actualizar la lista." reset="1">

          <div class="row">
            <div class="col-md-12">

             <label for="input_foto_perfil" class="label-avatar">
              <div class="plus_foto_perfil"><i class="fa fa-plus" aria-hidden="true"></i></div>
              <input type="file" onchange="readURL(this);" accept="image/*" data-target="foto_avatar" id="input_foto_perfil" class="input-img" name="avatar" accept="image/*">
              <img id="foto_avatar" src="../../images/avatars/avatar.png" alt="Foto perfil" class="preview-img">
            </label>

            <div class="form-group">
              <label for="nombre">Nombre</label>
              <input type="text" class="form-control" id="nombre" placeholder="Ingresa en nombre del autor" name="nombre" required="">
            </div>

            <div class="form-group">
              <label for="exampleFormControlSelect2">Plaza</label>
              <select class="js-example-basic-multiple" multiple="multiple" style="width:100%" name="plazas[]" required="">
                <?php foreach ($plazas as $k => $v): ?>
                  <option value="<?= $v['id']?>"><?= $v['nombre']?></option>
                <?php endforeach ?>
              </select>
            </div>


          </div>

        </div>

        <div class="text-right">
          <button type="submit" class="btn btn-success mr-2">Aceptar</button>
          <button type="button" class="btn btn-light" data-dismiss="modal" onClick="this.form.reset()">Cancelar</button>
        </div>
      </form>

    </div>

  </div>
</div>
</div>
















<div class="modal fade" id="edit_autor_modal" tabindex="-1" role="dialog" aria-labelledby="edit_autor_modal" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><i class="icon-user-follow text-primary"></i> Editar Autor</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
   <!--      <p class="card-description">
          Ingrese los datos del nuevo usuario.
        </p> -->

        <form class="forms-sample form-ajax" method="POST" action="noticias/edit_autor.php" notif="Autor actualizado con éxito <br>Refresque la página para actualizar la lista." reset="1">

          <div class="row">
            <div class="col-md-12">

             <label for="input_foto_perfil" class="label-avatar">
              <div class="plus_foto_perfil"><i class="fa fa-plus" aria-hidden="true"></i></div>
              <input type="file" onchange="readURL(this);" accept="image/*" data-target="foto_avatar" id="input_foto_perfil" class="input-img" name="avatar" accept="image/*">
              <img id="edit_foto_avatar" src="../../images/avatars/avatar.png" alt="Foto perfil" class="preview-img">
            </label>

            <div class="form-group">
              <label for="nombre">Nombre</label>
              <input type="text" class="form-control" id="edit_nombre" placeholder="Ingresa en nombre del autor" name="nombre" required="">
            </div>

            <div class="form-group">
              <label for="exampleFormControlSelect2">Plaza</label>
              <select class="js-example-basic-multiple" multiple="multiple" id="edit_plazas" style="width:100%" name="plazas[]" required="">
                <?php foreach ($plazas as $k => $v): ?>
                  <option value="<?= $v['id']?>"><?= $v['nombre']?></option>
                <?php endforeach ?>
              </select>
            </div>


          </div>

        </div>

        <div class="text-right">
          <input type="hidden" name="" id="edit_code">
          <button type="submit" class="btn btn-success mr-2">Aceptar</button>
          <button type="button" class="btn btn-light" data-dismiss="modal" onClick="this.form.reset()">Cancelar</button>
        </div>
      </form>

    </div>

  </div>
</div>
</div>





<link rel="stylesheet" href="../../node_modules/select2/dist/css/select2.min.css" />

<link rel="stylesheet" href="../../node_modules/jquery-tags-input/dist/jquery.tagsinput.min.css">
<!-- Plugin js for this page-->
<script src="../../node_modules/jquery-tags-input/dist/jquery.tagsinput.min.js"></script>
<script src="../../js/formpickers.js"></script>
<script src="../../js/form-addons.js"></script>
<!-- <link rel="stylesheet" href="node_modules/icheck/skins/all.css" />
<link rel="stylesheet" href="node_modules/select2/dist/css/select2.min.css" />
<link rel="stylesheet" href="node_modules/select2-bootstrap-theme/dist/select2-bootstrap.min.css" /> -->
<link rel="stylesheet" href="../../node_modules/lightgallery/dist/css/lightgallery.min.css">
<link rel="stylesheet" href="../../node_modules/flag-icon-css/css/flag-icon.min.css" />
<script src="../../node_modules/lightgallery/dist/js/lightgallery-all.min.js"></script>
<script src="../../js/light-gallery.js"></script>
<!-- endinject -->
<!-- Plugin js for this page-->
<script src="../../node_modules/icheck/icheck.min.js"></script>
<script src="../../node_modules/typeahead.js/dist/typeahead.bundle.min.js"></script>
<script src="../../node_modules/select2/dist/js/select2.min.js"></script>
<!-- End plugin js for this page-->
<!-- inject:js -->
<!-- endinject -->
<!-- Custom js for this page-->
<!-- <script src="js/file-upload.js"></script> -->
<script src="../../js/iCheck.js"></script>
<script src="../../js/typeahead.js"></script>
<script src="../../js/select2.js"></script>