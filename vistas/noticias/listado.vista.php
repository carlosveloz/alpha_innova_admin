
<script>
  $(document).ready(function(){

    $.fn.navbarTitle("Noticias");

    html = $("#navbar-html").html()
    $.fn.navbarBtn(html);

    // $.fn.sidebarContent("hide");

  });
</script>

<div id="navbar-html" style="display: none;">
  <ul class="navbar-nav">
    <li class="nav-item dropdown d-none d-lg-flex">
      <a class="nav-link dropdown-toggle nav-btn" id="actionDropdown" href="#" data-toggle="dropdown">
        <span class="btn">Opciones</span>
      </a>
      <div class="dropdown-menu navbar-dropdown dropdown-left" aria-labelledby="actionDropdown">

        <a class="dropdown-item" href="<?= $root ?>/app/noticias/agregar_noticia.php" target="_blank">
          <i class="ti-write text-primary"></i>
          Agregar Noticia
        </a>

        <div class="dropdown-divider"></div>

        <a class="dropdown-item toggler" href="#" data-toggle="#noticias-inactivas" data-this="disabled" data-that="#usuarios-activos-btn" id="usuarios-activos-btn">
          <i class="fa fa-times text-warning"></i>
          Noticias Inactivas
        </a>

        <div class="dropdown-divider"></div>
        <a class="dropdown-item toggler" href="#" data-toggle="#noticias-activas" data-this="disabled" data-that="#usuarios-inactivos-btn" id="usuarios-activos-btn">
          <i class="fa fa-check text-info"></i>
          Noticias Activas
        </a>



      </div>
    </li>
  </ul>
</div>















<!-- partial -->
<div class="content-wrapper">





  <div class="toggle" id="noticias-activas">
    <div class="row">
      <div class="col-md-12">
        <h3 class="text-primary">Noticias Activas</h3>
        <div class="space15"></div>
      </div>
      
      <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">
            <div class="row">

              <div class="col-12">
                <div class="table-responsive">
                  <table id="oorder-listing" class="table">
                    <thead>
                      <tr>
                        <th>Img</th>
                        <th>Titulo</th>
                        <th>Plaza</th>
                        <th>Edición/Año</th>
                        <th>Status</th>
                        <th>Vistas</th>
                        <th>*</th>
                      </tr>
                    </thead>

                    <tbody>
                      <?php foreach ($notas as $key => $value): ?>
                        <?php if ($value['status'] == 1): ?>
                          <tr>
                           <td>
                             <div class="row lightGallery lightgallery-without-thumb">
                            <a href="../../images/notas/<?= $value['img']?>" class="image-tile"><img src="../../images/notas/<?= $value['img']?>" alt="IMG <?= $value['titulo_es']?>"></a>
                          </div>
                           </td>
                           <td><?= $value['titulo_es']?></td>
                           <td><?= $value['plaza']?></td>
                           <td><?= mes_espanol($value['edicion_mes'])?>/<?= $value['edicion_ano']?></td>
                           <td><?= $value['autor']?></td>
                           <td><span class="text-success">Activo</span></td>
                           <td><label class="badge badge-info"><i class="fa fa-eye"></i> 249</label></td>
                           <td>
                            <a href="<?= $root ?>/app/noticias/detalles.php?c=<?= $value['code'] ?>" class="btn btn-outline-primary btn-xs">Detalles</a>
                          </td>
                        </tr>
                      <?php endif ?>
                    <?php endforeach ?>
                  </tbody>

                </table>                    
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>



  </div>
</div>

<div class="toggle" id="noticias-inactivas" style="display: none;">
  <div class="row">
    <div class="col-md-12">
      <h3 class="text-warning">Noticias Inctivas</h3>
      <div class="space15"></div>
    </div>

    <div class="col-lg-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">

          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>
                    Nombre
                  </th>
                  <th>
                    Descripción
                  </th>
                  <th>
                    Imagen
                  </th>
                  <th>
                    Palabras Clave
                  </th>
                  <th>
                    Slug
                  </th>
                  <th>
                    Status
                  </th>
                  <th>
                    *
                  </th>
                </tr>
              </thead>
              <tbody>



                <?php foreach ($plazas_inactivas as $key => $value): ?>

                  <tr>
                    <td>
                      <?= $value['nombre']?>
                    </td>
                    <td>
                      <?= $value['descripcion_es']?>
                      <hr>
                      <?= $value['descripcion_en']?>
                    </td>
                    <td>
                      <div class="row lightGallery lightgallery-without-thumb">
                        <?php if ($value['img_es'] != ""): ?>
                          <a href="../../images/plazas/<?= $value['img_es']?>" class="image-tile"><img src="../../images/plazas/<?= $value['img_es']?>" alt="img <?= $value['nombre']?> es"></a>
                        <?php endif ?>

                        <?php if ($value['img_en'] != ""): ?>
                          <a href="../../images/plazas/<?= $value['img_en']?>" class="image-tile"><img src="../../images/plazas/<?= $value['img_en']?>" alt="img <?= $value['nombre']?> en"></a>
                        <?php endif ?>
                      </div>
                    </td>
                    <td>
                      <?php 
                      $keys_es = explode(",",$value['keywords_es']);
                      ?>
                      <?php foreach ($keys_es as $k_k => $k_v): ?>
                        <label class="badge badge-info mb-1"><?= $k_v ?></label>
                      <?php endforeach ?>
                      <hr>
                      <?php 
                      $keys_en = explode(",",$value['keywords_en']);
                      ?>
                      <?php foreach ($keys_en as $k_k => $k_v): ?>
                        <label class="badge badge-info mb-1"><?= $k_v ?></label>
                      <?php endforeach ?>
                    </td>
                    <td>
                      <?= $value['slug']?>
                    </td>
                    <td>
                      <label class="badge badge-success mb-1">Activo</label>
                    </td>
                    <td>
                      <a href="<?= $root ?>/app/noticias/plaza.php?c=<?= $value['code'] ?>" class="btn btn-primary mb-1 btn-xs"><i class="icon-options-vertical"></i>Detalles</a>
                      <button type="button" class="btn btn-info mb-1 btn-xs btn-status" data-id="<?= $value['code'] ?>" data-type="7"><i class="fa fa-check"></i> Activar</button>
                    </td>
                  </tr>
                <?php endforeach ?>

              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>



  </div>
</div>


</div>










<style>
  .table td img{
    width: 60px !important;
    height: 60px !important;
    border-radius: 8% !important;
    box-shadow: 0 0 3px 1px #0000003b;
  }
</style>



<link rel="stylesheet" href="../../node_modules/datatables.net-bs4/css/dataTables.bootstrap4.css" />

<link rel="stylesheet" href="../../node_modules/jquery-tags-input/dist/jquery.tagsinput.min.css">
<!-- Plugin js for this page-->
<script src="../../node_modules/jquery-tags-input/dist/jquery.tagsinput.min.js"></script>
<script src="../../js/formpickers.js"></script>
<script src="../../js/form-addons.js"></script>
<!-- <link rel="stylesheet" href="node_modules/icheck/skins/all.css" />
<link rel="stylesheet" href="node_modules/select2/dist/css/select2.min.css" />
<link rel="stylesheet" href="node_modules/select2-bootstrap-theme/dist/select2-bootstrap.min.css" /> -->
<link rel="stylesheet" href="../../node_modules/lightgallery/dist/css/lightgallery.min.css">
<link rel="stylesheet" href="../../node_modules/flag-icon-css/css/flag-icon.min.css" />
<script src="../../node_modules/lightgallery/dist/js/lightgallery-all.min.js"></script>
<script src="../../js/light-gallery.js"></script>
<!-- endinject -->
<!-- Plugin js for this page-->
<script src="../../node_modules/icheck/icheck.min.js"></script>
<script src="../../node_modules/typeahead.js/dist/typeahead.bundle.min.js"></script>
<script src="../../node_modules/select2/dist/js/select2.min.js"></script>

<script src="../../node_modules/datatables.net/js/jquery.dataTables.js"></script>

<script src="../../node_modules/datatables.net-bs4/js/dataTables.bootstrap4.js"></script>

<script src="../../js/data-table.js"></script>

<!-- End plugin js for this page-->
<!-- inject:js -->
<!-- endinject -->
<!-- Custom js for this page-->
<!-- <script src="js/file-upload.js"></script> -->
<script src="../../js/iCheck.js"></script>
<script src="../../js/typeahead.js"></script>
<script src="../../js/select2.js"></script>