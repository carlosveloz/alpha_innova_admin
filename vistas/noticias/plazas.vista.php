
<script>
  $(document).ready(function(){

    $.fn.navbarTitle("Plazas");

    html = $("#navbar-html").html()
    $.fn.navbarBtn(html);

    // $.fn.sidebarContent("hide");

  });
</script>

<div id="navbar-html" style="display: none;">
  <ul class="navbar-nav">
    <li class="nav-item dropdown d-none d-lg-flex">
      <a class="nav-link dropdown-toggle nav-btn" id="actionDropdown" href="#" data-toggle="dropdown">
        <span class="btn">Opciones</span>
      </a>
      <div class="dropdown-menu navbar-dropdown dropdown-left" aria-labelledby="actionDropdown">

        <a class="dropdown-item" data-toggle="modal" data-target="#add_plaza_modal">
          <i class="icon-location-pin text-primary"></i>
          Agregar Plaza
        </a>

        <div class="dropdown-divider"></div>

        <a class="dropdown-item toggler" href="#" data-toggle="#plazas-inactivas" data-this="disabled" data-that="#usuarios-activos-btn" id="usuarios-activos-btn">
          <i class="icon-location-pin text-warning"></i>
          Plazas Inactivas
        </a>

        <div class="dropdown-divider"></div>
        <a class="dropdown-item toggler" href="#" data-toggle="#plazas-activas" data-this="disabled" data-that="#usuarios-inactivos-btn" id="usuarios-activos-btn">
          <i class="icon-location-pin text-info"></i>
          Plazas Activas
        </a>



      </div>
    </li>
  </ul>
</div>















<!-- partial -->
<div class="content-wrapper">





  <div class="toggle" id="plazas-activas">
    <div class="row">
      <div class="col-md-12">
        <h3 class="text-primary">Plazas Activas</h3>
        <div class="space15"></div>
      </div>
      
      <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">

            <div class="table-responsive">
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th>
                      Nombre
                    </th>
                    <th>
                      Descripción
                    </th>
                    <th>
                      Imagen
                    </th>
                    <th>
                      Palabras Clave
                    </th>
                    <th>
                      Slug
                    </th>
                    <th>
                      Status
                    </th>
                    <th>
                      *
                    </th>
                  </tr>
                </thead>
                <tbody>



                  <?php foreach ($plazas_activas as $key => $value): ?>

                    <tr>
                      <td>
                        <?= $value['nombre']?>
                      </td>
                      <td>
                        <?= $value['descripcion_es']?>
                        <hr>
                        <?= $value['descripcion_en']?>
                      </td>
                      <td>
                        <div class="row lightGallery lightgallery-without-thumb">
                          <?php if ($value['img_es'] != ""): ?>
                          <a href="../../images/plazas/<?= $value['img_es']?>" class="image-tile"><img src="../../images/plazas/<?= $value['img_es']?>" alt="img <?= $value['nombre']?> es"></a>
                          <?php endif ?>

                          <?php if ($value['img_en'] != ""): ?>
                          <a href="../../images/plazas/<?= $value['img_en']?>" class="image-tile"><img src="../../images/plazas/<?= $value['img_en']?>" alt="img <?= $value['nombre']?> en"></a>
                          <?php endif ?>
                        </div>
                      </td>
                      <td>
                        <?php 
                        $keys_es = explode(",",$value['keywords_es']);
                         ?>
                           <?php foreach ($keys_es as $k_k => $k_v): ?>
                          <label class="badge badge-info mb-1"><?= $k_v ?></label>
                        <?php endforeach ?>
                        <hr>
                        <?php 
                        $keys_en = explode(",",$value['keywords_en']);
                         ?>
                         <?php foreach ($keys_en as $k_k => $k_v): ?>
                          <label class="badge badge-info mb-1"><?= $k_v ?></label>
                        <?php endforeach ?>
                      </td>
                      <td>
                          <?= $value['slug']?>
                     </td>
                     <td>
                      <label class="badge badge-success mb-1">Activo</label>
                    </td>
                    <td>
                      <a href="<?= $root ?>/app/noticias/plaza.php?c=<?= $value['code'] ?>" class="btn btn-primary mb-1 btn-xs"><i class="icon-options-vertical"></i>Detalles</a>
                      <button type="button" class="btn btn-warning mb-1 btn-xs btn-status" data-id="<?= $value['code'] ?>" data-type="6"><i class="fa fa-times"></i>Desactivar</button>
                    </td>
                  </tr>
                <?php endforeach ?>

              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>



  </div>
</div>

<div class="toggle" id="plazas-inactivas" style="display: none;">
    <div class="row">
      <div class="col-md-12">
        <h3 class="text-warning">Plazas Inctivas</h3>
        <div class="space15"></div>
      </div>
      
      <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">

            <div class="table-responsive">
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th>
                      Nombre
                    </th>
                    <th>
                      Descripción
                    </th>
                    <th>
                      Imagen
                    </th>
                    <th>
                      Palabras Clave
                    </th>
                    <th>
                      Slug
                    </th>
                    <th>
                      Status
                    </th>
                    <th>
                      *
                    </th>
                  </tr>
                </thead>
                <tbody>



                  <?php foreach ($plazas_inactivas as $key => $value): ?>

                    <tr>
                      <td>
                        <?= $value['nombre']?>
                      </td>
                      <td>
                        <?= $value['descripcion_es']?>
                        <hr>
                        <?= $value['descripcion_en']?>
                      </td>
                      <td>
                        <div class="row lightGallery lightgallery-without-thumb">
                          <?php if ($value['img_es'] != ""): ?>
                          <a href="../../images/plazas/<?= $value['img_es']?>" class="image-tile"><img src="../../images/plazas/<?= $value['img_es']?>" alt="img <?= $value['nombre']?> es"></a>
                          <?php endif ?>

                          <?php if ($value['img_en'] != ""): ?>
                          <a href="../../images/plazas/<?= $value['img_en']?>" class="image-tile"><img src="../../images/plazas/<?= $value['img_en']?>" alt="img <?= $value['nombre']?> en"></a>
                          <?php endif ?>
                        </div>
                      </td>
                      <td>
                        <?php 
                        $keys_es = explode(",",$value['keywords_es']);
                         ?>
                           <?php foreach ($keys_es as $k_k => $k_v): ?>
                          <label class="badge badge-info mb-1"><?= $k_v ?></label>
                        <?php endforeach ?>
                        <hr>
                        <?php 
                        $keys_en = explode(",",$value['keywords_en']);
                         ?>
                         <?php foreach ($keys_en as $k_k => $k_v): ?>
                          <label class="badge badge-info mb-1"><?= $k_v ?></label>
                        <?php endforeach ?>
                      </td>
                      <td>
                          <?= $value['slug']?>
                     </td>
                     <td>
                      <label class="badge badge-success mb-1">Activo</label>
                    </td>
                    <td>
                      <a href="<?= $root ?>/app/noticias/plaza.php?c=<?= $value['code'] ?>" class="btn btn-primary mb-1 btn-xs"><i class="icon-options-vertical"></i>Detalles</a>
                      <button type="button" class="btn btn-info mb-1 btn-xs btn-status" data-id="<?= $value['code'] ?>" data-type="7"><i class="fa fa-check"></i> Activar</button>
                    </td>
                  </tr>
                <?php endforeach ?>

              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>



  </div>
</div>


</div>











<div class="modal fade" id="add_plaza_modal" tabindex="-1" role="dialog" aria-labelledby="add_plaza_modal" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><i class="icon-location-pin text-primary"></i> Agregar Plaza</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
   <!--      <p class="card-description">
          Ingrese los datos del nuevo usuario.
        </p> -->

        <form class="forms-sample form-ajax" method="POST" action="noticias/add_plaza.php" enctype="multipart/form-data" notif="Plaza creada con éxito" reset="1">

          <div class="row">
            <div class="col-md-10 offset-md-1">
              <div class="form-group">
                <label for="exampleInputName1">Nombre de la plaza</label>
                <input type="text" class="form-control" id="exampleInputName1" placeholder="Nombre" name="nombre" required="">
              </div>
              <br><br>  
            </div>
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="row text-center">
                <i class="flag-icon flag-icon-mx" title="mx" id="mx" style="    font-size: 30px;
                margin: auto;
                margin-bottom: 20px;"></i>
              </div>
              <label for="input_foto_es" class="label-logo" style="width: 100%;">
                <div class="plus_foto_square"><i class="fa fa-plus" aria-hidden="true"></i></div>
                <input type="file" onchange="readURL(this);" accept="image/*" data-target="foto_plaza_es" id="input_foto_es" class="input-img" name="img_es" accept="image/*">
                <img id="foto_plaza_es" src="../../images/art/img.png" alt="Foto perfil" class="preview-img-square">
              </label>



              <div class="form-group">
                <label for="exampleInputName1">Descripción</label>
                <input type="text" class="form-control" id="exampleInputName1" placeholder="Descripción" name="desc_es" required="">
              </div>

              <div class="form-group">
                <label for="exampleInputEmail3">Palabras Clave <small>(Separadas por coma)</small></label>
                <input name="keywords_es" class="tags" value="" />
              </div>

            </div>

            <div class="col-md-6">
              <div class="row text-center">
                <i class="flag-icon flag-icon-us" title="us" id="us" style="    font-size: 30px;
                margin: auto;
                margin-bottom: 20px;"></i>
              </div>
              <label for="input_foto_en" class="label-logo" style="width: 100%;">
                <div class="plus_foto_square"><i class="fa fa-plus" aria-hidden="true"></i></div>
                <input type="file" onchange="readURL(this);" accept="image/*" data-target="foto_plaza_en" id="input_foto_en" class="input-img" name="img_en" accept="image/*">
                <img id="foto_plaza_en" src="../../images/art/img.png" alt="Foto perfil" class="preview-img-square">
              </label>


              
              <div class="form-group">
                <label for="exampleInputName1">Descripción</label>
                <input type="text" class="form-control" id="exampleInputName1" placeholder="Descripcion (Ingles)" name="desc_en" required="">
              </div>

              <div class="form-group">
                <label for="exampleInputEmail3">Palabras Clave <small>(Separadas por coma)</small></label>
                <input name="keywords_en" class="tags" value="" />
              </div>

            </div>
          </div>

          <div class="text-right">
            <button type="submit" class="btn btn-success mr-2">Aceptar</button>
            <button type="button" class="btn btn-light" data-dismiss="modal" onClick="this.form.reset()">Cancelar</button>
          </div>
        </form>

      </div>

    </div>
  </div>
</div>



<link rel="stylesheet" href="../../node_modules/jquery-tags-input/dist/jquery.tagsinput.min.css">
<!-- Plugin js for this page-->
<script src="../../node_modules/jquery-tags-input/dist/jquery.tagsinput.min.js"></script>
<script src="../../js/formpickers.js"></script>
<script src="../../js/form-addons.js"></script>
<!-- <link rel="stylesheet" href="node_modules/icheck/skins/all.css" />
<link rel="stylesheet" href="node_modules/select2/dist/css/select2.min.css" />
<link rel="stylesheet" href="node_modules/select2-bootstrap-theme/dist/select2-bootstrap.min.css" /> -->
<link rel="stylesheet" href="../../node_modules/lightgallery/dist/css/lightgallery.min.css">
<link rel="stylesheet" href="../../node_modules/flag-icon-css/css/flag-icon.min.css" />
<script src="../../node_modules/lightgallery/dist/js/lightgallery-all.min.js"></script>
<script src="../../js/light-gallery.js"></script>
<!-- endinject -->
<!-- Plugin js for this page-->
<script src="../../node_modules/icheck/icheck.min.js"></script>
<script src="../../node_modules/typeahead.js/dist/typeahead.bundle.min.js"></script>
<script src="../../node_modules/select2/dist/js/select2.min.js"></script>
<!-- End plugin js for this page-->
<!-- inject:js -->
<!-- endinject -->
<!-- Custom js for this page-->
<!-- <script src="js/file-upload.js"></script> -->
<script src="../../js/iCheck.js"></script>
<script src="../../js/typeahead.js"></script>
<script src="../../js/select2.js"></script>