
<script>
  $(document).ready(function(){

    $.fn.navbarTitle("Editar Nota");

    // html = $("#navbar-html").html()
    // $.fn.navbarBtn(html);

    $.fn.sidebarContent("hide");

  });
</script>












 




<!-- partial -->
<div class="content-wrapper">

  <div class="row">
    <div class="col-12 grid-margin">
      <div class="card">
        <div class="card-body">
          <!-- <h4 class="card-title">Ingresa el contenido de la nota.</h4> -->
          <form id="nueva-nota-form" class="form-ajax" action="noticias/edit_nota.php" method="POST">
            <div>
              <h3>Detalles</h3>
              <section>
                <h4>Titulos</h4>
                <div class="form-group">
                  <label for="exampleInputEmail1">Titulo de la noticia en Español</label>
                  <input type="text" class="form-control" aria-describedby="emailHelp" placeholder="Titulo" name="titulo_es" value="<?= $nota['titulo_es']?>">
                  <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                </div>
                <div class="form-group" style="display: none;">
                  <label for="exampleInputPassword1">Titulo de la nota en Ingles</label>
                  <input type="text" id="exampleInputPassword1" class="form-control" placeholder="Titulo" name="titulo_en" value="<?= $nota['titulo_en']?>">
                </div>

                <hr>

                <h4>Palabras Clave</h4>
                <div class="form-group">
                  <label for="exampleInputEmail3">Palabras Clave Español<small>(Separadas por coma)</small></label>
                  <input name="keywords_es" class="tags" value="<?= $nota['keywords_es']?>" />
                </div>

                <div class="form-group" style="display: none;">
                  <label for="exampleInputEmail3">Palabras Clave Ingles<small>(Separadas por coma)</small></label>
                  <input name="keywords_en" class="tags" value="<?= $nota['keywords_en']?>" />
                </div>
                <hr> 
                <h4>Etiquetas</h4>
                <div class="form-group">
                   <?php 
                    $arr_etiquetas = getetiquetas($nota['etiquetas']);
                    ?>

                  <select class="js-example-basic-multiple" multiple="multiple" style="width:100%" name="etiquetas[]" autocomplete="off">
                    <?php foreach ($etiquetas as $k => $v): ?>
                      <option value="<?= $v['id']?>" <?=(in_array($v['nombre_es'], $arr_etiquetas)) ? "selected=''" : ''?>><?= $v['nombre_es']?></option>
                    <?php endforeach ?>
                  </select>
                </div>

                       <hr>
                <h4>Contenido</h4>
                <textarea name="contenido_es" id="" cols="30" rows="10" class="summernote"><?= $nota['contenido_es']?></textarea>
<input type="hidden" name="contenido_en">

              </section>

<!-- 
              <h3>Contenido</h3>
              <section>
                <h4>Contenido de la noticia</h4>
                <br>
                <h4>Español</h4>

                <textarea name="contenido_es" id="" cols="30" rows="10" class="summernote"><?= $nota['contenido_es']?></textarea>

                <br><br>
                <h4>Ingles</h4>

                <textarea name="contenido_en" id="" cols="30" rows="10" class="summernote"><?= $nota['contenido_en']?></textarea>

              </section> -->
              <h3>Galeria</h3>
              <section>
                <label for="upload-img" class="btn btn-success">
                  <i class="mdi mdi-cloud-download"></i>Seleccionar imagenes
                </label>
                <input type="file" name="" id="upload-img" class="file_upload input_gallery" data-id="0" data-limit="10" multiple="" data-target="#images_list" data-action="noticias/add_images.php" style="visibility: hidden;">



                <div class="table-responsive">
                  <table class="table table-striped">
                    <thead>
                      <tr>
                        <th>
                          Imagen
                        </th>
                        <th>
                          Descripción (español)
                        </th>
                        <th>
                          Descripción (ingles)
                        </th>


                        <th>
                          *
                        </th>
                      </tr>
                    </thead>
                    <tbody id="images_list">

<?php foreach ($nota_imgs as $a => $b): ?>
  <tr id="img_<?= $b['code']?>">
<td>

<img src="../../images/notas/<?= $b['ruta']?>" alt="img" class="preview-img-nota">
<input type="hidden" name="imagen[]" value="<?= $b['id']?>">

</td>
<td>
<div class="form-group">
<input type="text" id="" class="form-control" name="descripcion_es[]" placeholder="Descripción en Español" value="<?= $b['descripcion_es']?>">
</div>

</td>
<td>
<div class="form-group">
<input type="text" id="" class="form-control" name="descripcion_en[]" placeholder="Descripción Ingles" value="<?= $b['descripcion_en']?>">
</div>
</td>
<td>
<button type="button" class="btn btn-danger mb-1 btn-xs delete_data" data-table="notas_imagenes" data-code="<?= $b['code']?>"><i class="fa fa-times"></i>Eliminar</button>




</td>
</tr>
<?php endforeach ?>


                    </tbody>
                  </table>
                </div>



              </section>
              <h3>Plaza</h3>
              <section>
                <h4>Plaza/Autor</h4>
                <div class="form-group">
                  <label for="select_plaza">Plaza</label>
                  <select class="form-control" id="select_plaza" name="plaza_id">
                    <option>Selecciona la plaza</option>
                    <?php foreach ($plazas as $key => $value): ?>
                      <option value="<?= $value['id']?>" <?=($nota['plaza_id'] == $value['id']) ? "selected=''" : ''?>><?= $value['nombre']?></option>
                    <?php endforeach ?>
                  </select>
                </div>


                <div class="form-group">
                  <label for="select_autor">Seleccionar autor</label>
                  <select class="form-control" id="select_autor" name="autor_id">
                      <?php foreach ($autores as $key => $value): ?>
                      <option value="<?= $value['id']?>" <?=($nota['autor_id'] == $value['id']) ? "selected=''" : ''?>><?= $value['nombre']?></option>
                    <?php endforeach ?>
                  </select>
                </div>

                <hr>

                <h4>Año/Edición</h4>
                <div class="form-group">
                  <label for="select_plaza">Año</label>
                  <select class="form-control" id="" name="edicion_ano">
              
                    <option value="2018" <?=($nota['edicion_ano']==2018) ? 'selected=""' : ''?>>2018</option>                      
                    <option value="2019" <?=($nota['edicion_ano']==2019) ? 'selected=""' : ''?>>2019</option>
                    <option value="2020" <?=($nota['edicion_ano']==2020) ? 'selected=""' : ''?>>2020</option>
                    <option value="2021" <?=($nota['edicion_ano']==2021) ? 'selected=""' : ''?>>2021</option>
                    <option value="2022" <?=($nota['edicion_ano']==2022) ? 'selected=""' : ''?>>2022</option>
                    <option value="2023" <?=($nota['edicion_ano']==2023) ? 'selected=""' : ''?>>2023</option>
                  </select>
                </div>


                <div class="form-group">
                  <label for="select_autor">Edición</label>
                  <select class="form-control" id="" name="edicion_mes">
                    <option value="1" <?=($nota['edicion_mes']==1) ? 'selected=""' : ''?>>Enero</option>
                    <option value="2" <?=($nota['edicion_mes']==2) ? 'selected=""' : ''?>>Febrero</option>
                    <option value="3" <?=($nota['edicion_mes']==3) ? 'selected=""' : ''?>>Marzo</option>
                    <option value="4" <?=($nota['edicion_mes']==4) ? 'selected=""' : ''?>>Abril</option>
                    <option value="5" <?=($nota['edicion_mes']==5) ? 'selected=""' : ''?>>Mayo</option>
                    <option value="6" <?=($nota['edicion_mes']==6) ? 'selected=""' : ''?>>Junio</option>
                    <option value="7" <?=($nota['edicion_mes']==7) ? 'selected=""' : ''?>>Julio</option>
                    <option value="8" <?=($nota['edicion_mes']==8) ? 'selected=""' : ''?>>Agosto</option>
                    <option value="9" <?=($nota['edicion_mes']==9) ? 'selected=""' : ''?>>Septiembre</option>
                    <option value="10" <?=($nota['edicion_mes']==10) ? 'selected=""' : ''?>>Octubre</option>
                    <option value="11" <?=($nota['edicion_mes']==11) ? 'selected=""' : ''?>>Noviembre</option>
                    <option value="12" <?=($nota['edicion_mes']==12) ? 'selected=""' : ''?>>Diciembre</option>
                  </select>
                </div>

  <input type="hidden" name="id" value="<?= $nota['id']?>">


             
              </section>
            </div>
            <button type="submit" id="send_nota" style="visibility: hidden;">send</button>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!--vertical wizard-->

</div>



<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-bs4.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-bs4.js"></script>

<script>
  $(document).ready(function(){
    $('.summernote').summernote();  


  });
</script>

<script>
// // Warning before leaving the page (back button, or outgoinglink)
// window.onbeforeunload = function() {
//  return "Es posible que los cambios no se guarden.";
//    //if we return nothing here (just calling return;) then there will be no pop-up question at all
//    //return;
//  };
</script>

<script src="../../node_modules/jquery-steps/build/jquery.steps.min.js"></script>

<script src="../../node_modules/jquery-validation/dist/jquery.validate.min.js"></script>

<script src="../../js/wizard.js"></script>


<link rel="stylesheet" href="../../node_modules/select2/dist/css/select2.min.css" />

<link rel="stylesheet" href="../../node_modules/jquery-tags-input/dist/jquery.tagsinput.min.css">
<!-- Plugin js for this page-->
<script src="../../node_modules/jquery-tags-input/dist/jquery.tagsinput.min.js"></script>
<script src="../../js/formpickers.js"></script>
<script src="../../js/form-addons.js"></script>
<!-- <link rel="stylesheet" href="node_modules/icheck/skins/all.css" />
<link rel="stylesheet" href="node_modules/select2/dist/css/select2.min.css" />
<link rel="stylesheet" href="node_modules/select2-bootstrap-theme/dist/select2-bootstrap.min.css" /> -->
<link rel="stylesheet" href="../../node_modules/lightgallery/dist/css/lightgallery.min.css">
<link rel="stylesheet" href="../../node_modules/flag-icon-css/css/flag-icon.min.css" />
<script src="../../node_modules/lightgallery/dist/js/lightgallery-all.min.js"></script>
<script src="../../js/light-gallery.js"></script>
<!-- endinject -->
<!-- Plugin js for this page-->
<script src="../../node_modules/icheck/icheck.min.js"></script>
<script src="../../node_modules/typeahead.js/dist/typeahead.bundle.min.js"></script>
<script src="../../node_modules/select2/dist/js/select2.min.js"></script>
<!-- End plugin js for this page-->
<!-- inject:js -->
<!-- endinject -->
<!-- Custom js for this page-->
<!-- <script src="js/file-upload.js"></script> -->
<script src="../../js/iCheck.js"></script>
<script src="../../js/typeahead.js"></script>
<script src="../../js/select2.js"></script>

