
<script>
  $(document).ready(function(){

    $.fn.navbarTitle("Noticias");

    html = $("#navbar-html").html()
    $.fn.navbarBtn(html);

    // $.fn.sidebarContent("hide");

  });
</script>

<div id="navbar-html" style="display: none;">
  <ul class="navbar-nav">
    <li class="nav-item dropdown d-none d-lg-flex">
      <a class="nav-link dropdown-toggle nav-btn" id="actionDropdown" href="#" data-toggle="dropdown">
        <span class="btn">Opciones</span>
      </a>
      <div class="dropdown-menu navbar-dropdown dropdown-left" aria-labelledby="actionDropdown">

        <a class="dropdown-item" href="<?= $root ?>/app/noticias/editar_noticia.php?c=<?= $nota['code']?>">
          <i class="ti-write text-primary"></i>
          Editar Noticia
        </a>

        <div class="dropdown-divider"></div>

        <a class="dropdown-item toggler" href="#" data-toggle="#contenido_ingles" data-this="disabled" data-that="#usuarios-activos-btn" id="usuarios-activos-btn">
          <i class="flag-icon flag-icon-us" title="us" id="us"></i>
          Contenido Ingles
        </a>

        <div class="dropdown-divider"></div>
        <a class="dropdown-item toggler" href="#" data-toggle="#contenido_español" data-this="disabled" data-that="#usuarios-inactivos-btn" id="usuarios-activos-btn">
         <i class="flag-icon flag-icon-mx" title="mx" id="mx"></i>
         Contenido Español
       </a>



     </div>
   </li>
 </ul>
</div>















<!-- partial -->
<div class="content-wrapper">


<div class="row"> 
<div class="col-md-4 grid-margin">
              <div class="card">
                <div class="card-body">
                  <div class="wrapper d-flex align-items-center justify-content-start justify-content-sm-center">
                    <img class="img-md rounded" src="../../images/autores/<?= $nota['avatar']?>" alt="image">
                    <div class="wrapper ml-4">
                      <p class="mb-0 font-weight-medium"><?= $nota['autor']?></p>
                      <!-- <small class="text-muted mb-0"><?= $nota['avatar']?></small> -->
                      <p class="text-success mb-0 font-weight-medium"><?= $nota['plaza']?></p>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          <div class="col-md-4 grid-margin">
              <div class="card">
                <div class="card-body">
                  <div class="d-flex align-items-center justify-content-md-center">
                    <i class="mdi mdi-calendar icon-lg text-success"></i>
                    <div class="ml-3">
                      <p class="mb-0"><?= mes_espanol($nota['edicion_mes'])?></p>
                      <h6><?= $nota['edicion_ano']?></h6>
                    </div>
                  </div>
                </div>
              </div>
            </div>


           <div class="col-md-4 grid-margin">
              <div class="card">
                <div class="card-body">
                  <div class="d-flex align-items-center justify-content-md-center">
                    <i class="mdi mdi-eye icon-lg text-success"></i>
                    <div class="ml-3">
                      <p class="mb-0">239</p>
                      <h6>Vistas</h6>
                    </div>
                  </div>
                </div>
              </div>
            </div>


            </div>


  <div class="toggle" id="contenido_español">
    <div class="row">
      <div class="col-md-12">
        <h3 class="text-primary"><i class="flag-icon flag-icon-mx" title="mx" id="mx"></i> Contenido Español</h3>
        <div class="space15"></div>
      </div>
      
      <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">
            <div class="row">

              <div class="col-12">
                <h2><?= $nota['titulo_es']?></h2>
                <br>

                <div class="row">
                  <div class="col-md-3"></div>
                  <div class="col-md-6">
                <div class="owl-carousel owl-theme full-width">
                  <?php foreach ($nota_imgs as $key => $value): ?>
                    
                  <div class="item">
                    <div class="card text-white">
                      <img class="card-img" src="../../images/notas/<?= $value['ruta']?>" alt="Card image">
                      <div class="card-img-overlay d-flex">
                        <div class="mt-auto text-center w-100">
                          <!-- <h3>Third Slide Label</h3> -->
                          <h6 class="card-text mb-4 font-weight-normal" style="text-shadow: 0px 0px 20px black;"><?= $value['descripcion_es']?></h6>
                        </div>
                      </div>
                    </div>
                  </div>
                  
                  
                  <?php endforeach ?>
                </div>
                </div>
</div>

                <br>
                <br>
                <br>
                <div class="row">
                  <div class="col-md-6">
                   <h5>Palabras clave:</h5>
                   <?php foreach ($kw_es as $key => $value): ?>
                   <label class="badge badge-info"><?= $value ?></label>                                          
                   <?php endforeach ?>
                 </div>

                 <div class="col-md-6">
                   <h5>Categorias:</h5>
                   <?php foreach ($etiquetas as $key => $value): ?>
                   <label class="badge badge-success"><?= $value['nombre_es'] ?></label>                     
                   <?php endforeach ?>

                 </div>
               </div>
               
               <br>
               <hr>
               <h5>Contenido:</h5>
               <div class="contenido">
                <?= $nota['contenido_es']?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>



  </div>
</div>

<div class="toggle" id="contenido_ingles" style="display: none;">
  <div class="row">
      <div class="col-md-12">
        <h3 class="text-primary"><i class="flag-icon flag-icon-us" title="us" id="us"></i> Contenido Ingles</h3>
        <div class="space15"></div>
      </div>
      
      <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">
            <div class="row">

              <div class="col-12">
                <h2><?= $nota['titulo_en']?></h2>
                <br>

                <div class="row">
                  <div class="col-md-3"></div>
                  <div class="col-md-6">
                <div class="owl-carousel owl-theme full-width">
                  <?php foreach ($nota_imgs as $key => $value): ?>
                    
                  <div class="item">
                    <div class="card text-white">
                      <img class="card-img" src="../../images/notas/<?= $value['ruta']?>" alt="Card image">
                      <div class="card-img-overlay d-flex">
                        <div class="mt-auto text-center w-100">
                          <!-- <h3>Third Slide Label</h3> -->
                          <h6 class="card-text mb-4 font-weight-normal" style="text-shadow: 0px 0px 20px black;"><?= $value['descripcion_en']?></h6>
                        </div>
                      </div>
                    </div>
                  </div>
                  
                  
                  <?php endforeach ?>
                </div>
                </div>
</div>

                <br>
                <br>
                <br>
                <div class="row">
                  <div class="col-md-6">
                   <h5>Palabras clave:</h5>
                   <?php foreach ($kw_en as $key => $value): ?>
                   <label class="badge badge-info"><?= $value ?></label>                                          
                   <?php endforeach ?>
                 </div>

                 <div class="col-md-6">
                   <h5>Categorias:</h5>
                   <?php foreach ($etiquetas as $key => $value): ?>
                   <label class="badge badge-success"><?= $value['nombre_en'] ?></label>                     
                   <?php endforeach ?>

                 </div>
               </div>
               
               <br>
               <hr>
               <h5>Contenido:</h5>
               <div class="contenido">
                <?= $nota['contenido_en']?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>



  </div>
</div>


</div>










<style>
  .table td img{
    width: 60px !important;
    height: 60px !important;
    border-radius: 8% !important;
    box-shadow: 0 0 3px 1px #0000003b;
  }

  .card-text{
    text-shadow: 0px 0px 20px black;
    background: #06060694;
    padding: 10px;
    border-radius: 10px;
  }
</style>

  <link rel="stylesheet" href="../../node_modules/owl-carousel-2/assets/owl.carousel.min.css" />

    <link rel="stylesheet" href="../../node_modules/owl-carousel-2/assets/owl.theme.default.min.css" />

  <script src="../../node_modules/owl-carousel-2/owl.carousel.min.js"></script>

  <script src="../../js/owl-carousel.js"></script>


<link rel="stylesheet" href="../../node_modules/datatables.net-bs4/css/dataTables.bootstrap4.css" />

<link rel="stylesheet" href="../../node_modules/jquery-tags-input/dist/jquery.tagsinput.min.css">
<!-- Plugin js for this page-->
<script src="../../node_modules/jquery-tags-input/dist/jquery.tagsinput.min.js"></script>
<script src="../../js/formpickers.js"></script>
<script src="../../js/form-addons.js"></script>
<!-- <link rel="stylesheet" href="node_modules/icheck/skins/all.css" />
<link rel="stylesheet" href="node_modules/select2/dist/css/select2.min.css" />
<link rel="stylesheet" href="node_modules/select2-bootstrap-theme/dist/select2-bootstrap.min.css" /> -->
<link rel="stylesheet" href="../../node_modules/lightgallery/dist/css/lightgallery.min.css">
<link rel="stylesheet" href="../../node_modules/flag-icon-css/css/flag-icon.min.css" />
<script src="../../node_modules/lightgallery/dist/js/lightgallery-all.min.js"></script>
<script src="../../js/light-gallery.js"></script>
<!-- endinject -->
<!-- Plugin js for this page-->
<script src="../../node_modules/icheck/icheck.min.js"></script>
<script src="../../node_modules/typeahead.js/dist/typeahead.bundle.min.js"></script>
<script src="../../node_modules/select2/dist/js/select2.min.js"></script>

<script src="../../node_modules/datatables.net/js/jquery.dataTables.js"></script>

<script src="../../node_modules/datatables.net-bs4/js/dataTables.bootstrap4.js"></script>

<script src="../../js/data-table.js"></script>

<!-- End plugin js for this page-->
<!-- inject:js -->
<!-- endinject -->
<!-- Custom js for this page-->
<!-- <script src="js/file-upload.js"></script> -->
<script src="../../js/iCheck.js"></script>
<script src="../../js/typeahead.js"></script>
<script src="../../js/select2.js"></script>