
<script>
  $(document).ready(function(){

    $.fn.navbarTitle("Nueva Historia de Éxito");

    // html = $("#navbar-html").html()
    // $.fn.navbarBtn(html);

    $.fn.sidebarContent("hide");

  });
</script>

















<!-- partial -->
<div class="content-wrapper">

  <div class="row">
    <div class="col-12 grid-margin">
      <div class="card">
        <div class="card-body">
          <!-- <h4 class="card-title">Ingresa el contenido de la nota.</h4> -->
          <form id="nueva-nota-form" class="form-ajax" action="historias/add_historia.php" method="POST">
            <div>
              <h3>Detalles</h3>
              <section>
                <h4>Titulos</h4>
                <div class="form-group">
                  <label for="exampleInputEmail1">Titulo de la historia en Español</label>
                  <input type="text" class="form-control" aria-describedby="emailHelp" placeholder="Titulo" name="titulo_es" value="">
                  <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Titulo de la historia en Ingles</label>
                  <input type="text" id="exampleInputPassword1" class="form-control" placeholder="Titulo" name="titulo_en" value="">
                </div>

                <hr>

                <h4>Palabras Clave</h4>
                <div class="form-group">
                  <label for="exampleInputEmail3">Palabras Clave Español<small>(Separadas por coma)</small></label>
                  <input name="keywords_es" class="tags" value="" />
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail3">Palabras Clave Ingles<small>(Separadas por coma)</small></label>
                  <input name="keywords_en" class="tags" value="" />
                </div>
                


              </section>


              <h3>Contenido</h3>
              <section>
                <h4>Contenido de la historia</h4>
                <br>
                <h4>Español</h4>

                <textarea name="contenido_es" id="" cols="30" rows="10" class="summernote">contenido es</textarea>

                <br><br>
                <h4>Ingles</h4>

                <textarea name="contenido_en" id="" cols="30" rows="10" class="summernote">contenido en</textarea>



              </section>
              <h3>Galeria</h3>
              <section>
                <label for="upload-img" class="btn btn-success">
                  <i class="mdi mdi-cloud-download"></i>Seleccionar imagenes
                </label>
                <input type="file" name="" id="upload-img" class="file_upload input_gallery" data-id="0" data-limit="10" multiple="" data-target="#images_list" data-action="historias/add_images.php" style="visibility: hidden;">



                <div class="table-responsive">
                  <table class="table table-striped">
                    <thead>
                      <tr>
                        <th>
                          Imagen
                        </th>
                        <th>
                          Descripción (español)
                        </th>
                        <th>
                          Descripción (ingles)
                        </th>


                        <th>
                          *
                        </th>
                      </tr>
                    </thead>
                    <tbody id="images_list">




                    </tbody>
                  </table>
                </div>



              </section>
              <h3>Plaza</h3>
              <section>
                <h4>Plaza/Autor</h4>
                <div class="form-group">
                  <label for="select_plaza">Plaza</label>
                  <select class="form-control" id="select_plaza" name="plaza_id">
                    <option>Selecciona la plaza</option>
                    <?php foreach ($plazas as $key => $value): ?>
                      <option value="<?= $value['id']?>"><?= $value['nombre']?></option>
                    <?php endforeach ?>
                  </select>
                </div>


                <div class="form-group">
                  <label for="select_autor">Seleccionar autor</label>
                  <select class="form-control" id="select_autor" name="autor_id">
                    <option>Favor de seleccionar plaza</option>
                  </select>
                </div>

                <hr>

                <h4>Año/Edición</h4>
                <div class="form-group">
                  <label for="select_plaza">Año</label>
                  <select class="form-control" id="" name="edicion_ano">
                    <?php 
                    if ($month == 12) {
                      $year = $year+1;
                    }
                    ?>
                    <option value="2018" <?=($year==2018) ? 'selected=""' : ''?>>2018</option>                      
                    <option value="2019" <?=($year==2019) ? 'selected=""' : ''?>>2019</option>
                    <option value="2020" <?=($year==2020) ? 'selected=""' : ''?>>2020</option>
                    <option value="2021" <?=($year==2021) ? 'selected=""' : ''?>>2021</option>
                    <option value="2022" <?=($year==2022) ? 'selected=""' : ''?>>2022</option>
                    <option value="2023" <?=($year==2023) ? 'selected=""' : ''?>>2023</option>
                  </select>
                </div>


                <div class="form-group">
                  <label for="select_autor">Edición</label>
                  <select class="form-control" id="" name="edicion_mes">
                    <option value="1" <?=($month+1==13) ? 'selected=""' : ''?>>Enero</option>
                    <option value="2" <?=($month+1==2) ? 'selected=""' : ''?>>Febrero</option>
                    <option value="3" <?=($month+1==3) ? 'selected=""' : ''?>>Marzo</option>
                    <option value="4" <?=($month+1==4) ? 'selected=""' : ''?>>Abril</option>
                    <option value="5" <?=($month+1==5) ? 'selected=""' : ''?>>Mayo</option>
                    <option value="6" <?=($month+1==6) ? 'selected=""' : ''?>>Junio</option>
                    <option value="7" <?=($month+1==7) ? 'selected=""' : ''?>>Julio</option>
                    <option value="8" <?=($month+1==8) ? 'selected=""' : ''?>>Agosto</option>
                    <option value="9" <?=($month+1==9) ? 'selected=""' : ''?>>Septiembre</option>
                    <option value="10" <?=($month+1==10) ? 'selected=""' : ''?>>Octubre</option>
                    <option value="11" <?=($month+1==11) ? 'selected=""' : ''?>>Noviembre</option>
                    <option value="12" <?=($month+1==12) ? 'selected=""' : ''?>>Diciembre</option>
                  </select>
                </div>




             
              </section>
            </div>
            <button type="submit" id="send_nota" style="visibility: hidden;">send</button>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!--vertical wizard-->

</div>



<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-bs4.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-bs4.js"></script>

<script>
  $(document).ready(function(){
    $('.summernote').summernote();  


  });
</script>

<script>
// // Warning before leaving the page (back button, or outgoinglink)
// window.onbeforeunload = function() {
//  return "Es posible que los cambios no se guarden.";
//    //if we return nothing here (just calling return;) then there will be no pop-up question at all
//    //return;
//  };
</script>

<script src="../../node_modules/jquery-steps/build/jquery.steps.min.js"></script>

<script src="../../node_modules/jquery-validation/dist/jquery.validate.min.js"></script>

<script src="../../js/wizard.js"></script>


<link rel="stylesheet" href="../../node_modules/select2/dist/css/select2.min.css" />

<link rel="stylesheet" href="../../node_modules/jquery-tags-input/dist/jquery.tagsinput.min.css">
<!-- Plugin js for this page-->
<script src="../../node_modules/jquery-tags-input/dist/jquery.tagsinput.min.js"></script>
<script src="../../js/formpickers.js"></script>
<script src="../../js/form-addons.js"></script>
<!-- <link rel="stylesheet" href="node_modules/icheck/skins/all.css" />
<link rel="stylesheet" href="node_modules/select2/dist/css/select2.min.css" />
<link rel="stylesheet" href="node_modules/select2-bootstrap-theme/dist/select2-bootstrap.min.css" /> -->
<link rel="stylesheet" href="../../node_modules/lightgallery/dist/css/lightgallery.min.css">
<link rel="stylesheet" href="../../node_modules/flag-icon-css/css/flag-icon.min.css" />
<script src="../../node_modules/lightgallery/dist/js/lightgallery-all.min.js"></script>
<script src="../../js/light-gallery.js"></script>
<!-- endinject -->
<!-- Plugin js for this page-->
<script src="../../node_modules/icheck/icheck.min.js"></script>
<script src="../../node_modules/typeahead.js/dist/typeahead.bundle.min.js"></script>
<script src="../../node_modules/select2/dist/js/select2.min.js"></script>
<!-- End plugin js for this page-->
<!-- inject:js -->
<!-- endinject -->
<!-- Custom js for this page-->
<!-- <script src="js/file-upload.js"></script> -->
<script src="../../js/iCheck.js"></script>
<script src="../../js/typeahead.js"></script>
<script src="../../js/select2.js"></script>

