
<script>
  $(document).ready(function(){

    $.fn.navbarTitle("Configuración");

    // html = $("#navbar-html").html()
    // $.fn.navbarBtn(html);

    // $.fn.sidebarContent("hide");

  });
</script>



<div class="content-wrapper">

  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-body">

          <div class="wrapper d-block d-sm-flex align-items-center justify-content-between">
            <h4 class="card-title mb-0"></h4>
            <ul class="nav nav-tabs tab-solid tab-solid-primary mb-0" id="myTab" role="tablist">
              <li class="nav-item">
                <a class="nav-link active" id="info-tab" data-toggle="tab" href="#info" role="tab" aria-controls="info" aria-expanded="true">Layout</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="avatar-tab" data-toggle="tab" href="#avatar" role="tab" aria-controls="avatar">Mailing</a>
              </li>

            </ul>
          </div>
          <div class="wrapper">
            <hr>
            <div class="tab-content" id="myTabContent">



              <div class="tab-pane fade show active" id="info" role="tabpanel" aria-labelledby="info">

                <div class="row">
                  <div class="col-12">
                    <form class="form-ajax" method="POST" action="general/edit_config_layout.php" notif="Título actualizado con éxito" reset="0">
                      <div class="form-group">
                        <label for="name">Título del Administrador:</label>
                        <input type="text" class="form-control" id="name" name="title" placeholder="Cambiar nombre" required="" value="<?= getConfig('title')?>">
                      </div>

                      <div class="form-group mb-5">
                        <button type="submit" class="btn btn-success mr-2">Actualizar</button>
                        <input class="btn btn-outline-danger" type="reset" value="Cancelar">
                      </div>
                    </form>
                  </div>
                </div>
                <div class="row"> 
                  <div class="col-md-6">  
                    <form class="form-ajax" method="POST" action="general/edit_config_layout.php" notif="Logo actualizado con éxito <p class='whitesmoke'><small>* Puede tardar un momento en aparecer.<small></p>" reset="0"  enctype="multipart/form-data" >
                      <label for="input_foto_logo" class="label-logo">
                        <div class="plus_foto_square"><i class="fa fa-plus" aria-hidden="true"></i></div>


                        <input type="file" onchange="readURL(this);" accept="image/*" data-target="foto_logo" id="input_foto_logo" class="input-img" name="logo" accept="image/*" required="">
                        <img id="foto_logo" src="../../images/<?= getConfig('logo') ?>" alt="Foto perfil" class="preview-img-square">
                      </label>

                      <div class="form-group mt-5">
                        <input type="hidden" name="unlink" value="<?= getConfig('logo') ?>">
                        <button type="submit" class="btn btn-success mr-2">Actualizar Logo</button>
                        <input class="btn btn-outline-danger reset-img" data-src="../../images/<?= getConfig('logo') ?>" data-target="#foto_logo" type="reset" value="Cancelar">
                      </div>
                    </form>
                  </div>



                  <div class="col-md-6">  
                    <form class="form-ajax" method="POST" action="general/edit_config_layout.php" notif="Fondo actualizado con éxito <p class='whitesmoke'><small>* Puede tardar un momento en aparecer.<small></p>" reset="0"  enctype="multipart/form-data" >
                      <label for="input_foto_fondo" class="label-logo">
                        <div class="plus_foto_square"><i class="fa fa-plus" aria-hidden="true"></i></div>


                        <input type="file" onchange="readURL(this);" accept="image/*" data-target="foto_fondo" id="input_foto_fondo" class="input-img" name="background" accept="image/*" required="">
                        <img id="foto_fondo" src="../../images/<?= getConfig('background') ?>" alt="Foto perfil" class="preview-img-square">
                      </label>

                      <div class="form-group mt-5">
                        <input type="hidden" name="unlink" value="<?= getConfig('background') ?>">
                        <button type="submit" class="btn btn-success mr-2">Actualizar Fondo</button>
                        <input class="btn btn-outline-danger reset-img" data-src="../../images/<?= getConfig('background') ?>" data-target="#foto_fondo" type="reset" value="Cancelar">
                      </div>
                    </form>
                  </div>

                </div>
              </div><!-- tab content ends -->










              <div class="tab-pane fade" id="avatar" role="tabpanel" aria-labelledby="avatar-tab">
                <form class="form-ajax" method="POST" action="general/edit_config_layout.php" notif="Datos actualizados con éxito" reset="0">
                  <p class="text-primary">* Por medio de este correo se enviaran los avisos a los usuarios.</p>
                  <div class="form-group">
                    <label for="name">Host</label>
                    <input type="text" class="form-control" id="name" name="mail_host" placeholder="Cambiar nombre" required="" value="<?= getConfig('mail_host')?>">
                  </div>
                  <div class="form-group">
                    <label for="designation">Correo:</label>
                    <input type="email" class="form-control" id="designation" name="mail_correo" placeholder="Cambiar correo" required="" value="<?= getConfig('mail_correo')?>">
                  </div>
                  <div class="form-group">
                    <label for="designation">Contraseña:</label>
                    <input type="text" class="form-control" id="designation" name="mail_password" placeholder="Cambiar correo" required="" value="<?= getConfig('mail_password')?>">
                  </div>
                  <div class="form-group">
                    <label for="designation">Puerto:</label>
                    <input type="text" class="form-control" id="designation" name="mail_port" placeholder="Cambiar correo" required="" value="<?= getConfig('mail_port')?>">
                  </div>
<br>
           <a  data-toggle="modal" data-target="#test_mail_modal" class="btn btn-info mr-2">Probar</a>

                  <div class="form-group mt-5">
                    <button type="submit" class="btn btn-success mr-2">Actualizar</button>
                    <input class="btn btn-outline-danger" type="reset" value="Cancelar">
                  </div>
                </form>
              </div>








            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>




<div class="modal fade" id="test_mail_modal" tabindex="-1" role="dialog" aria-labelledby="test_mail_modal" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><i class="icon-user-follow text-primary"></i> Correo de Prueba</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
   <!--      <p class="card-description">
          Ingrese los datos del nuevo usuario.
        </p> -->

        <form class="forms-sample form-ajax" method="POST" action="general/test_mail.php" enctype="multipart/form-data" notif="Usuario creado con éxito" reset="1">

   
          <div class="form-group">
            <label for="exampleInputName1">Correo al que se enviara la prueba:</label>
            <input type="text" class="form-control" id="exampleInputName1" placeholder="Nombre" name="correo" required="">
          </div>

          <div class="text-right">
            <button type="submit" class="btn btn-success mr-2">Aceptar</button>
            <button type="button" class="btn btn-light" data-dismiss="modal" onClick="this.form.reset()">Cancelar</button>
          </div>
        </form>

      </div>

    </div>
  </div>
</div>




<link rel="stylesheet" href="../../node_modules/icheck/skins/all.css" />

<link rel="stylesheet" href="../../node_modules/dropify/dist/css/dropify.min.css">


<script src="../../node_modules/dropify/dist/js/dropify.min.js"></script>
<script src="../../node_modules/icheck/icheck.min.js"></script>

<script src="../../js/iCheck.js"></script>

<script>
  (function($) {
    'use strict';
    $('.dropify').dropify();
  })(jQuery);

</script>