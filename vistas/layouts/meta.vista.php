<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title><?= $meta_title ?> | <?= getConfig('title')?></title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="../../node_modules/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="../../node_modules/simple-line-icons/css/simple-line-icons.css">
  <link rel="stylesheet" href="../../node_modules/flag-icon-css/css/flag-icon.min.css">
  <link rel="stylesheet" href="../../node_modules/perfect-scrollbar/dist/css/perfect-scrollbar.min.css">
  <!-- endinject -->

  <link rel="stylesheet" href="../../node_modules/jquery-toast-plugin/dist/jquery.toast.min.css">

  
  <!-- plugin css for this page -->
  <link rel="stylesheet" href="../../node_modules/font-awesome/css/font-awesome.min.css" />
  <link rel="stylesheet" href="../../node_modules/jquery-bar-rating/dist/themes/fontawesome-stars.css">
  <link rel="stylesheet" href="../../node_modules/ti-icons/css/themify-icons.css">

  <link rel="stylesheet" href="../../css/general_styles.css">
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="../../css/style.css">
  <link rel="stylesheet" href="../../css/custom.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="../../images/<?= getConfig('logo') ?>" />

  <script src="../../node_modules/jquery/dist/jquery.min.js"></script>
  
</head>
<body class="<?php echo $_SESSION['estilo_sidebar'] ?>">
  <input type="hidden" name="" id="ruta" value="<?php echo $root ?>">
  
    <div class="container-scroller">