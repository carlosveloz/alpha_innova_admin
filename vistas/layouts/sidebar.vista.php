
<!-- partial:partials/_settings-panel.html -->
<div class="theme-setting-wrapper">
  <!-- <div id="settings-trigger"><i class="mdi mdi-settings"></i></div> -->
  <div id="theme-settings" class="settings-panel">
    <i class="settings-close mdi mdi-close"></i>
    <p class="settings-heading">MENÚ LATERAL</p>

    <div class="sidebar-bg-options <?=($_SESSION['estilo_sidebar'] == 'sidebar-light') ? 'selected' : ''?>" id="sidebar-light-theme"><div class="img-ss rounded-circle bg-light border mr-3"></div>Ligero</div>

    <div class="sidebar-bg-options <?=($_SESSION['estilo_sidebar'] == 'sidebar-dark') ? 'selected' : ''?>" id="sidebar-dark-theme"><div class="img-ss rounded-circle bg-dark border mr-3"></div>Obscuro</div>
    
    <p class="settings-heading mt-2">CABECERA</p>
    <div class="color-tiles mx-0 px-4">
      <div class="tiles primary <?=($_SESSION['estilo_navbar'] == 'navbar-primary') ? 'selected' : ''?>" data-theme="navbar-primary"></div>
      <div class="tiles success <?=($_SESSION['estilo_navbar'] == 'navbar-success') ? 'selected' : ''?>" data-theme="navbar-success"></div>
      <div class="tiles warning <?=($_SESSION['estilo_navbar'] == 'navbar-warning') ? 'selected' : ''?>" data-theme="navbar-warning"></div>
      <div class="tiles danger <?=($_SESSION['estilo_navbar'] == 'navbar-danger') ? 'selected' : ''?>" data-theme="navbar-danger"></div>
      <div class="tiles pink <?=($_SESSION['estilo_navbar'] == 'navbar-pink') ? 'selected' : ''?>" data-theme="navbar-pink"></div>
      <div class="tiles info <?=($_SESSION['estilo_navbar'] == 'navbar-info') ? 'selected' : ''?>" data-theme="navbar-info"></div>
      <div class="tiles dark <?=($_SESSION['estilo_navbar'] == 'navbar-dark') ? 'selected' : ''?>" data-theme="navbar-dark"></div>
      <div class="tiles default <?=($_SESSION['estilo_navbar'] == 'navbar-default') ? 'selected' : ''?>" data-theme="navbar-default"></div>
    </div>
  </div>
</div>












<!-- partial:partials/_sidebar.html -->
<nav class="sidebar sidebar-offcanvas" id="sidebar">
  <ul class="nav">
    <li class="nav-item nav-profile">
      <div class="nav-link">
        <div class="profile-image">
          <img src="../../images/avatars/<?= $_SESSION['avatar']?>" alt="image"/>
          <span class="online-status online"></span> <!--change class online to offline or busy as needed-->
        </div>
        <div class="profile-name">
          <p class="name">
            <?= $_SESSION['nombre']?>
          </p>
          <p class="designation">
            <?= getPerfil($_SESSION['perfil'])?>
          </p>
        </div>
      </div>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="<?= $root ?>/app/dashboard">
        <i class="icon-rocket menu-icon"></i>
        <span class="menu-title">Inicio</span>
        <!-- <span class="badge badge-success">New</span> -->
      </a>
    </li>
<!-- 
    <li class="nav-item">
      <a class="nav-link" data-toggle="collapse" href="#notas" aria-expanded="false" aria-controls="notas">
        <i class="icon-note menu-icon"></i>
        <span class="menu-title">Notas</span>
      </a>
      <div class="collapse" id="notas">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item"> <a class="nav-link" href="<?= $root ?>/app/noticias/agregar_noticia.php"> Agregar Noticia </a></li>
          <li class="nav-item"> <a class="nav-link" href="<?= $root ?>/app/noticias/listado.php"> Listado de Noticias</a></li>
          <li class="nav-item"> <a class="nav-link" href="<?= $root ?>/app/noticias/autores.php"> Autores </a></li>
          <li class="nav-item"> <a class="nav-link" href="<?= $root ?>/app/noticias/etiquetas.php"> Etiquetas </a></li>
          <li class="nav-item"> <a class="nav-link" href="<?= $root ?>/app/noticias/plazas.php"> Plazas </a></li>
        </ul>
      </div>
    </li>

    <li class="nav-item">
      <a class="nav-link" data-toggle="collapse" href="#historias-de-exito" aria-expanded="false" aria-controls="historias-de-exito">
        <i class="icon-badge menu-icon"></i>
        <span class="menu-title">Historias de Éxito</span>
      </a>
      <div class="collapse" id="historias-de-exito">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item"> <a class="nav-link" href="<?= $root ?>/app/historias/agregar_historia.php"> Agregar Historia </a></li>
          <li class="nav-item"> <a class="nav-link" href="<?= $root ?>/app/historias/listado_historias.php"> Listado de Historias</a></li>
        </ul>
      </div>
    </li>


    <li class="nav-item">
      <a class="nav-link" data-toggle="collapse" href="#suppliers-book" aria-expanded="false" aria-controls="suppliers-book">
        <i class="icon-briefcase menu-icon"></i>
        <span class="menu-title">Suppliers Book</span>
        <span class="badge badge-primary">2</span>
      </a>
      <div class="collapse" id="suppliers-book">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item"> <a class="nav-link" href="<?= $root ?>/app/empresas/agregar_empresa.php"> Agregar Empresa </a></li>
          <li class="nav-item"> <a class="nav-link" href="<?= $root ?>/app/empresas/listado_empresas.php"> Listado de Empresas</a></li>
          <li class="nav-item"> <a class="nav-link" href="<?= $root ?>/app/empresas/empresas_pendientes.php"> Empresas pendientes</a></li>
          <li class="nav-item"> <a class="nav-link" href="<?= $root ?>/app/empresas/giros.php"> Giros</a></li>
        </ul>
      </div>
    </li>

 -->

<!--     <li class="nav-item">
      <a class="nav-link" data-toggle="collapse" href="#anuncios" aria-expanded="false" aria-controls="anuncios">
        <i class="icon-tag menu-icon"></i>
        <span class="menu-title">Anuncios</span>
      </a>
      <div class="collapse" id="anuncios">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item"> <a class="nav-link" href="pages/samples/error-404.html"> Agregar Empresa </a></li>
          <li class="nav-item"> <a class="nav-link" href="pages/samples/error-404.html"> Listado de Empresas</a></li>
          <li class="nav-item"> <a class="nav-link" href="pages/samples/error-404.html"> Empresas pendientes</a></li>
        </ul>
      </div>
    </li> -->

        <li class="nav-item">
        <a class="nav-link" href="<?= $root ?>/app/webinars">
          <i class="icon-microphone menu-icon"></i>
          <span class="menu-title">Webinars</span>
        </a>
      </li>



    <?php if (in_array($_SESSION['perfil'], array(0,1))): ?>  
      <li class="nav-item">
        <a class="nav-link" href="<?= $root ?>/app/usuarios">
          <i class="icon-people menu-icon"></i>
          <span class="menu-title">Usuarios</span>
        </a>
      </li>
    <?php endif ?>

    <?php if (in_array($_SESSION['perfil'], array(0))): ?>  
      <li class="nav-item">
        <a class="nav-link" href="<?= $root ?>/app/configuracion">
          <i class="icon-settings menu-icon"></i>
          <span class="menu-title">Configuración</span>
        </a>
      </li>
    <?php endif ?>



  </ul>
</nav>











<div id="right-sidebar" class="settings-panel">
  <i class="settings-close mdi mdi-close"></i>
  <ul class="nav nav-tabs" id="setting-panel" role="tablist">
    <li class="nav-item">
      <a class="nav-link active" id="todo-tab" data-toggle="tab" href="#todo-section" role="tab" aria-controls="todo-section" aria-expanded="true">TAREAS</a>
    </li>
 <!--    <li class="nav-item">
      <a class="nav-link" id="chats-tab" data-toggle="tab" href="#chats-section" role="tab" aria-controls="chats-section">CHATS</a>
    </li> -->
  </ul>
  <div class="tab-content" id="setting-content">
    <div class="tab-pane fade show active scroll-wrapper" id="todo-section" role="tabpanel" aria-labelledby="todo-section">
      <div class="add-items d-flex px-3 mb-0">
        <form class="form w-100">
          <div class="form-group d-flex">
            <input type="text" class="form-control todo-list-input" id="input-tarea" placeholder="Agregar tarea">
            <button type="button" class="add btn btn-primary" id="add-task"><i class="fa fa-check"></i></button>
          </div>
        </form>
      </div>
      <div class="list-wrapper px-3">

        <?php 
        $tareas = getTareas($_SESSION['id']);
        ?>

        <ul class="d-flex flex-column-reverse todo-list">

          <?php foreach ($tareas as $key => $value): ?>

           <li class="<?=($value['status'] == 0) ? "completed" : ""?>" id="tarea_<?php echo $value['id'] ?>  ">
            <div class="form-check">
              <label class="form-check-label">
                <input class="checkbox" type="checkbox" <?=($value['status'] == 0) ? "checked" : ""?> onchange="doneTarea(<?php echo $value['id'] ?>)">
                <?= $value['tarea'] ?>
              </label>
            </div>
            <i class="remove mdi mdi-close-circle-outline delete-tarea" data-id="<?php echo $value['id'] ?>" onclick="deleteTarea(<?php echo $value['id'] ?>)"></i>
          </li>

        <?php endforeach ?>

      </ul>
    </div>

  </div>
  <!-- To do section tab ends -->









  <div class="tab-pane fade" id="chats-section" role="tabpanel" aria-labelledby="chats-section">
    <div class="d-flex align-items-center justify-content-between border-bottom">
      <p class="settings-heading border-top-0 mb-3 pl-3 pt-0 border-bottom-0 pb-0">Friends</p>
      <small class="settings-heading border-top-0 mb-3 pt-0 border-bottom-0 pb-0 pr-3 font-weight-normal">See All</small>
    </div>
    <ul class="chat-list">
      <li class="list active">
        <div class="profile"><img src="http://via.placeholder.com/100x100/f4f4f4/000000" alt="image"><span class="online"></span></div>
        <div class="info">
          <p>Thomas Douglas</p>
          <p>Available</p>
        </div>
        <small class="text-muted my-auto">19 min</small>
      </li>
      <li class="list">
        <div class="profile"><img src="http://via.placeholder.com/100x100/f4f4f4/000000" alt="image"><span class="offline"></span></div>
        <div class="info">
          <div class="wrapper d-flex">
            <p>Catherine</p>
          </div>
          <p>Away</p>
        </div>
        <div class="badge badge-success badge-pill my-auto mx-2">4</div>
        <small class="text-muted my-auto">23 min</small>
      </li>
      <li class="list">
        <div class="profile"><img src="http://via.placeholder.com/100x100/f4f4f4/000000" alt="image"><span class="online"></span></div>
        <div class="info">
          <p>Daniel Russell</p>
          <p>Available</p>
        </div>
        <small class="text-muted my-auto">14 min</small>
      </li>
      <li class="list">
        <div class="profile"><img src="http://via.placeholder.com/100x100/f4f4f4/000000" alt="image"><span class="offline"></span></div>
        <div class="info">
          <p>James Richardson</p>
          <p>Away</p>
        </div>
        <small class="text-muted my-auto">2 min</small>
      </li>
      <li class="list">
        <div class="profile"><img src="http://via.placeholder.com/100x100/f4f4f4/000000" alt="image"><span class="online"></span></div>
        <div class="info">
          <p>Madeline Kennedy</p>
          <p>Available</p>
        </div>
        <small class="text-muted my-auto">5 min</small>
      </li>
      <li class="list">
        <div class="profile"><img src="http://via.placeholder.com/100x100/f4f4f4/000000" alt="image"><span class="online"></span></div>
        <div class="info">
          <p>Sarah Graves</p>
          <p>Available</p>
        </div>
        <small class="text-muted my-auto">47 min</small>
      </li>
    </ul>
  </div>
  <!-- chat tab ends -->
</div>
</div>








