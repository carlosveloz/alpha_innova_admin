    <nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row <?php echo $_SESSION['estilo_navbar'] ?>">
      <div class="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center">
        <a class="navbar-brand brand-logo" href="<?php echo  $root ?>/app/dashboard"><img src="../../images/<?= getConfig('logo') ?>" alt="logo"/></a>
        <a class="navbar-brand brand-logo-mini" href="<?php echo  $root ?>/app/dashboard"><img src="../../images/<?= getConfig('logo') ?>" alt="logo"/></a>
      </div>
      <div class="navbar-menu-wrapper d-flex align-items-center">
        <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
          <span class="icon-menu"></span>
        </button>

        <h3 class="mb-0" id="navbar-title"></h3>
        <div id="navbar-options-btn"></div>

        <ul class="navbar-nav navbar-nav-right">
        <!--   <li class="nav-item dropdown d-none d-lg-flex">
            <a class="nav-link dropdown-toggle" id="languageDropdown" href="#" data-toggle="dropdown">
              <i class="flag-icon flag-icon-gb"></i>
              English
            </a>
            <div class="dropdown-menu navbar-dropdown" aria-labelledby="languageDropdown">
              <a class="dropdown-item font-weight-medium" href="#">
                <i class="flag-icon flag-icon-fr"></i>
                French
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item font-weight-medium" href="#">
                <i class="flag-icon flag-icon-es"></i>
                Espanol
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item font-weight-medium" href="#">
                <i class="flag-icon flag-icon-lt"></i>
                Latin
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item font-weight-medium" href="#">
                <i class="flag-icon flag-icon-ae"></i>
                Arabic
              </a>
            </div>
          </li> -->

            <li class="nav-item dropdown">
            <a class="nav-link count-indicator sidebar-open" id="" href="#">
              <i class="icon-notebook"></i>
              <span class="count"></span>
            </a>
          </li>

 

          <li class="nav-item dropdown">
            <a class="nav-link count-indicator dropdown-toggle" id="messageDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
              <i class="icon-user mx-0"></i>
              
            </a>
            <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="messageDropdown">


              <a class="dropdown-item" href="<?= $root ?>/app/usuarios/mi-perfil.php">
                <i class="icon-user text-primary"></i>
                Mi Perfil
              </a>

              <div class="dropdown-divider"></div>

              <a class="dropdown-item settings-open" href="#">
                <i class="ti-ruler-pencil text-warning"></i>
                Estilo
              </a>

              <a class="dropdown-item" data-toggle="modal" data-target="#edit_password_modal">
                <i class="icon-key text-info"></i>
                Cambiar Contraseña
              </a>

              <div class="dropdown-divider"></div>

              <a class="dropdown-item" href="<?php echo $root ?>/logout.php">
                <i class="icon-logout text-primary"></i>
                Cerrar Sesión
              </a>




            </div>
          </li>

          
        </ul>
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
          <span class="icon-menu"></span>
        </button>
      </div>
    </nav>


    <div class="container-fluid page-body-wrapper">
      <div class="row row-offcanvas row-offcanvas-right">



