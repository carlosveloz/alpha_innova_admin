
<script>
  $(document).ready(function(){

    $.fn.navbarTitle("Usuarios");

    html = $("#navbar-html").html()
    $.fn.navbarBtn(html);

    // $.fn.sidebarContent("hide");

  });
</script>

<div id="navbar-html" style="display: none;">
  <ul class="navbar-nav">
    <li class="nav-item dropdown d-none d-lg-flex">
      <a class="nav-link dropdown-toggle nav-btn" id="actionDropdown" href="#" data-toggle="dropdown">
        <span class="btn">Opciones</span>
      </a>
      <div class="dropdown-menu navbar-dropdown dropdown-left" aria-labelledby="actionDropdown">

        <a class="dropdown-item" data-toggle="modal" data-target="#add_user_modal">
          <i class="icon-user-follow text-primary"></i>
          Crear Usuario
        </a>

        <div class="dropdown-divider"></div>

        <a class="dropdown-item toggler" href="#" data-toggle="#usuarios-inactivos" data-this="disabled" data-that="#usuarios-activos-btn" id="usuarios-activos-btn">
          <i class="icon-user-unfollow text-warning"></i>
          Usuarios Inactivos
        </a>

        <a class="dropdown-item toggler" href="#" data-toggle="#usuarios-activos" data-this="disabled" data-that="#usuarios-inactivos-btn" id="usuarios-activos-btn">
          <i class="icon-user-follow text-info"></i>
          Usuarios Activos
        </a>

        <div class="dropdown-divider"></div>

        <a class="dropdown-item" data-toggle="modal" data-target="#add_tarea_general_modal">
          <i class="icon-notebook text-primary"></i>
          Crear Tarea Global
        </a>

      </div>
    </li>
  </ul>
</div>















<!-- partial -->
<div class="content-wrapper">

  <div class="toggle" id="usuarios-activos">
    <div class="row">
      <div class="col-md-12">
        <h3 class="text-primary">Usuarios Activos</h3>
        <div class="space15"></div>
      </div>
      <?php foreach ($usuarios_activos as $key => $value): ?>
        <div class="col-md-4" id="<?= $value['code'] ?>">
          <div class="card rounded mb-2">
            <div class="card-body p-3">
              <div class="media">
                <img src="<?= $root ?>/images/avatars/<?= $value['avatar'] ?>" alt="image" class="img-lg mr-3 rounded-circle">
                <div class="media-body">
                  <h6 class="mb-1"><?= $value['nombre'] ?></h6>
                  <p class="mb-0 text-muted">
                   <?= getPerfil($value['perfil']) ?>
                 </p>
                 <p class="mb-0 text-muted">
                  <?= $value['correo'] ?>
                </p>
                <div class="space10"></div>

                <a href="perfil.php?c=<?= $value['code'] ?>" class="btn btn-primary btn-xs"><i class="icon-user"></i> Perfil</a>

                <button type="button" class="btn btn-warning btn-xs btn-status" onclick="deactivateUser('<?= $value['code'] ?>')" data-id="<?= $value['code'] ?>" data-type="0" id="d_<?= $value['code'] ?>"><i class="icon-user-unfollow"></i> Desactivar</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    <?php endforeach ?>



  </div>
</div>

<div class="toggle" id="usuarios-inactivos" style="display: none;">
  <div class="row">
      <div class="col-md-12">
        <h3 class="text-warning">Usuarios Inactivos</h3>
        <div class="space15"></div>
      </div>
    <?php foreach ($usuarios_inactivos as $key => $value): ?>
      <div class="col-md-4" id="<?= $value['code'] ?>">
        
          <div class="card rounded mb-2">
            <div class="card-body p-3">
              <div class="media">
                <img src="<?= $root ?>/images/avatars/<?= $value['avatar'] ?>" alt="image" class="img-lg mr-3 rounded-circle">
                <div class="media-body">
                  <h6 class="mb-1"><?= $value['nombre'] ?></h6>
                  <p class="mb-0 text-muted">
                   <?= getPerfil($value['perfil']) ?>
                 </p>
                 <p class="mb-0 text-muted">
                  <?= $value['correo'] ?>
                </p>
                <div class="space10"></div>


                <a href="perfil.php?c=<?= $value['code'] ?>" class="btn btn-primary btn-xs"><i class="icon-user"></i> Perfil</a>

                <button type="button" class="btn btn-info btn-xs btn-status" onclick="activateUser('<?= $value['code'] ?>')" data-id="<?= $value['code'] ?>" data-type="1" id="a_<?= $value['code'] ?>"><i class="icon-user"></i> Activar</button>
              </div>
            </div>
          </div>
        </div>
      
      </div>
  <?php endforeach ?>



</div>
</div>


</div>











<div class="modal fade" id="add_user_modal" tabindex="-1" role="dialog" aria-labelledby="add_user_modal" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><i class="icon-user-follow text-primary"></i> Agregar Usuario</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
   <!--      <p class="card-description">
          Ingrese los datos del nuevo usuario.
        </p> -->

        <form class="forms-sample form-ajax" method="POST" action="usuarios/add_user.php" enctype="multipart/form-data" notif="Usuario creado con éxito" reset="1">

          <label for="input_foto_perfil" class="label-avatar">
            <div class="plus_foto_perfil"><i class="fa fa-plus" aria-hidden="true"></i></div>
            <input type="file" onchange="readURL(this);" accept="image/*" data-target="foto_avatar" id="input_foto_perfil" class="input-img" name="avatar" accept="image/*">
            <img id="foto_avatar" src="../../images/avatars/avatar.png" alt="Foto perfil" class="preview-img">
          </label>


          <div class="form-group">
            <label for="exampleInputName1">Nombre</label>
            <input type="text" class="form-control" id="exampleInputName1" placeholder="Nombre" name="nombre" required="">
          </div>

          <div class="form-group">
            <label for="exampleInputEmail3">Correo</label>
            <input type="email" class="form-control" id="exampleInputEmail3" placeholder="Email" name="correo" required="">
          </div>

          <div class="form-group">
            <label for="exampleInputPassword4">Contraseña</label>
            <input type="text" class="form-control" id="exampleInputPassword4" placeholder="Password" name="password" required="">
          </div>

          <div class="form-group">
            <label for="exampleFormControlSelect2">Tipo de perfil</label>
            <select class="form-control" id="" name="perfil">
              <option value="1">Admin</option>
              <option value="2">Editor</option>
              <!-- <option>Super Admin</option> -->
            </select>
          </div>


          <div class="text-right">
            <button type="submit" class="btn btn-success mr-2">Aceptar</button>
            <button type="button" class="btn btn-light" data-dismiss="modal" onClick="this.form.reset()">Cancelar</button>
          </div>
        </form>

      </div>

    </div>
  </div>
</div>









<div class="modal fade" id="add_tarea_general_modal" tabindex="-1" role="dialog" aria-labelledby="add_tarea_general_modal" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><i class="icon-notebook text-primary"></i> Agregar Tarea Global</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
   <!--      <p class="card-description">
          Ingrese los datos del nuevo usuario.
        </p> -->

        <form class="forms-sample form-ajax" method="POST" action="general/add_tarea_global.php" enctype="multipart/form-data" notif="Tarea global creada con éxito" reset="1">

          <div class="form-group">
            <label for="exampleFormControlSelect2">Tipo de perfil</label>
            <select class="form-control" id="" name="perfil">
              <option value="0">Todos</option>
              <option value="1">Admin</option>
              <option value="2">Editor</option>
              <!-- <option>Super Admin</option> -->
            </select>
          </div>

           <div class="form-group">
            <label for="exampleInputName1">Tarea</label>
            <input type="text" class="form-control" id="exampleInputName1" placeholder="Escribe la tarea a agregar." name="tarea" required="">
          </div>


          <div class="text-right">
            <button type="submit" class="btn btn-success mr-2">Aceptar</button>
            <button type="button" class="btn btn-light" data-dismiss="modal" onClick="this.form.reset()">Cancelar</button>
          </div>
        </form>

      </div>

    </div>
  </div>
</div>



<!-- <link rel="stylesheet" href="node_modules/icheck/skins/all.css" />
<link rel="stylesheet" href="node_modules/select2/dist/css/select2.min.css" />
<link rel="stylesheet" href="node_modules/select2-bootstrap-theme/dist/select2-bootstrap.min.css" /> -->



<!-- endinject -->
<!-- Plugin js for this page-->
<script src="../../node_modules/icheck/icheck.min.js"></script>
<script src="../../node_modules/typeahead.js/dist/typeahead.bundle.min.js"></script>
<script src="../../node_modules/select2/dist/js/select2.min.js"></script>
<!-- End plugin js for this page-->
<!-- inject:js -->

<!-- endinject -->
<!-- Custom js for this page-->
<!-- <script src="js/file-upload.js"></script> -->
<script src="../../js/iCheck.js"></script>
<script src="../../js/typeahead.js"></script>
<script src="../../js/select2.js"></script>