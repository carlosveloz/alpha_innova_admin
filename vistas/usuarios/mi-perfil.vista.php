
<script>
  $(document).ready(function(){

    $.fn.navbarTitle("Mi Perfil");

    // html = $("#navbar-html").html()
    // $.fn.navbarBtn(html);

    // $.fn.sidebarContent("hide");

  });
</script>



<div class="content-wrapper">
  <div class="row user-profile">
    <div class="col-lg-4 side-left d-flex align-items-stretch">
      <div class="row">
        <div class="col-12 grid-margin stretch-card">
          <div class="card">
            <div class="card-body avatar">
              <h4 class="card-title">Info</h4>
              <img src="../../images/avatars/<?= $usuario['avatar'] ?>" alt="">
              <span class="online-status online"></span>
              <p class="name"><?= $usuario['nombre'] ?></p>
              <p class="designation"><?= getPerfil($usuario['perfil']) ?></p>
              <a class="d-block text-center text-dark" href="#"><?= $usuario['correo'] ?></a>
              <!-- <a class="d-block text-center text-dark" href="#">+1 9438 934089</a> -->
            </div>
          </div>
        </div>

        <div class="col-12 stretch-card">
          <div class="card">
            <div class="card-body overview">
              <ul class="achivements">
                <li><p>34</p><p>Proyectos</p></li>
                <li><p>23</p><p>Tareas</p></li>
                <li><p>29</p><p>Sesiones</p></li>
              </ul>
              <div class="wrapper about-user">
                <h4 class="card-title mt-4 mb-3">Acerca de</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veniam consectetur ex quod.</p>
              </div>
              <div class="info-links">
                <a class="website" href="http://urbanui.com/">
                  <i class="mdi mdi-earth text-gray"></i>
                  <span>http://urbanui.com/</span>
                </a>
                <a class="social-link" href="#">
                  <i class="mdi mdi-facebook text-gray"></i>
                  <span>https://www.facebook.com/johndoe</span>
                </a>
                <a class="social-link" href="#">
                  <i class="mdi mdi-linkedin text-gray"></i>
                  <span>https://www.linkedin.com/johndoe</span>
                </a>
              </div>
            </div>
          </div>
        </div>


      </div>
    </div>
    <div class="col-lg-8 side-right stretch-card">
      <div class="card">
        <div class="card-body">
          <div class="wrapper d-block d-sm-flex align-items-center justify-content-between">
            <h4 class="card-title mb-0">Detalles</h4>
            <ul class="nav nav-tabs tab-solid tab-solid-primary mb-0" id="myTab" role="tablist">
              <li class="nav-item">
                <a class="nav-link active" id="info-tab" data-toggle="tab" href="#info" role="tab" aria-controls="info" aria-expanded="true">Información</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="avatar-tab" data-toggle="tab" href="#avatar" role="tab" aria-controls="avatar">Avatar</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="security-tab" data-toggle="tab" href="#security" role="tab" aria-controls="security">Seguridad</a>
              </li>
            </ul>
          </div>
          <div class="wrapper">
            <hr>
            <div class="tab-content" id="myTabContent">
              <div class="tab-pane fade show active" id="info" role="tabpanel" aria-labelledby="info">


                  <div class="form-group">
                    <h5>Nombre:</h5>
                    <p><?= $usuario['nombre']?></p>
                  </div>
                  <div class="form-group">
                    <h5>Correo:</h5>
                    <p><?= $usuario['correo']?></p>
                  </div>
                  <div class="form-group">
                    <h5>Tipo de perfil:</h5>
                    <p><?php echo getPerfil($usuario['perfil']) ?></p>
                  </div>
                  <div class="form-group">
                    <h5>Estatus:</h5>
                      <p><?= ($usuario['status'] == 1) ? '<div class="badge badge-success">Activo</div>' : '<div class="badge badge-warning">Inactivo</div>'?></p>
                  </div>
                  <p class="text-right text-muted"><small>* Para modificar tu información contacta al administrador del sistema.</small></p>

               
              </div><!-- tab content ends -->
              <div class="tab-pane fade" id="avatar" role="tabpanel" aria-labelledby="avatar-tab">
                       <!--  <div class="wrapper mb-5 mt-4">
                          <span class="badge badge-warning text-white">Note : </span>
                          <p class="d-inline ml-3 text-muted">Image size is limited to not greater than 1MB .</p>
                        </div> -->
                        <form class="form-ajax" method="POST" action="usuarios/edit_avatar.php" notif="Avatar de usuario actualizado con éxito <p class='whitesmoke'><small>* Puede tardar un momento en aparecer.<small></p>" reset="0"  enctype="multipart/form-data" >
                          <label for="input_foto_perfil" class="label-avatar">
                            <div class="plus_foto_perfil"><i class="fa fa-plus" aria-hidden="true"></i></div>
                            <input type="file" onchange="readURL(this);" accept="image/*" data-target="foto_avatar" id="input_foto_perfil" class="input-img" name="avatar" accept="image/*" required="">
                            <img id="foto_avatar" src="../../images/avatars/<?= $usuario['avatar'] ?>" alt="Foto perfil" class="preview-img">
                          </label>

                          <input type="hidden" name="id" value="<?= $usuario['id']?>">

                          <div class="form-group mt-5">
                            <button type="submit" class="btn btn-success mr-2">Actualizar</button>
                            <input class="btn btn-outline-danger reset-img" data-src="../../images/avatars/<?= $usuario['avatar'] ?>" data-target="#foto_avatar" type="reset" value="Cancelar">
                          </div>
                        </form>
                      </div>
                      <div class="tab-pane fade" id="security" role="tabpanel" aria-labelledby="security-tab">
                        <form class="form-ajax" method="POST" action="usuarios/edit_password_by_admin.php" notif="Contraseña actualizada con éxito" reset="1">
                          <div class="form-group">
                            <label for="change-password">Cambiar contaseña:</label>
                            <input type="text" class="form-control" name="password" id="change-password" placeholder="Ingresar contraseña nueva." required="">
                          </div>
                          <div class="form-group">
                            <input type="text" class="form-control" name="re_password" id="new-password" placeholder="Reingresar contraseña" required="">
                          </div>
                          <div class="form-group mt-5">
                            <input type="hidden" name="code" value="<?= $usuario['code']?>">

                            <button type="submit" class="btn btn-success mr-2">Actualizar</button>
                            <input class="btn btn-outline-danger" type="reset" value="Cancelar">
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>


        <link rel="stylesheet" href="../../node_modules/icheck/skins/all.css" />

        <link rel="stylesheet" href="../../node_modules/dropify/dist/css/dropify.min.css">


        <script src="../../node_modules/dropify/dist/js/dropify.min.js"></script>
        <script src="../../node_modules/icheck/icheck.min.js"></script>

        <script src="../../js/iCheck.js"></script>

        <script>
          (function($) {
            'use strict';
            $('.dropify').dropify();
          })(jQuery);

        </script>