

<script>
  $(document).ready(function(){

    $.fn.navbarTitle("Giros de Empresas");

    html = $("#navbar-html").html()
    $.fn.navbarBtn(html);

    // $.fn.sidebarContent("hide");

  });
</script>

<div id="navbar-html" style="display: none;">
  <ul class="navbar-nav">
    <li class="nav-item dropdown d-none d-lg-flex">
      <a class="nav-link dropdown-toggle nav-btn" id="actionDropdown" href="#" data-toggle="dropdown">
        <span class="btn">Opciones</span>
      </a>
      <div class="dropdown-menu navbar-dropdown dropdown-left" aria-labelledby="actionDropdown">

        <a class="dropdown-item" data-toggle="modal" data-target="#add_giro_modal">
          <i class="icon-rocket text-primary"></i>
          Agregar Giro
        </a>



      </div>
    </li>
  </ul>
</div>





<div class="content-wrapper">
  <div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">Giros</h4>
                     <div class="table-responsive table-striped">
           
                    <table class="table">
                      <thead>
                        <tr>
                          <th>Imagen</th>
                          <th>Nombre</th>
                          <th># Empresas</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php foreach ($empresas_giros as $k => $v): ?>
                          
                        <tr id="emp_giros_<?= $v['code']?>">
                          <td>
                              <div class="row lightGallery lightgallery-without-thumb">
                            <a href="../../images/empresas/giros/<?= $v['imagen']?>" class="image-tile"><img src="../../images/empresas/giros/<?= $v['imagen']?>" alt="IMG <?= $v['imagen']?>"></a>
                          </div>

                          </td>
                          <td><?= $v['nombre'] ?></td>
                           <td><label class="badge badge-success">250</label></td>
                          <td><button type="button" class="btn btn-warning mb-1 btn-xs get_data" data-table="empresas_giros" data-code="<?= $v['code']?>">Editar</button></td>
                        </tr>
                        <?php endforeach ?>
                        
                      </tbody>
                    </table>
                  </div>
        </div>
      </div>
    </div>
    
    
    
  </div>
</div>






<div class="modal fade" id="add_giro_modal" tabindex="-1" role="dialog" aria-labelledby="add_giro_modal" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><i class="icon-rocket text-primary"></i>&nbsp; Agregar Giro</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
   <!--      <p class="card-description">
          Ingrese los datos del nuevo usuario.
        </p> -->

        <form class="forms-sample form-ajax" method="POST" action="empresas/add_giro.php" enctype="multipart/form-data" notif="Giro agregado con éxito" reset="1">

          <label for="input_portada" class="label-avatar">
            <div class="plus_foto_perfil"><i class="fa fa-plus" aria-hidden="true"></i></div>
            <input type="file" onchange="readURL(this);" accept="image/*" data-target="foto_avatar" id="input_portada" class="input-img" name="imagen" accept="image/*">
            <img id="foto_avatar" src="../../images/empresas/giros/default.png" alt="imagen" class="preview-img">
          </label>


          <div class="form-group">
            <label for="exampleInputName1">Nombre</label>
            <input type="text" class="form-control" id="exampleInputName1" placeholder="nombre" name="nombre" required="">
          </div>

                    


          <div class="text-right">
            <button type="submit" class="btn btn-success mr-2">Aceptar</button>
            <button type="button" class="btn btn-light" data-dismiss="modal" onClick="this.form.reset()">Cancelar</button>
          </div>
        </form>

      </div>

    </div>
  </div>
</div>



<div class="modal fade" id="edit_giro_modal" tabindex="-1" role="dialog" aria-labelledby="edit_giro_modal" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><i class="icon-rocket text-primary"></i>&nbsp; Editar Giro</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
   <!--      <p class="card-description">
          Ingrese los datos del nuevo usuario.
        </p> -->

        <form class="forms-sample form-ajax" method="POST" action="empresas/edit_giro.php" enctype="multipart/form-data" notif="Giro modificado con éxito. Recargar para visualizar." reset="1">

          <label for="input_edit_img" class="label-avatar">
            <div class="plus_foto_perfil"><i class="fa fa-plus" aria-hidden="true"></i></div>
            <input type="file" onchange="readURL(this);" accept="image/*" data-target="edit_img" id="input_edit_img" class="input-img" name="imagen" accept="image/*">
            <img id="edit_img" src="../../images/empresas/giros/default.png" alt="imagen" class="preview-img">
          </label>


          <div class="form-group">
            <label for="exampleInputName1">Nombre</label>
            <input type="text" class="form-control" id="edit_nombre" placeholder="nombre" name="nombre" required="">
          </div>

                    
  <input type="hidden" name="code" id="edit_code">

          <div class="text-right">
            <button type="submit" class="btn btn-success mr-2">Aceptar</button>
            <button type="button" class="btn btn-light" data-dismiss="modal" onClick="this.form.reset()">Cancelar</button>
          </div>
        </form>

      </div>

    </div>
  </div>
</div>



<link rel="stylesheet" href="../../node_modules/lightgallery/dist/css/lightgallery.min.css">

<script src="../../node_modules/lightgallery/dist/js/lightgallery-all.min.js"></script>

<script src="../../js/light-gallery.js"></script>





