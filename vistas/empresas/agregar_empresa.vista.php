

<script>
  $(document).ready(function(){

    $.fn.navbarTitle("Agregar Empresa a Suppliers Book");

    // html = $("#navbar-html").html()
    // $.fn.navbarBtn(html);

    // $.fn.sidebarContent("hide");

  });
</script>

<!-- <div id="navbar-html" style="display: none;">
  <ul class="navbar-nav">
    <li class="nav-item dropdown d-none d-lg-flex">
      <a class="nav-link dropdown-toggle nav-btn" id="actionDropdown" href="#" data-toggle="dropdown">
        <span class="btn">Opciones</span>
      </a>
      <div class="dropdown-menu navbar-dropdown dropdown-left" aria-labelledby="actionDropdown">

        <a class="dropdown-item" data-toggle="modal" data-target="#add_giro_modal">
          <i class="icon-rocket text-primary"></i>
          Agregar Giro
        </a>



      </div>
    </li>
  </ul>
</div> -->





<div class="content-wrapper">
  <div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h3>Datos:</h3>


          <form class="forms-sample form-ajax" method="POST" action="empresas/add_empresa.php" enctype="multipart/form-data" notif="Empresa agregada con éxito" reset="0">
            <div class="row">
              <div class="col-md-4">
                <br><br>
                <label for="input_portada" class="label-avatar">
                  <div class="plus_foto_perfil"><i class="fa fa-plus" aria-hidden="true"></i></div>
                  <input type="file" onchange="readURL(this);" accept="image/*" data-target="foto_avatar" id="input_portada" class="input-img" name="logo" accept="image/*">
                  <img id="foto_avatar" src="../../images/empresas/empresas_logos/logo.png" alt="logo" class="preview-img">
                </label>
              </div>
              <div class="col-md-8">

                <div class="form-group">
                  <label for="exampleInputName1">Nombre:</label>
                  <input type="text" class="form-control" id="exampleInputName1" placeholder="Nombre" name="nombre" required="">
                </div>

                <div class="form-group">
                  <label for="exampleInputName1">Slogan:</label>
                  <input type="text" class="form-control" id="exampleInputName1" placeholder="Slogan" name="slogan">
                </div>

                <div class="form-group">
                  <label for="exampleInputName1">Descripción:</label>
                  <textarea id="" class="form-control" rows="3" placeholder="Descripción" name="descripcion_emp" required=""></textarea>
                </div>

                <div class="form-group">
                  <select class="js-example-basic-multiple" multiple="multiple" style="width:100%" name="giros[]" autocomplete="off">
                    <?php foreach ($empresas_giros as $k => $v): ?>
                      <option value="<?= $v['id']?>"><?= $v['nombre']?></option>
                    <?php endforeach ?>
                  </select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <hr>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="exampleInputName1">Correo:</label>
                  <input type="text" class="form-control" id="exampleInputName1" placeholder="Correo" name="correo">
                </div>

                <div class="form-group">
                  <label for="exampleInputName1">Teléfono:</label>
                  <input type="text" class="form-control" id="exampleInputName1" placeholder="Teléfono" name="telefono">
                </div>


                <div class="form-group">
                  <label for="exampleFormControlSelect2">Estado:</label>
                  <select class="form-control" id="lista_estados" name="estado_id" required="">
                    <option value="" selected="" disabled="">Seleccionar Estado</option>
                    <?php foreach ($estados as $key => $value): ?>
                      <option value="<?php echo $value['id']?>"><?php echo $value['nombre']?></option>
                    <?php endforeach ?>
                  </select>
                </div>


                <div class="form-group">
                  <label for="exampleFormControlSelect2">Municipio:</label>
                  <select class="form-control" id="lista_municipios" name="municipio_id" required=""></select>
                </div>

                <div class="form-group">
                  <label for="exampleInputName1">Dirección:</label>
                  <input type="text" class="form-control" id="exampleInputName1" placeholder="Dirección" name="direccion">
                </div>


              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label for="exampleInputName1">Pagina web:</label>
                  <input type="text" class="form-control" id="exampleInputName1" placeholder="Web" name="url">
                </div>

                <div class="form-group">
                  <label for="exampleInputName1">Facebook:</label>
                  <input type="text" class="form-control" id="exampleInputName1" placeholder="Facebook page" name="facebook">
                </div>

                <div class="form-group">
                  <label for="exampleInputName1">Twitter:</label>
                  <input type="text" class="form-control" id="exampleInputName1" placeholder="Twitter" name="twitter">
                </div>

                <div class="form-group">
                  <label for="exampleInputName1">Instagram:</label>
                  <input type="text" class="form-control" id="exampleInputName1" placeholder="Instagram" name="instagram">
                </div>

                <div class="form-group">
                  <label for="exampleInputName1">LinkedIn:</label>
                  <input type="text" class="form-control" id="exampleInputName1" placeholder="LinkedIn" name="linkedin">
                </div>

              </div>
            </div>

            <div class="row">
              <div class="col-md-12">
                <hr>
              </div>

              <div class="col-md-12">
                <h3>Galería</h3>
                
                <label for="upload-img" class="btn btn-success">
                  <i class="mdi mdi-cloud-download"></i>Seleccionar imagenes
                </label>
                <input type="file" name="" id="upload-img" class="file_upload input_gallery" data-id="0" data-limit="10" multiple="" data-target="#images_list" data-action="empresas/add_images.php" style="visibility: hidden;">



                <div class="table-responsive">
                  <table class="table table-striped">
                    <thead>
                      <tr>
                        <th>
                          Imagen
                        </th>
                        <th>
                          Descripción
                        </th>



                        <th>
                          *
                        </th>
                      </tr>
                    </thead>
                    <tbody id="images_list">




                    </tbody>
                  </table>
                </div>




              </div>
            </div>


            <div class="text-right">
              <br><br>
              <button type="submit" class="btn btn-success mr-2">Aceptar</button>
              <button type="button" class="btn btn-light" data-dismiss="modal" onClick="this.form.reset()">Cancelar</button>
            </div>
          </form>

        </div>
      </div>
    </div>
    
    
    
  </div>
</div>



<link rel="stylesheet" href="../../node_modules/select2/dist/css/select2.min.css" />
<script src="../../node_modules/select2/dist/js/select2.min.js"></script>
<script src="../../js/select2.js"></script>
