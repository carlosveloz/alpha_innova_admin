

<script>
  $(document).ready(function(){

    $.fn.navbarTitle("Suppliers Book");

    html = $("#navbar-html").html()
    $.fn.navbarBtn(html);

    // $.fn.sidebarContent("hide");

  });
</script>

<div id="navbar-html" style="display: none;">
  <ul class="navbar-nav">
    <li class="nav-item dropdown d-none d-lg-flex">
      <a class="nav-link dropdown-toggle nav-btn" id="actionDropdown" href="#" data-toggle="dropdown">
        <span class="btn">Opciones</span>
      </a>
      <div class="dropdown-menu navbar-dropdown dropdown-left" aria-labelledby="actionDropdown">

        <a class="dropdown-item" href="<?= $root ?>/app/empresas/agregar_empresa.php">
          <i class="icon-briefcase text-primary"></i>
          Agregar Empresa
        </a>



      </div>
    </li>
  </ul>
</div>





<div class="content-wrapper">
  <div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">Empresas</h4>
          <div class="table-responsive table-striped">

            <table class="table">
              <thead>
                <tr>
                  <th>Logo</th>
                  <th>Nombre</th>
                  <th>Giros</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($empresas as $k => $v): ?>

                  <tr id="emp_<?= $v['code']?>">
                    <td>
                      <div class="row lightGallery lightgallery-without-thumb">
                        <a href="../../images/empresas/empresas_logos/<?= $v['logo']?>" class="image-tile"><img src="../../images/empresas/empresas_logos/<?= $v['logo']?>" alt="IMG <?= $v['logo']?>"></a>
                      </div>

                    </td>
                    <td><?= $v['nombre'] ?></td>

                    <?php 
                    $giros = getgiros($v['giros'])
                    ?>
                    <td>
                      <?php foreach ($giros as $a => $b): ?>
                        <label class="badge badge-success"><?= $b ?></label>
                      <?php endforeach ?>
                    </td>
                    <td>
                     <a href="<?= $root ?>/app/empresas/detalles_empresa.php?c=<?= $v['code'] ?>" class="btn btn-primary mb-1 btn-xs"><i class="icon-options-vertical"></i>Detalles</a>
                      <button type="button" class="btn btn-warning mb-1 btn-xs btn-status" data-id="<?= $v['code'] ?>" data-type="8"><i class="fa fa-check"></i> Desactivar</button>
                    </td>
                  </tr>
                <?php endforeach ?>

              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    
    
    
  </div>
</div>














<link rel="stylesheet" href="../../node_modules/lightgallery/dist/css/lightgallery.min.css">

<script src="../../node_modules/lightgallery/dist/js/lightgallery-all.min.js"></script>

<script src="../../js/light-gallery.js"></script>





