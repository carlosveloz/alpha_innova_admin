

<script>
  $(document).ready(function(){

    $.fn.navbarTitle("Detalles de Empresa (Suppliers Book)");

    html = $("#navbar-html").html()
    $.fn.navbarBtn(html);

    // $.fn.sidebarContent("hide");

  });
</script>

<div id="navbar-html" style="display: none;">
  <ul class="navbar-nav">
    <li class="nav-item dropdown d-none d-lg-flex">
      <a class="nav-link dropdown-toggle nav-btn" id="actionDropdown" href="#" data-toggle="dropdown">
        <span class="btn">Opciones</span>
      </a>
      <div class="dropdown-menu navbar-dropdown dropdown-left" aria-labelledby="actionDropdown">

          <a class="dropdown-item toggler" href="#" data-toggle="#editar" data-this="disabled" data-that="#usuarios-inactivos-btn" id="usuarios-activos-btn">
          <i class="icon-user-follow text-info"></i>
          Editar Empresa
        </a>



      </div>
    </li>
  </ul>
</div>





<div class="content-wrapper">
  <div class="toggle" id="detalles">

  <div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h3>Datos:</h3>


          <form class="forms-sample form-ajax" method="POST" action="empresas/add_empresa.php" enctype="multipart/form-data" notif="Empresa agregada con éxito" reset="0">
            <div class="row">
              <div class="col-md-4">
                <br><br>
                  <img id="foto_avatar" src="../../images/empresas/empresas_logos/<?= $empresa['logo']?>" alt="logo" style="width:60%;display:block;margin:auto;">
              </div>
              <div class="col-md-8">

                <div class="form-group">
                  <label for="exampleInputName1"><strong>Nombre:</strong></label>
                  <p><?= $empresa['nombre']?></p>
                </div>

                <div class="form-group">
                  <label for="exampleInputName1"><strong>Slogan:</strong></label>
                  <p><?= $empresa['slogan']?></p>
                </div>
 
                <div class="form-group">
                  <label for="exampleInputName1"><strong>Descripción:</strong></label>
                  <p><?= $empresa['descripcion']?></p>
                </div>

                <div class="form-group">
                 <label for=""><strong>Giros:</strong></label><br>
                    <?php 
                    $giros = getgiros($empresa['giros'])
                    ?>
                    <td>
                      <?php foreach ($giros as $a => $b): ?>
                        <label class="badge badge-success"><?= $b ?></label>
                      <?php endforeach ?>
                    </td>
                </div>

                <?php if ($empresa['status'] == 1): ?>
                  <label class="badge badge-info"><i class="fa fa-check"></i> Activa</label>
                <?php else: ?>
                  <label class="badge badge-warning"><i class="fa fa-times"></i> Inactiva</label>
                <?php endif ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <hr>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="exampleInputName1"><strong>Correo:</strong></label>
                  <p><?= $empresa['correo']?></p>
                </div>

                <div class="form-group">
                  <label for="exampleInputName1"><strong>Teléfono:</strong></label>
                  <p><?= $empresa['telefono']?></p>
                </div>


                <div class="form-group">
                  <label for="exampleFormControlSelect2"><strong>Estado:</strong></label>
                  <p><?= getestado($empresa['estado_id'])?></p>
                </div>


                <div class="form-group">
                  <label for="exampleFormControlSelect2"><strong>Municipio:</strong></label>
                  <p><?= getmunicipio($empresa['municipio_id'])?></p>
                </div>

                <div class="form-group">
                  <label for="exampleInputName1"><strong>Dirección:</strong></label>
                  <p><?= $empresa['direccion']?></p>
                </div>


              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label for="exampleInputName1"><strong>Pagina web:</strong></label>
                  <p><a href="<?= $empresa['url']?>"><?= $empresa['url']?></a></p>
                </div>

                <div class="form-group">
                  <label for="exampleInputName1"><strong>Facebook:</strong></label>
                  <p><a href="<?= $empresa['facebook']?>"><?= $empresa['facebook']?></a></p>
                </div>

                <div class="form-group">
                  <label for="exampleInputName1"><strong>Twitter:</strong></label>
                  <p><a href="<?= $empresa['twitter']?>"><?= $empresa['twitter']?></a></p>
                </div>

                <div class="form-group">
                  <label for="exampleInputName1"><strong>Instagram:</strong></label>
                  <p><a href="<?= $empresa['instagram']?>"><?= $empresa['instagram']?></a></p>
                  
                </div>

                <div class="form-group">
                   <label for="exampleInputName1"><strong>Linkedin:</strong></label>
                 <p> <a href="<?= $empresa['linkedin']?>"><?= $empresa['linkedin']?></a></p>
                </div>

              </div>
            </div>

            <div class="row">
              <div class="col-md-12">
                <hr>
              </div>

              <div class="col-md-12">
                <h3>Galería</h3>
                
                       <div class="row lightGallery lightgallery-without-thumb">
                       <?php foreach ($empresas_imagenes as $key => $value): ?>  
                        <a href="../../images/empresas/empresas_imagenes/<?= $value['ruta']?>" class="image-tile" style="width: 100%;"><img src="../../images/empresas/empresas_imagenes/<?= $value['ruta']?>" alt="<?= $value['descripcion']?>" style="width: 100%;"></a>
                       <?php endforeach ?>
                      </div>


              </div>
            </div>


            
          </form>

        </div>
      </div>
    </div>
    
    
    
  </div>
  </div>




  <div class="toggle" id="editar" style="display: none;">

  <div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h3>Editar Datos:</h3>


          <form class="forms-sample form-ajax" method="POST" action="empresas/edit_empresa.php" enctype="multipart/form-data" notif="Empresa actualizada con éxito" reset="0">
            
            <div class="row">
              <div class="col-md-4">
                <br><br>
                <label for="input_portada" class="label-avatar">
                  <div class="plus_foto_perfil"><i class="fa fa-plus" aria-hidden="true"></i></div>
                  <input type="file" onchange="readURL(this);" accept="image/*" data-target="foto_avatar" id="input_portada" class="input-img" name="logo" accept="image/*">
                  <img id="foto_avatar" src="../../images/empresas/empresas_logos/<?= $empresa['logo']?>" alt="logo" class="preview-img">
                </label>
              </div>
              <div class="col-md-8">

                <div class="form-group">
                  <label for="exampleInputName1">Nombre:</label>
                  <input type="text" class="form-control" id="exampleInputName1" placeholder="Nombre" name="nombre" required="" value="<?= $empresa['nombre']?>">
                </div>

                <div class="form-group">
                  <label for="exampleInputName1">Slogan:</label>
                  <input type="text" class="form-control" id="exampleInputName1" placeholder="Slogan" name="slogan" value="<?= $empresa['slogan']?>">
                </div>

                <div class="form-group">
                  <label for="exampleInputName1">Descripción:</label>
                  <textarea id="" class="form-control" rows="3" placeholder="Descripción" name="descripcion_emp" required=""><?= $empresa['descripcion']?></textarea>
                </div>
      
                <div class="form-group">
                  <select class="js-example-basic-multiple" multiple="multiple" style="width:100%" name="giros[]" autocomplete="off">
                    <?php foreach ($empresas_giros as $k => $v): ?>
                      <option value="<?= $v['id']?>" <?=(in_array($v['nombre'], $giros)) ? "selected=''" : ''?>><?= $v['nombre']?></option>
                    <?php endforeach ?>
                  </select>
                </div>

                 <div class="icheck-square">
                        <input tabindex="6" type="checkbox" id="square-checkbox-2" <?=($empresa['status'] == 1) ? "checked" : ''?> name="status" value="1">
                        <label for="square-checkbox-2">Activo</label>
                      </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <hr>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="exampleInputName1">Correo:</label>
                  <input type="text" class="form-control" id="exampleInputName1" placeholder="Correo" name="correo" value="<?= $empresa['correo']?>">
                </div>

                <div class="form-group">
                  <label for="exampleInputName1">Teléfono:</label>
                  <input type="text" class="form-control" id="exampleInputName1" placeholder="Teléfono" name="telefono" value="<?= $empresa['telefono']?>">
                </div>


                <div class="form-group">
                  <label for="exampleFormControlSelect2">Estado:</label>
                  <select class="form-control" id="lista_estados" name="estado_id" required="">
                    <option value="" disabled="">Seleccionar Estado</option>
                    <?php foreach ($estados as $key => $value): ?>
                      <option value="<?php echo $value['id']?>" <?=($empresa['estado_id'] == $value['id']) ? "selected=''" : ''?>><?php echo $value['nombre']?></option>
                    <?php endforeach ?>
                  </select>
                </div>


                <div class="form-group">
                  <label for="exampleFormControlSelect2">Municipio:</label>
                  <select class="form-control" id="lista_municipios" name="municipio_id" required="">
                    <?php foreach ($municipios as $key => $value): ?>
                      <option value="<?php echo $value['id']?>" <?=($empresa['municipio_id'] == $value['id']) ? "selected=''" : ''?>><?php echo $value['nombre']?></option>
                    <?php endforeach ?>
                  </select>
                </div>

                <div class="form-group">
                  <label for="exampleInputName1">Dirección:</label>
                  <input type="text" class="form-control" id="exampleInputName1" placeholder="Dirección" name="direccion" value="<?= $empresa['direccion']?>">
                </div>


              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label for="exampleInputName1">Pagina web:</label>
                  <input type="text" class="form-control" id="exampleInputName1" placeholder="Web" name="url" value="<?= $empresa['url']?>">
                </div>

                <div class="form-group">
                  <label for="exampleInputName1">Facebook:</label>
                  <input type="text" class="form-control" id="exampleInputName1" placeholder="Facebook page" name="facebook" value="<?= $empresa['facebook']?>">
                </div>

                <div class="form-group">
                  <label for="exampleInputName1">Twitter:</label>
                  <input type="text" class="form-control" id="exampleInputName1" placeholder="Twitter" name="twitter" value="<?= $empresa['twitter']?>">
                </div>

                <div class="form-group">
                  <label for="exampleInputName1">Instagram:</label>
                  <input type="text" class="form-control" id="exampleInputName1" placeholder="Instagram" name="instagram" value="<?= $empresa['instagram']?>">
                </div>

                <div class="form-group">
                  <label for="exampleInputName1">LinkedIn:</label>
                  <input type="text" class="form-control" id="exampleInputName1" placeholder="LinkedIn" name="linkedin" value="<?= $empresa['linkedin']?>">
                </div>

              </div>
            </div>

            <div class="row">
              <div class="col-md-12">
                <hr>
              </div>

              <div class="col-md-12">
                <h3>Galería</h3>
                
                <label for="upload-img" class="btn btn-success">
                  <i class="mdi mdi-cloud-download"></i>Seleccionar imagenes
                </label>
                <input type="file" name="" id="upload-img" class="file_upload input_gallery" data-id="0" data-limit="10" multiple="" data-target="#images_list" data-action="empresas/add_images.php" style="visibility: hidden;">



                <div class="table-responsive">
                  <table class="table table-striped">
                    <thead>
                      <tr>
                        <th>
                          Imagen
                        </th>
                        <th>
                          Descripción
                        </th>



                        <th>
                          *
                        </th>
                      </tr>
                    </thead>
                    <tbody id="images_list">

<?php foreach ($empresas_imagenes as $key => $value): ?>
  <tr id="img_<?= $value['code']?>">
<td>

<img src="../../images/empresas/empresas_imagenes/<?= $value['ruta']?>" alt="img" class="preview-img-nota">
<input type="hidden" name="imagen[]" value="<?= $value['id']?>">

</td>
<td>
<div class="form-group">
<input type="text" id="" class="form-control" name="descripcion[]" placeholder="Descripción" value="<?= $value['descripcion']?>">
</div>

</td>

<td>
<button type="button" class="btn btn-danger mb-1 btn-xs delete_data" data-table="empresas_imagenes" data-code="<?= $value['code']?>"><i class="fa fa-times"></i>Eliminar</button>




</td>
</tr>
<?php endforeach ?>


                    </tbody>
                  </table>
                </div>




              </div>
            </div>


            <div class="text-right">
              <br><br>
              <input type="hidden" name="id" value="<?= $empresa['id']?>">
              <button type="submit" class="btn btn-success mr-2">Aceptar</button>
              <button type="button" class="btn btn-light toggler" href="#" data-toggle="#detalles" data-dismiss="modal" onClick="this.form.reset()">Cancelar</button>
            </div>
          
          </form>

        </div>
      </div>
    </div>
    
    
    
  </div>
  </div>





</div>



<link rel="stylesheet" href="../../node_modules/select2/dist/css/select2.min.css" />
<script src="../../node_modules/select2/dist/js/select2.min.js"></script>
<script src="../../js/select2.js"></script>

<link rel="stylesheet" href="../../node_modules/lightgallery/dist/css/lightgallery.min.css">

<script src="../../node_modules/lightgallery/dist/js/lightgallery-all.min.js"></script>

<script src="../../js/light-gallery.js"></script>


  <link rel="stylesheet" href="../../node_modules/icheck/skins/all.css" />

  <script src="../../node_modules/icheck/icheck.min.js"></script>

  <script src="../../js/iCheck.js"></script>
