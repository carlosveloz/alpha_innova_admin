<?php 

$conexion = conexion();



$statement = $conexion->prepare(
	'SELECT * FROM empresas WHERE code = :code'
);
$statement->execute(array(':code' => $_GET['c']));
$empresa = $statement->fetch();


$statement = $conexion->prepare(
	'SELECT * FROM empresas_imagenes WHERE empresa_id = :empresa_id'
);
$statement->execute(array(':empresa_id' => $empresa['id'] ));
$empresas_imagenes = $statement->fetchAll();



$statement = $conexion->prepare(
	'SELECT * FROM empresas_giros'
);
$statement->execute();
$empresas_giros = $statement->fetchAll();

$estados= getestados();
$municipios= getmunicipios($empresa['estado_id']);



require '../../vistas/layouts/navbar.vista.php';
require '../../vistas/layouts/sidebar.vista.php';
require '../../vistas/empresas/detalles_empresa.vista.php';
require '../../vistas/layouts/footer.vista.php';
 ?>