<?php 


function mails($data,$type,$root){

  switch ($type) {
    case 1:

    $layout= '
    <!DOCTYPE html>
    <html lang="en">
    <head>
    <meta charset="UTF-8">
    <title>Document</title>
    </head>
    <body style="margin: 0;">
    <table width="100%" height="100%" border="0" cellpadding="20" cellspacing="0" style="margin:0;border-collapse:collapse;border-spacing:0;padding:20px;font-family: Arial;">
    <tr>
    <td width="100%" valign="top" align="center" bgcolor="#eceeed">
    <table border="0" cellpadding="0" cellspacing="0" class="body" style="margin:0;padding:0;border-collapse:collapse;border-spacing:0;width:100%;background:#fff;max-width:780px;margin:0px;border-radius:5px;">
    <tr style="margin:0;padding:0;border-collapse:collapse;border-spacing:0;">
    <td class="container" style="padding:0;border-collapse:collapse;border-spacing:0;">
    <div class="content" style="margin:0;padding:0;">
    <table class="main" style="margin:0;padding:0;border-collapse:collapse;border-spacing:0;">
    <tr style="margin:0;padding:0;border-collapse:collapse;border-spacing:0;">
    <td class="wrapper" style="margin:0;padding:0;border-collapse:collapse;border-spacing:0;">
    <table border="0" cellpadding="0" cellspacing="0" style="margin:0;padding:0;border-collapse:collapse;border-spacing:0;">
    <tr style="margin:0;padding:0;border-collapse:collapse;border-spacing:0;">
    <td style="margin:0;padding:0;border-collapse:collapse;border-spacing:0;">
    <div class="headerimage" style="margin:0;padding:0;">
    <br>
    <img src="'.$root.'/images/'.getConfigR('logo').'" alt="logo" style="margin:0;padding:0;border-radius:5px 5px 0px 0px;overflow:hidden;width: 50%; display: block; margin:auto;" width="100%" />
    <br>
    </div>
    <div class="in-content" style="margin:0;padding:0;padding:30px;">
    <h3 style="margin:0;font-family:Helvetica, Arial;line-height:1.4;color:#3f526d;font-weight:500;font-size:20px;margin:20px 0px 15px;padding:0;">Este correo se a enviado como prueba para el sitio: '.getConfigR('title').'. El recibirlo indica que los datos de mailing son funcionales.</h3>
  

    <br>

    <table border="0" cellpadding="0" cellspacing="0" class="" width="100%" style="margin:0;padding:0;border-collapse:collapse;border-spacing:0;">
    <tbody style="margin:0;padding:0;">
    <tr style="margin:0;padding:0;border-collapse:collapse;border-spacing:0;">
    <td align="center" style="margin:0;padding:0;border-collapse:collapse;border-spacing:0;">
    <table border="0" cellpadding="0" cellspacing="0" style="margin:0;padding:0;border-collapse:collapse;border-spacing:0;">
    <tbody style="margin:0;padding:0;">
    <tr style="margin:0;padding:0;border-collapse:collapse;border-spacing:0;">
    <td style="margin:0;padding:0;border-collapse:collapse;border-spacing:0;">
    <div style="margin:0;padding:0;text-align: center;width: 100%;">
    <a href="'.$root.'" target="_blank" class="cta-button" style="margin:0;padding:0;display:inline-block;background:#ee662e;color:#fff;text-decoration:none;padding:15px 25px;border-radius:5px;font-size:14px;letter-spacing:1px;font-weight:100;margin:20px auto;">Ir a sitio</a>                                              </div>
    </td>
    </tr>
    </tbody>
    </table>
    </td>
    </tr>
    </tbody>
    </table>
    </div>
    </td>
    </tr>
    </table>
    </td>
    </tr>
    </table>
    </div>
    </td>
    </tr>
    </table>
    </td>
    </tr>
    <tr>
    <td width="100%" valign="top" align="center" bgcolor="#eceeed">
    <table style="margin:0;padding:0;border-collapse:collapse;border-spacing:0;width: 100%;max-width: 550px;margin: 0px auto;text-align: center;color: #ccc;font-size: 14px;" align="center">
    <tr style="margin:0;padding:0;border-collapse:collapse;border-spacing:0;">
    <td style="margin:0;padding:0;border-collapse:collapse;border-spacing:0;">&copy; 2019 '.getConfigR('title').'</td>
    </tr>
    </table>
    </td>
    </tr>
    </table>





    </body>
    </html>';

    return $layout;
    break;

    case 2:
         $layout= '
    <!DOCTYPE html>
    <html lang="en">
    <head>
    <meta charset="UTF-8">
    <title>Document</title>
    </head>
    <body style="margin: 0;">
    <table width="100%" height="100%" border="0" cellpadding="20" cellspacing="0" style="margin:0;border-collapse:collapse;border-spacing:0;padding:20px;font-family: Arial;">
    <tr>
    <td width="100%" valign="top" align="center" bgcolor="#eceeed">
    <table border="0" cellpadding="0" cellspacing="0" class="body" style="margin:0;padding:0;border-collapse:collapse;border-spacing:0;width:100%;background:#fff;max-width:780px;margin:0px;border-radius:5px;">
    <tr style="margin:0;padding:0;border-collapse:collapse;border-spacing:0;">
    <td class="container" style="padding:0;border-collapse:collapse;border-spacing:0;">
    <div class="content" style="margin:0;padding:0;">
    <table class="main" style="margin:0;padding:0;border-collapse:collapse;border-spacing:0;">
    <tr style="margin:0;padding:0;border-collapse:collapse;border-spacing:0;">
    <td class="wrapper" style="margin:0;padding:0;border-collapse:collapse;border-spacing:0;">
    <table border="0" cellpadding="0" cellspacing="0" style="margin:0;padding:0;border-collapse:collapse;border-spacing:0;">
    <tr style="margin:0;padding:0;border-collapse:collapse;border-spacing:0;">
    <td style="margin:0;padding:0;border-collapse:collapse;border-spacing:0;">
    <div class="headerimage" style="margin:0;padding:0;">
    <br>
    <img src="'.$root.'/images/'.getConfigR('logo').'" alt="logo" style="margin:0;padding:0;border-radius:5px 5px 0px 0px;overflow:hidden;width: 50%; display: block; margin:auto;" width="100%" />
    <br>
    </div>
    <div class="in-content" style="margin:0;padding:0;padding:30px;">
    <h3 style="margin:0;font-family:Helvetica, Arial;line-height:1.4;color:#3f526d;font-weight:500;font-size:20px;margin:20px 0px 15px;padding:0;">Tu registro en '.getConfigR('title').' a sido completado.</h3>
    <p style="margin:0;padding:0;font-family:Helvetica, Arial;margin-bottom:10px;font-weight:300;color:#5f5f5f;font-size:15px;line-height:1.6;">Buenos días <strong>Carlos R.</strong> Hemos recibido tu información y tu cuenta está lista.
    Para confirmar tu correo electronico por favor ingresa al siguiente link
    <br>
    <br>
    <a href="">http://test.com/</a>

    <br>
    <br>
    <strong>Saludos cordiales.</strong></p>

    <br>

    <table border="0" cellpadding="0" cellspacing="0" class="" width="100%" style="margin:0;padding:0;border-collapse:collapse;border-spacing:0;">
    <tbody style="margin:0;padding:0;">
    <tr style="margin:0;padding:0;border-collapse:collapse;border-spacing:0;">
    <td align="center" style="margin:0;padding:0;border-collapse:collapse;border-spacing:0;">
    <table border="0" cellpadding="0" cellspacing="0" style="margin:0;padding:0;border-collapse:collapse;border-spacing:0;">
    <tbody style="margin:0;padding:0;">
    <tr style="margin:0;padding:0;border-collapse:collapse;border-spacing:0;">
    <td style="margin:0;padding:0;border-collapse:collapse;border-spacing:0;">
    <div style="margin:0;padding:0;text-align: center;width: 100%;">
    <a href="'.$root.'" target="_blank" class="cta-button" style="margin:0;padding:0;display:inline-block;background:#ee662e;color:#fff;text-decoration:none;padding:15px 25px;border-radius:5px;font-size:14px;letter-spacing:1px;font-weight:100;margin:20px auto;">Ir a sitio</a>                                              </div>
    </td>
    </tr>
    </tbody>
    </table>
    </td>
    </tr>
    </tbody>
    </table>
    </div>
    </td>
    </tr>
    </table>
    </td>
    </tr>
    </table>
    </div>
    </td>
    </tr>
    </table>
    </td>
    </tr>
    <tr>
    <td width="100%" valign="top" align="center" bgcolor="#eceeed">
    <table style="margin:0;padding:0;border-collapse:collapse;border-spacing:0;width: 100%;max-width: 550px;margin: 0px auto;text-align: center;color: #ccc;font-size: 14px;" align="center">
    <tr style="margin:0;padding:0;border-collapse:collapse;border-spacing:0;">
    <td style="margin:0;padding:0;border-collapse:collapse;border-spacing:0;">&copy; 2019 inline.mx</td>
    </tr>
    </table>
    </td>
    </tr>
    </table>





    </body>
    </html>';

    return $layout;
        break;
    // case label3:
    //     code to be executed if n=label3;
    //     break;
    // ...
    // default:
    //     code to be executed if n is different from all labels;
  }


}

?>