<?php 
// $root = "http://localhost/victory_admin_php";

require_once "parameters.php";



function conexion(){
	try {
		$db_data= parameters();

		$conexion = new PDO("mysql:host=$db_data->server;port=3306;dbname=$db_data->database;charset=UTF8", $db_data->user,$db_data->password);
		return $conexion;
	} catch (PDOException $e) {
		return false;
	}
}
$conexion = conexion();

date_default_timezone_set('Mexico/General');
$hoy = date('Y-m-d H:i:s', time());
$year = date('Y', time());
$month = date('m', time());

function fecha_espanol($date){
// Este es por si la fecha esta en formato dd/mm/yyyy
	$date = DateTime::createFromFormat('d/m/Y', $date);
	$timestamp = $date->format('Y-m-d');
// hasta aqui
	date_default_timezone_set('UTC');
	date_default_timezone_set("America/Mexico_City");
	setlocale(LC_TIME, 'spanish');
    //$hora = strftime("%I:%M:%S %p", strtotime($timestamp));Descomentar en caso de requerir hora
	// $fecha = utf8_encode(strftime("%A %d de %B del %Y", strtotime($timestamp)));
	$fecha = utf8_encode(strftime("%d/%B/%Y", strtotime($timestamp)));
    return $fecha ;//$hora; concatenar con fecha para obtener fecha y hora
}


function mes_espanol($mes){
    	switch ($mes) {
		case 1:
		$r = "Enero";
		break;
		case 2:
		$r = "Febrero";
		break;
		case 3:
		$r = "Marzo";
		break;
		case 4:
		$r = "Abril";
		break;
		case 5:
		$r = "Mayo";
		break;
		case 6:
		$r = "Junio";
		break;
		case 7:
		$r = "Julio";
		break;
		case 8:
		$r = "Agosto";
		break;
		case 9:
		$r = "Septiembre";
		break;
		case 10:
		$r = "Octubre";
		break;
		case 11:
		$r = "Noviembre";
		break;
		case 12:
		$r = "Diciembre";
		break;
	}
	return $r;
}



function quitar_acentos($cadena){
    $originales = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿ';
    $modificadas = 'aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuyyby';
    $cadena = utf8_decode($cadena);
    $cadena = strtr($cadena, utf8_decode($originales), $modificadas);
    return utf8_encode($cadena);
}




function getPerfil($n){
	switch ($n) {
		case 0:
		$r = "SUPERADMIN";
		break;
		case 1:
		$r = "Administrador";
		break;
		case 2:
		$r = "Editor";
		break;
	}
	return $r;
}


function auth($p){
	$conexion = conexion();
	$root = parameters()->root;

	if (empty($_SESSION)) {
		header("Location: $root");
	}
	if ($p != "x") {

		if (!in_array($_SESSION['perfil'], $p))
		{
			header("Location: $root"."/403.php");			
		}

	}
}


function getConfig($n){
	$conexion = conexion();
	$statement = $conexion->prepare("
		SELECT value FROM config WHERE data = '$n'"
	);
	$statement->execute();
	$config = $statement->fetch();
	echo $config['value'];
}

function getConfigR($n){
	$conexion = conexion();
	$statement = $conexion->prepare("
		SELECT value FROM config WHERE data = '$n'"
	);
	$statement->execute();
	$config = $statement->fetch();
	return $config['value'];
}



function getTareas($n){
	$conexion = conexion();
	$statement = $conexion->prepare("
		SELECT * FROM tareas where usuario_id = :usuario_id"
	);
	$statement->execute(array(':usuario_id' => $n ));
	$tareas = $statement->fetchAll();
	return $tareas;
}





function getestados(){
	$conexion = conexion();
	$statement = $conexion->prepare("
		SELECT * FROM estados"
	);
	$statement->execute();
	$estados = $statement->fetchAll();
	return $estados;
}



function getestado($id){

	$conexion = conexion();


	$statement = $conexion->prepare("
		SELECT nombre FROM estados WHERE id = :id"
	);
	$statement->execute(array(
		':id' => $id
	));

	$estado = $statement->fetch();

	return $estado['nombre'];
}


function getmunicipio($id){

	$conexion = conexion();


	$statement = $conexion->prepare("
		SELECT nombre FROM municipios WHERE id = :id"
	);
	$statement->execute(array(
		':id' => $id
	));

	$estado = $statement->fetch();

	return $estado['nombre'];
}

function getmunicipios($estado_id){

	$conexion = conexion();


	$statement = $conexion->prepare("
		SELECT * FROM municipios WHERE estado_id = :estado_id"
	);
	$statement->execute(array(
		':estado_id' => $estado_id
	));

	$municipios = $statement->fetchAll();

	return $municipios;
}

function getimpresasbyplaza($plaza_id){

	$conexion = conexion();

$statement = $conexion->prepare('SELECT * FROM versiones_impresas WHERE plaza_id = :plaza_id ORDER BY id DESC');
$statement->execute(array(
	':plaza_id' => $plaza_id
));
$resultado = $statement->fetchAll();
	return $resultado;

}

function getgiros($value_giros){

	$conexion = conexion();
 $arr_giros = explode(",",$value_giros);


                    $giros = array();
                    foreach ($arr_giros as $key => $value) {
                      if ($value != "") {
                        $value = str_replace("'","",$value);
                        $statement = $conexion->prepare(
                          "SELECT nombre FROM empresas_giros WHERE id = :id"
                        );
                        $statement->execute(array(':id' => $value));
                        $et = $statement->fetch();

                        array_push($giros, $et['nombre']);
                      }
                    }
	return $giros;

}


function getetiquetas($value_etiquetas){

	$conexion = conexion();
 $arr_etiquetas = explode(",",$value_etiquetas);


                    $etiquetas = array();
                    foreach ($arr_etiquetas as $key => $value) {
                      if ($value != "") {
                        $value = str_replace("'","",$value);
                        $statement = $conexion->prepare(
                          "SELECT nombre_es FROM etiquetas WHERE id = :id"
                        );
                        $statement->execute(array(':id' => $value));
                        $et = $statement->fetch();

                        array_push($etiquetas, $et['nombre_es']);
                      }
                    }
	return $etiquetas;

}






function reduceImagen($imgUrl,$size){
	list($ancho, $alto,$extension) = getimagesize($imgUrl);

	if($ancho > $size){
		// Obtener nuevas dimensiones
		$porcentaje = $ancho/$size;
		$nancho = $ancho /$porcentaje;
		$nalto = $alto /$porcentaje;
		$extension = image_type_to_mime_type($extension);

		//Crear instancia de nueva imagen
		$newformimagen = imagecreatetruecolor($nancho, $nalto);

		switch ($extension) {
			case 'image/gif':
			$source=imagecreatefromgif($imgUrl);
			imagecopyresampled($newformimagen, $source, 0, 0, 0, 0, $nancho, $nalto, $ancho, $alto);

			imagegif($newformimagen,$source);
			break;			 
			case "image/pjpeg": 
			case "image/jpeg": 
			case "image/jpg": 
			// Redimensionar Imagen JPG

			$source = imagecreatefromjpeg($imgUrl); 
			imagecopyresampled($newformimagen, $source, 0, 0, 0, 0, $nancho, $nalto, $ancho, $alto);

				// Redimensionar 
			imagejpeg($newformimagen,$imgUrl); 
			break;

			case 'png':
			// Redimensionar Imagen PNG
			$source=imagecreatefrompng($imgUrl);
			$normal = imagecolorallocate($newformimagen, 0, 0, 0);
			imagecolortransparent($newformimagen, $normal);
			imagefilledrectangle($newformimagen, 0, 0, $nancho, $nalto, 0);

			imagecopyresampled($newformimagen, $source, 0, 0, 0, 0, $nancho, $nalto, $ancho, $alto);

				// Mostrar la nueva imagen 
			imagepng($newformimagen, $imgUrl);
			break;
		}

		chmod($imgUrl, 0777); 

	}
}




function enviar_correo($receptor,$data,$type,$root){


	require 'PHPMailer/PHPMailerAutoload.php';
	require 'mails.php';

	$message = mails($data,1,$root);
	$mail = new PHPMailer;

//$mail->SMTPDebug = 3;                               
// Enable verbose debug output

$mail->isSMTP();                                      // Set mailer to use SMTP
$mail->Host = getConfigR('mail_host');  // Specify main and backup SMTP servers
$mail->SMTPAuth = true;                               // Enable SMTP authentication
$mail->Username = getConfigR('mail_correo');                 // SMTP username
$mail->Password = getConfigR('mail_password');                           // SMTP password
$mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
$mail->Port = getConfigR('mail_port');                                    // TCP port to connect to
$mail->CharSet = 'UTF-8';

$mail->setFrom(getConfigR('mail_correo'), 'Admin '.getConfigR('title'));
    $mail->addAddress($receptor, '');     // Add a recipient
    $mail->isHTML(true);             
    $mail->Subject = 'TEST MAIL';
    $mail->Body    = $message;

    if(!$mail->send()) {
    	echo 'Error, mensaje no enviado.<br>Por favor verifique los datos';
    	die;
    	// echo 'Error del mensaje: ' . $mail->ErrorInfo;
    } else {
    	

    }
}

?>