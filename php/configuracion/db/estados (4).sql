-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 09-01-2020 a las 20:14:22
-- Versión del servidor: 5.5.61-38.13-log
-- Versión de PHP: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `rentolhl_ruber`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estados`
--

CREATE TABLE `estados` (
  `id` int(11) NOT NULL,
  `clave` varchar(2) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `abrev` varchar(16) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Tabla de Estados de la República Mexicana';

--
-- Volcado de datos para la tabla `estados`
--

INSERT INTO `estados` (`id`, `clave`, `nombre`, `abrev`) VALUES
(1, '01', 'Aguascalientes', 'Ags.'),
(2, '02', 'Baja California', 'BC'),
(3, '03', 'Baja California Sur', 'BCS'),
(4, '04', 'Campeche', 'Camp.'),
(5, '05', 'Coahuila de Zaragoza', 'Coah.'),
(6, '06', 'Colima', 'Col.'),
(7, '07', 'Chiapas', 'Chis.'),
(8, '08', 'Chihuahua', 'Chih.'),
(9, '09', 'Distrito Federal', 'DF'),
(10, '10', 'Durango', 'Dgo.'),
(11, '11', 'Guanajuato', 'Gto.'),
(12, '12', 'Guerrero', 'Gro.'),
(13, '13', 'Hidalgo', 'Hgo.'),
(14, '14', 'Jalisco', 'Jal.'),
(15, '15', 'México', 'Mex.'),
(16, '16', 'Michoacán de Ocampo', 'Mich.'),
(17, '17', 'Morelos', 'Mor.'),
(18, '18', 'Nayarit', 'Nay.'),
(19, '19', 'Nuevo León', 'NL'),
(20, '20', 'Oaxaca', 'Oax.'),
(21, '21', 'Puebla', 'Pue.'),
(22, '22', 'Querétaro', 'Qro.'),
(23, '23', 'Quintana Roo', 'Q. Roo'),
(24, '24', 'San Luis Potosí', 'SLP'),
(25, '25', 'Sinaloa', 'Sin.'),
(26, '26', 'Sonora', 'Son.'),
(27, '27', 'Tabasco', 'Tab.'),
(28, '28', 'Tamaulipas', 'Tamps.'),
(29, '29', 'Tlaxcala', 'Tlax.'),
(30, '30', 'Veracruz de Ignacio de la Llave', 'Ver.'),
(31, '31', 'Yucatán', 'Yuc.'),
(32, '32', 'Zacatecas', 'Zac.');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `estados`
--
ALTER TABLE `estados`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `estados`
--
ALTER TABLE `estados`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
