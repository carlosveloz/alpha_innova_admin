<?php session_start();
require 'php/config/funciones.php';

// $conexion = conexion();

if (!empty($_SESSION)) {
  $statement = $conexion->prepare(
    'SELECT id FROM usuarios WHERE code = :code'
  );
  $statement->execute(array(
    ':code' => $_SESSION['code']
  ));

  $resultado = $statement->fetch();

  if (!empty($resultado)) {
  	header("Location: $root"."/app/dashboard");
  }
}


// require './vistas/layouts/navbar.vista.php';
require './vistas/login.vista.php';
// require './vistas/layouts/footer.vista.php';
 ?>