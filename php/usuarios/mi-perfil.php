<?php 

$conexion = conexion();


$statement = $conexion->prepare(
	'SELECT * FROM usuarios WHERE code = :code'
	);


$statement->execute(array(':code' => $_SESSION['code']));
$usuario = $statement->fetch();

require '../../vistas/layouts/navbar.vista.php';
require '../../vistas/layouts/sidebar.vista.php';
require '../../vistas/usuarios/mi-perfil.vista.php';
require '../../vistas/layouts/footer.vista.php';
 ?>