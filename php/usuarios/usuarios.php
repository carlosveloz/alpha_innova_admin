<?php 

$conexion = conexion();


$statement = $conexion->prepare(
	'SELECT * FROM usuarios WHERE status = 1'
	);
$statement->execute();
$usuarios_activos = $statement->fetchAll();

$statement = $conexion->prepare(
	'SELECT * FROM usuarios WHERE status = 0'
	);
$statement->execute();
$usuarios_inactivos = $statement->fetchAll();

require '../../vistas/layouts/navbar.vista.php';
require '../../vistas/layouts/sidebar.vista.php';
require '../../vistas/usuarios/usuarios.vista.php';
require '../../vistas/layouts/footer.vista.php';
 ?>