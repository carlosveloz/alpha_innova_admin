<?php 

$conexion = conexion();


$statement = $conexion->prepare(
	'SELECT 
	notas.*,
	plazas.nombre as plaza,
	autores.nombre as autor
	FROM notas
	LEFT JOIN plazas ON notas.plaza_id = plazas.id
	LEFT JOIN autores ON notas.autor_id = autores.id'
	);
$statement->execute();
$notas = $statement->fetchAll();

foreach ($notas as $key => $value) {
	$statement = $conexion->prepare(
	"SELECT ruta FROM notas_imagenes WHERE nota_id = :nota_id LIMIT 1"
	);
$statement->execute(array(':nota_id' => $value['id']));
$nota_img = $statement->fetch();

$notas[$key]['img'] = $nota_img['ruta'];
}


require '../../vistas/layouts/navbar.vista.php';
require '../../vistas/layouts/sidebar.vista.php';
require '../../vistas/noticias/listado.vista.php';
require '../../vistas/layouts/footer.vista.php';
 ?>