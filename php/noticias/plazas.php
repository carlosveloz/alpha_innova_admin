<?php 

$conexion = conexion();


$statement = $conexion->prepare(
	'SELECT * FROM plazas WHERE status = 1'
	);
$statement->execute();
$plazas_activas = $statement->fetchAll();

$statement = $conexion->prepare(
	'SELECT * FROM plazas WHERE status = 0'
	);
$statement->execute();
$plazas_inactivas = $statement->fetchAll();

require '../../vistas/layouts/navbar.vista.php';
require '../../vistas/layouts/sidebar.vista.php';
require '../../vistas/noticias/plazas.vista.php';
require '../../vistas/layouts/footer.vista.php';
 ?>