<?php 

$conexion = conexion();


$statement = $conexion->prepare(
	'SELECT 
	notas.*,
	plazas.nombre as plaza,
	autores.nombre as autor,
	autores.avatar as avatar
	FROM notas
	LEFT JOIN plazas ON notas.plaza_id = plazas.id
	LEFT JOIN autores ON notas.autor_id = autores.id
	WHERE notas.code = :code'
);
$statement->execute(array(':code' => $_GET['c']));
$nota = $statement->fetch();

$statement = $conexion->prepare(
	"SELECT * FROM notas_imagenes WHERE nota_id = :nota_id"
);
$statement->execute(array(':nota_id' => $nota['id']));
$nota_imgs = $statement->fetchAll();

$kw_es = explode(",", $nota['keywords_es']);
$kw_en = explode(",", $nota['keywords_en']);

$arr_etiquetas = explode(",",$nota['etiquetas']);


$etiquetas = array();
foreach ($arr_etiquetas as $key => $value) {
	if ($value != "") {
		$value = str_replace("'","",$value);
		$statement = $conexion->prepare(
			"SELECT * FROM etiquetas WHERE id = :id"
		);
		$statement->execute(array(':id' => $value));
		$et = $statement->fetch();

		array_push($etiquetas, $et);
	}
}



require '../../vistas/layouts/navbar.vista.php';
require '../../vistas/layouts/sidebar.vista.php';
require '../../vistas/noticias/detalles.vista.php';
require '../../vistas/layouts/footer.vista.php';
?>