<?php 

$conexion = conexion();


$statement = $conexion->prepare(
	'SELECT 
	historias.*,
	plazas.nombre as plaza,
	autores.nombre as autor
	FROM historias
	LEFT JOIN plazas ON historias.plaza_id = plazas.id
	LEFT JOIN autores ON historias.autor_id = autores.id'
	);
$statement->execute();
$historias = $statement->fetchAll();

foreach ($historias as $key => $value) {
	$statement = $conexion->prepare(
	"SELECT ruta FROM historias_imagenes WHERE nota_id = :nota_id LIMIT 1"
	);
$statement->execute(array(':nota_id' => $value['id']));
$nota_img = $statement->fetch();

$historias[$key]['img'] = $nota_img['ruta'];
}


require '../../vistas/layouts/navbar.vista.php';
require '../../vistas/layouts/sidebar.vista.php';
require '../../vistas/historias/listado.vista.php';
require '../../vistas/layouts/footer.vista.php';
 ?>