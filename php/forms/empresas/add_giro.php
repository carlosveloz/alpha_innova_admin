<?php 

require '../../config/funciones.php';
$conexion = conexion();
if (!$conexion) {
	die();
}



$respuesta="";

$statement = $conexion->prepare('SELECT id FROM empresas_giros WHERE nombre = :nombre LIMIT 1');
$statement->execute(array(
	':nombre' => $_POST['nombre'],
));
$resultado = $statement->fetch();

if ($resultado != false) {
	$respuesta = 'Ya existe un giro con este nombre.';
}


if ($respuesta == '') {

$slug = str_replace(" ","-",$_POST['nombre']);


	$statement = $conexion->prepare('INSERT INTO empresas_giros (nombre, slug, imagen) VALUES
		(:nombre, :slug, :imagen)');
	$statement->execute(array(
		':nombre' => $_POST['nombre'],
		':slug' => $slug,
		':imagen' => "default.png",
	));

	$new_id = $conexion->lastInsertId();

	$code = hash('md4', $new_id);

	$statement = $conexion->prepare('UPDATE empresas_giros set code = :code WHERE id = :id');
	$statement->execute(array(
		':code' => $code,
		':id' => $new_id			
	));



	if(!empty($new_id)){


				//Cargar imagen si existe
		$carpeta = "../../../images/empresas/giros/";
		if (!file_exists($carpeta)) {
			mkdir($carpeta, 0777, true);
		}

		foreach ($_FILES as $key) {
			if($key['error'] == UPLOAD_ERR_OK ) {

				$nombreOriginal = $key['name'];
				$temporal = $key['tmp_name'];
				$trozos = explode(".", $nombreOriginal);
				$extension = array_pop($trozos);
				$archivo = $slug.'.'.$extension;
				$destino = $carpeta.$archivo;

				move_uploaded_file($temporal, $destino);

				if($key['error'] == UPLOAD_ERR_OK ) {

					reduceImagen($destino,300);
					if(file_exists($destino)===true){
						$statement = $conexion->prepare('UPDATE empresas_giros SET imagen = :imagen 
							WHERE id = :id LIMIT 1');
						$statement->execute(array(
							':imagen'=>$archivo,
							':id' => $new_id
						));
						// $response->imagen = 'ok';
					}else{
						// $response->imagen = 'error';
					}
				}
			}
		}

		$respuesta = 'ok';
	}

	echo $respuesta;
}else{
	echo $respuesta;	
}



 ?>
