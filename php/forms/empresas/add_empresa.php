



<?php
require '../../config/funciones.php';
$conexion = conexion();
if (!$conexion) {
	die();
}


	

$giros = "";

if (!empty($_POST['giros'])) {
foreach ($_POST['giros'] as $key => $value) {
	$giros .= "'".$value."',";
}
}




$respuesta="";


// $statement = $conexion->prepare('SELECT id FROM notas WHERE (titulo_es = :titulo_es OR titulo_en = :titulo_en) AND status = 1 LIMIT 1');
// $statement->execute(array(
// 	':titulo_es' => $_POST['titulo_es'],
// 	':titulo_en' => $_POST['titulo_en']
// ));

$statement = $conexion->prepare('SELECT id FROM empresas WHERE nombre = :nombre AND status = 1 LIMIT 1');
$statement->execute(array(
	':nombre' => $_POST['nombre']
));
$resultado = $statement->fetch();

if ($resultado != false) {
	$respuesta = 'Ya existe una empresa activa con este nombre.';
}

if ($respuesta == '') {


	$slug = $_POST['nombre'];
	$slug = quitar_acentos($slug);
	$slug = strtolower($slug);
	$slug = preg_replace("/[^a-zA-Z0-9 -]+/", "", $slug);
	$slug = str_replace(" ","-",$slug);



	$statement = $conexion->prepare('INSERT INTO empresas (nombre, slogan, descripcion, logo, correo, telefono, estado_id, municipio_id, direccion, giros, url, facebook, twitter, instagram, linkedin,	slug, status, created_at) VALUES
		(:nombre, :slogan, :descripcion, :logo, :correo, :telefono, :estado_id, :municipio_id, :direccion, :giros, :url, :facebook, :twitter, :instagram, :linkedin, :slug, :status, :created_at)');
	$statement->execute(array(
	":nombre" => $_POST['nombre'],
	 ":slogan" => $_POST['slogan'],
	 ":descripcion" => $_POST['descripcion_emp'],
	 ":logo" => "logo.png",
	 ":correo" => $_POST['correo'],
	 ":telefono" => $_POST['telefono'],
	 ":estado_id" => $_POST['estado_id'],
	 ":municipio_id" => $_POST['municipio_id'],
	 ":direccion" => $_POST['direccion'],
	 ":giros" => $giros,
	 ":url" => $_POST['url'],
	 ":facebook" => $_POST['facebook'],
	 ":twitter" => $_POST['twitter'],
	 ":instagram" => $_POST['instagram'],
	 ":linkedin" => $_POST['linkedin'],
	 ":slug" => $slug,
	 ":status" => 1,
	 ":created_at" => $hoy
	));

	$new_id = $conexion->lastInsertId();



	$code = hash('md4', $new_id);

	$statement = $conexion->prepare('UPDATE empresas set code = :code WHERE id = :id');
	$statement->execute(array(
		':code' => $code,
		':id' => $new_id			
	));


		if(!empty($_FILES['logo'])){
				//Cargar imagen si existe
		$carpeta = "../../../images/empresas/empresas_logos/";
		if (!file_exists($carpeta)) {
			mkdir($carpeta, 0777, true);
		}

		foreach ($_FILES as $key) {
			if($key['error'] == UPLOAD_ERR_OK ) {

				$nombreOriginal = $key['name'];
				$temporal = $key['tmp_name'];
				$trozos = explode(".", $nombreOriginal);
				$extension = array_pop($trozos);
				$archivo = $slug.'.'.$extension;
				$destino = $carpeta.$archivo;

				move_uploaded_file($temporal, $destino);

				if($key['error'] == UPLOAD_ERR_OK ) {

					reduceImagen($destino,300);
					if(file_exists($destino)===true){
						$statement = $conexion->prepare('UPDATE empresas SET logo = :logo 
							WHERE id = :id LIMIT 1');
						$statement->execute(array(
							':logo'=>$archivo,
							':id' => $new_id
						));
						// $response->imagen = 'ok';
					}else{
						// $response->imagen = 'error';
					}
				}
			}
		}

		$respuesta = 'ok';
	}


if (isset($_POST['imagen'])) {
	# code...
	foreach ($_POST['imagen'] as $key => $value) {

		$statement = $conexion->prepare('SELECT ruta FROM empresas_imagenes WHERE id = :id');
		$statement->execute(array(
			':id' => $value
		));
		$img = $statement->fetch();

	

		$new_name = explode("/",$img['ruta']);
		$ext = explode(".",$new_name[2]);
		$extension = $ext[1];

	

		rename("../../../images/empresas/empresas_imagenes/".$img['ruta'],"../../../images/empresas/empresas_imagenes/".$new_name[0]."/".$new_name[1]."/".$slug."-".$value.".".$extension);


		$statement = $conexion->prepare('UPDATE empresas_imagenes set empresa_id = :empresa_id, ruta = :ruta, descripcion = :descripcion WHERE id = :id');
		$statement->execute(array(
			":empresa_id" => $new_id,
			":ruta" => $new_name[0]."/".$new_name[1]."/".$slug."-".$value.".".$extension,
			":descripcion" => $_POST['descripcion'][$key],
			":id" => $value
		));
	}
}
	

	$respuesta = "ok";

	echo $respuesta;
}else{
	echo $respuesta;	
}
?>
