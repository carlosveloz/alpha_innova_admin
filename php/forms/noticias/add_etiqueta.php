<?php
require '../../config/funciones.php';
$conexion = conexion();
if (!$conexion) {
	die();
}


$respuesta="";


$statement = $conexion->prepare('SELECT id FROM etiquetas WHERE nombre_es = :nombre_es OR nombre_en = :nombre_en LIMIT 1');
$statement->execute(array(
	':nombre_es' => $_POST['nombre_es'],
	':nombre_en' => $_POST['nombre_en']
));
$resultado = $statement->fetch();

if ($resultado != false) {
	$respuesta = 'Ya existe una etiqueta con este nombre.';
}

if ($respuesta == '') {


	$slug_es = $_POST['nombre_es'];
	$slug_es = quitar_acentos($slug_es);
	$slug_es = strtolower($slug_es);
	$slug_es = preg_replace("/[^a-zA-Z0-9 -]+/", "", $slug_es);
	$slug_es = str_replace(" ","-",$slug_es);


	$slug_en = $_POST['nombre_en'];
	$slug_en = quitar_acentos($slug_en);
	$slug_en = strtolower($slug_en);
	$slug_en = preg_replace("/[^a-zA-Z0-9 -]+/", "", $slug_en);
	$slug_en = str_replace(" ","-",$slug_en);




	$statement = $conexion->prepare('INSERT INTO etiquetas (nombre_es, nombre_en, slug_es, slug_en) VALUES
		(:nombre_es, :nombre_en, :slug_es, :slug_en)');
	$statement->execute(array(
		':nombre_es' => $_POST['nombre_es'],
		':nombre_en' => $_POST['nombre_en'],
		':slug_es' => $slug_es,
		':slug_en' => $slug_en
	));

	$new_id = $conexion->lastInsertId();

	$code = hash('md4', $new_id);

	$statement = $conexion->prepare('UPDATE etiquetas set code = :code WHERE id = :id');
	$statement->execute(array(
		':code' => $code,
		':id' => $new_id			
	));

	$respuesta = 'ok';

	echo $respuesta;
}else{
	echo $respuesta;	
}
?>
