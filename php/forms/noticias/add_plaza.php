<?php
require '../../config/funciones.php';
$conexion = conexion();
if (!$conexion) {
	die();
}


$respuesta="";


$statement = $conexion->prepare('SELECT id FROM plazas WHERE nombre = :nombre LIMIT 1');
$statement->execute(array(':nombre' => $_POST['nombre']));
$resultado = $statement->fetch();

if ($resultado != false) {
	$respuesta = 'Ya existe una plaza con este nombre.';
}

if ($respuesta == '') {


	$slug = $_POST['nombre'];
$slug = quitar_acentos($slug);
$slug = strtolower($slug);
$slug = preg_replace("/[^a-zA-Z0-9 -]+/", "", $slug);
$slug = str_replace(" ","-",$slug);




	$statement = $conexion->prepare('INSERT INTO plazas (nombre, descripcion_es, descripcion_en, keywords_es, keywords_en, slug) VALUES
		(:nombre, :descripcion_es, :descripcion_en, :keywords_es, :keywords_en, :slug)');
	$statement->execute(array(
		':nombre' => $_POST['nombre'],
		':descripcion_es' => $_POST['desc_es'],
		':descripcion_en' => $_POST['desc_en'],
		':keywords_es' => $_POST['keywords_es'],
		':keywords_en' => $_POST['keywords_en'],
		':slug' => $slug
	));

	$new_id = $conexion->lastInsertId();

	$code = hash('md4', $new_id);

	$statement = $conexion->prepare('UPDATE plazas set code = :code WHERE id = :id');
	$statement->execute(array(
		':code' => $code,
		':id' => $new_id			
	));



	if(!empty($new_id)){


				//Cargar imagen si existe
		$carpeta = "../../../images/plazas/";
		if (!file_exists($carpeta)) {
			mkdir($carpeta, 0777, true);
		}

		
			if($_FILES['img_es']['error'] == UPLOAD_ERR_OK ) {

				$nombreOriginal = $_FILES['img_es']['name'];
				$temporal = $_FILES['img_es']['tmp_name'];
				$trozos = explode(".", $nombreOriginal);
				$extension = array_pop($trozos);
				$archivo = $slug.'-es.'.$extension;
				$destino = $carpeta.$archivo;

				move_uploaded_file($temporal, $destino);

				if($_FILES['img_es']['error'] == UPLOAD_ERR_OK ) {

					reduceImagen($destino,500);
					if(file_exists($destino)===true){
						$statement = $conexion->prepare('UPDATE plazas SET img_es = :img_es 
							WHERE id = :id LIMIT 1');
						$statement->execute(array(
							':img_es'=>$archivo,
							':id' => $new_id
						));
						// $response->imagen = 'ok';
					}else{
						// $response->imagen = 'error';
					}
				}
			}
		



		if($_FILES['img_en']['error'] == UPLOAD_ERR_OK ) {

				$nombreOriginal = $_FILES['img_en']['name'];
				$temporal = $_FILES['img_en']['tmp_name'];
				$trozos = explode(".", $nombreOriginal);
				$extension = array_pop($trozos);
				$archivo = $slug.'-en.'.$extension;
				$destino = $carpeta.$archivo;

				move_uploaded_file($temporal, $destino);

				if($_FILES['img_en']['error'] == UPLOAD_ERR_OK ) {

					reduceImagen($destino,500);
					if(file_exists($destino)===true){
						$statement = $conexion->prepare('UPDATE plazas SET img_en = :img_en 
							WHERE id = :id LIMIT 1');
						$statement->execute(array(
							':img_en'=>$archivo,
							':id' => $new_id
						));
						// $response->imagen = 'ok';
					}else{
						// $response->imagen = 'error';
					}
				}
			}






		$respuesta = 'ok';
	}

	echo $respuesta;
}else{
	echo $respuesta;	
}
?>
