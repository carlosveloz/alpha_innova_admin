<?php
require '../../config/funciones.php';
$conexion = conexion();
if (!$conexion) {
	die();
}



$respuesta="";


$statement = $conexion->prepare('SELECT id FROM autores WHERE nombre = :nombre LIMIT 1');
$statement->execute(array(
	':nombre' => $_POST['nombre']
));
$resultado = $statement->fetch();

if ($resultado != false) {
	$respuesta = 'Ya existe un Autor con este nombre.';
}

if ($respuesta == '') {


	$statement = $conexion->prepare('INSERT INTO autores (nombre, avatar) VALUES
		(:nombre, :avatar)');
	$statement->execute(array(
		':nombre' => $_POST['nombre'],
		':avatar' => "avatar.png"
	));

	$new_id = $conexion->lastInsertId();

	$code = hash('md4', $new_id);

	$statement = $conexion->prepare('UPDATE autores set code = :code WHERE id = :id');
	$statement->execute(array(
		':code' => $code,
		':id' => $new_id			
	));




foreach ($_POST['plazas'] as $key => $value) {
	
	$statement = $conexion->prepare('INSERT INTO autores_plazas (autor_id, plaza_id) VALUES
		(:autor_id, :plaza_id)');
	$statement->execute(array(
		':autor_id' => $new_id,
		':plaza_id' => $value
	));
}



		$carpeta = "../../../images/autores/";
		if (!file_exists($carpeta)) {
			mkdir($carpeta, 0777, true);
		}

	$slug = $_POST['nombre'];
$slug = quitar_acentos($slug);
$slug = strtolower($slug);
$slug = preg_replace("/[^a-zA-Z0-9 -]+/", "", $slug);
$slug = str_replace(" ","-",$slug);


			if($_FILES['avatar']['error'] == UPLOAD_ERR_OK ) {

				$nombreOriginal = $_FILES['avatar']['name'];
				$temporal = $_FILES['avatar']['tmp_name'];
				$trozos = explode(".", $nombreOriginal);
				$extension = array_pop($trozos);
				$archivo = $slug.'.'.$extension;
				$destino = $carpeta.$archivo;

				move_uploaded_file($temporal, $destino);

				if($_FILES['avatar']['error'] == UPLOAD_ERR_OK ) {

					reduceImagen($destino,500);
					if(file_exists($destino)===true){
						$statement = $conexion->prepare('UPDATE autores SET avatar = :avatar 
							WHERE id = :id LIMIT 1');
						$statement->execute(array(
							':avatar'=>$archivo,
							':id' => $new_id
						));
						// $response->imagen = 'ok';
					}else{
						// $response->imagen = 'error';
					}
				}
			}




	$respuesta = 'ok';

	echo $respuesta;
}else{
	echo $respuesta;	
}
?>
