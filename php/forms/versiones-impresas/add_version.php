<?php 

require '../../config/funciones.php';
$conexion = conexion();
if (!$conexion) {
	die();
}


// print_r("POST:");
// print_r($_POST);

// if (!empty($_FILES)) {
// print_r("FILES:");
// print_r($_FILES);
// }else{
// echo "NO FILES DATA";
// }

// die;

$respuesta="";

$statement = $conexion->prepare('SELECT id FROM versiones_impresas WHERE plaza_id = :plaza_id AND edicion_mes = :edicion_mes AND edicion_ano = :edicion_ano LIMIT 1');
$statement->execute(array(
	':plaza_id' => $_POST['plaza_id'],
	':edicion_mes' => $_POST['edicion_mes'],
	':edicion_ano' => $_POST['edicion_ano'],
));
$resultado = $statement->fetch();

if ($resultado != false) {
	$respuesta = 'Ya existe una versión impresa con estos datos.';
}


if ($respuesta == '') {

	$statement = $conexion->prepare('INSERT INTO versiones_impresas (url, portada, edicion_mes, edicion_ano, plaza_id, status, created_at) VALUES
		(:url, :portada, :edicion_mes, :edicion_ano, :plaza_id, :status, :created_at)');
	$statement->execute(array(
		':url' => $_POST['url'],
		':portada' => "portada.png",
		':edicion_mes' => $_POST['edicion_mes'],
		':edicion_ano' => $_POST['edicion_ano'],
		':plaza_id' => $_POST['plaza_id'],
		':status' => 1,
		':created_at' => $hoy
	));

	$new_id = $conexion->lastInsertId();

	$code = hash('md4', $new_id);

	$statement = $conexion->prepare('UPDATE versiones_impresas set code = :code WHERE id = :id');
	$statement->execute(array(
		':code' => $code,
		':id' => $new_id			
	));



	if(!empty($new_id)){

$statement = $conexion->prepare('SELECT slug FROM plazas WHERE id = :plaza_id');
$statement->execute(array(
	':plaza_id' => $_POST['plaza_id']
));
$plaza = $statement->fetch();


				//Cargar imagen si existe
		$carpeta = "../../../images/versiones-impresas/";
		if (!file_exists($carpeta)) {
			mkdir($carpeta, 0777, true);
		}

		foreach ($_FILES as $key) {
			if($key['error'] == UPLOAD_ERR_OK ) {

				$nombreOriginal = $key['name'];
				$temporal = $key['tmp_name'];
				$trozos = explode(".", $nombreOriginal);
				$extension = array_pop($trozos);
				$archivo = $plaza['slug'].'-'.$_POST['edicion_mes'].'-'.$_POST['edicion_ano'].'-'.$new_id.'.'.$extension;
				$destino = $carpeta.$archivo;

				move_uploaded_file($temporal, $destino);

				if($key['error'] == UPLOAD_ERR_OK ) {

					reduceImagen($destino,300);
					if(file_exists($destino)===true){
						$statement = $conexion->prepare('UPDATE versiones_impresas SET portada = :portada 
							WHERE id = :id LIMIT 1');
						$statement->execute(array(
							':portada'=>$archivo,
							':id' => $new_id
						));
						// $response->imagen = 'ok';
					}else{
						// $response->imagen = 'error';
					}
				}
			}
		}

		$respuesta = 'ok';
	}

	echo $respuesta;
}else{
	echo $respuesta;	
}



 ?>
