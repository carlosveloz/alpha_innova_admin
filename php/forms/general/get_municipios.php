<?php 

require '../../config/funciones.php';
$conexion = conexion();
if (!$conexion) {
	die();
}

$conexion = conexion();


$statement = $conexion->prepare("
	SELECT * FROM municipios WHERE estado_id = :id"
);

$statement->execute(array(
	':id' => $_POST['estado_id']
));

$municipios = $statement->fetchAll();

$respuesta = "";

foreach ($municipios as $key => $value) {
	$respuesta .= '<option value='.$value['id'].'">'.$value['nombre'].'</option>';
}

echo $respuesta;
die;
?>