<?php 
require '../../config/funciones.php';
$conexion = conexion();
if (!$conexion) {
	die();
}



$statement = $conexion->prepare('INSERT INTO tareas (usuario_id, tarea) VALUES
	(:usuario_id, :tarea)');
$statement->execute(array(
	':usuario_id' => $_POST['usuario_id'],
	':tarea' => $_POST['tarea']
));

$new_id = $conexion->lastInsertId();

echo $new_id;


?>