<?php 
require '../../config/funciones.php';
$conexion = conexion();
if (!$conexion) {
	die();
}

$table = $_POST['table'];

$statement = $conexion->prepare("SELECT * FROM $table WHERE code = :code");
$statement->execute(array(
	':code' => $_POST['code']
));
$respuesta = $statement->fetch();

if ($table == "autores") {
	$statement = $conexion->prepare(
		'SELECT plazas.id AS plaza_id FROM plazas
		LEFT JOIN autores_plazas ON autores_plazas.plaza_id = plazas.id
		WHERE autores_plazas.autor_id = :id'
	);
	$statement->execute(array(":id" => $respuesta['id']));
	$plazas = $statement->fetchAll();

	$respuesta['plazas'] = $plazas;
}

$respuesta = json_encode($respuesta);

print_r($respuesta);
die;


?>