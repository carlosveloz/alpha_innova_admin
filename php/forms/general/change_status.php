<?php 

require '../../config/funciones.php';
$conexion = conexion();
if (!$conexion) {
	die();
}
/*
0 : Desactivar usuario
1: Activar usuario
2: Desactivar empresa
3: Activar empresa
4: Desactivar vendedor
5: Activar vendedor
*/


if ($_POST['type'] == 0) {
	$statement = $conexion->prepare("UPDATE usuarios set status = 0 WHERE code = :code");
	$statement->execute(array(
		':code' => $_POST['id']				
	));

}elseif ($_POST['type'] == 1) {
	$statement = $conexion->prepare("UPDATE usuarios set status = 1 WHERE code = :code");
	$statement->execute(array(
		':code' => $_POST['id']				
	));
}elseif ($_POST['type'] == 2) {
	$statement = $conexion->prepare("UPDATE encuestas set status = 0 WHERE code = :code");
	$statement->execute(array(
		':code' => $_POST['id']				
	));
}elseif ($_POST['type'] == 3) {
	$statement = $conexion->prepare("UPDATE encuestas set status = 1 WHERE code = :code");
	$statement->execute(array(
		':code' => $_POST['id']				
	));
}elseif ($_POST['type'] == 4) {
	$statement = $conexion->prepare("UPDATE etiquetas set status = 0 WHERE code = :code");
	$statement->execute(array(
		':code' => $_POST['id']				
	));
}elseif ($_POST['type'] == 5) {
	$statement = $conexion->prepare("UPDATE etiquetas set status = 1 WHERE code = :code");
	$statement->execute(array(
		':code' => $_POST['id']				
	));
}elseif ($_POST['type'] == 6) {
	$statement = $conexion->prepare("UPDATE plazas set status = 0 WHERE code = :code");
	$statement->execute(array(
		':code' => $_POST['id']				
	));
}elseif ($_POST['type'] == 7) {
	$statement = $conexion->prepare("UPDATE plazas set status = 1 WHERE code = :code");
	$statement->execute(array(
		':code' => $_POST['id']				
	));
}elseif ($_POST['type'] == 8) {
	$statement = $conexion->prepare("UPDATE empresas set status = 0 WHERE code = :code");
	$statement->execute(array(
		':code' => $_POST['id']				
	));
}elseif ($_POST['type'] == 9) {
	$statement = $conexion->prepare("UPDATE empresas set status = 1 WHERE code = :code");
	$statement->execute(array(
		':code' => $_POST['id']				
	));
}


echo "ok";
die;
?>


