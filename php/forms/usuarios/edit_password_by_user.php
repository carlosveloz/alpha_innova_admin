<?php 
session_start();

require '../../config/funciones.php';
$conexion = conexion();
if (!$conexion) {
	die();
}

if ($_POST['password'] != $_POST['password2']) {
	echo "Las contraseñas no coinciden.";
	die;
}

$password = hash('md4', $_POST['password']);

	$statement = $conexion->prepare("UPDATE usuarios set password = :password WHERE code = :code");
	$statement->execute(array(
		':password' => $password,			
		':code' => $_SESSION['code']			
	));


echo "ok";
die;
?>


