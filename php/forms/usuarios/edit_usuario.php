<?php 
session_start();

require '../../config/funciones.php';
$conexion = conexion();
if (!$conexion) {
	die();
}

$status = 0;
if (isset($_POST['status'])) {
	$status = 1;
}

	$statement = $conexion->prepare("UPDATE usuarios set nombre = :nombre, correo = :correo, perfil = :perfil, status = :status WHERE code = :code");
	$statement->execute(array(
		':nombre' => $_POST['nombre'],				
		':correo' => $_POST['correo'],				
		':perfil' => $_POST['perfil'],				
		':status' => $status,				
		':code' => $_POST['code']			
	));


echo "ok";
die;
?>


