<?php
require '../../config/funciones.php';
$conexion = conexion();
if (!$conexion) {
	die();
}


$respuesta="";
$password = hash('md4', $_POST['password']);


$statement = $conexion->prepare('SELECT id FROM usuarios WHERE correo = :correo LIMIT 1');
$statement->execute(array(':correo' => $_POST['correo']));
$resultado = $statement->fetch();

if ($resultado != false) {
	$respuesta = 'El correo ya esta en uso';
}

if ($respuesta == '') {

	$statement = $conexion->prepare('INSERT INTO usuarios (nombre, correo, password, avatar, perfil, created_at) VALUES
		(:nombre, :correo, :password,:avatar, :perfil, :created_at)');
	$statement->execute(array(
		':nombre' => $_POST['nombre'],
		':correo' => $_POST['correo'],
		':password' => $password,
		':avatar' => "avatar.png",
		':perfil' => $_POST['perfil'],
		':created_at' => $hoy
	));

	$new_id = $conexion->lastInsertId();

	$code = hash('md4', $new_id);

	$statement = $conexion->prepare('UPDATE usuarios set code = :code WHERE id = :id');
	$statement->execute(array(
		':code' => $code,
		':id' => $new_id			
	));



	if(!empty($new_id)){


				//Cargar imagen si existe
		$carpeta = "../../../images/avatars/";
		if (!file_exists($carpeta)) {
			mkdir($carpeta, 0777, true);
		}

		foreach ($_FILES as $key) {
			if($key['error'] == UPLOAD_ERR_OK ) {

				$nombreOriginal = $key['name'];
				$temporal = $key['tmp_name'];
				$trozos = explode(".", $nombreOriginal);
				$extension = array_pop($trozos);
				$archivo = str_pad($new_id,6,0,STR_PAD_LEFT).'.'.$extension;
				$destino = $carpeta.'us-'.$archivo;

				move_uploaded_file($temporal, $destino);

				if($key['error'] == UPLOAD_ERR_OK ) {

					reduceImagen($destino,300);
					if(file_exists($destino)===true){
						$statement = $conexion->prepare('UPDATE usuarios SET avatar = :avatar 
							WHERE id = :id LIMIT 1');
						$statement->execute(array(
							':avatar'=>'us-'.$archivo,
							':id' => $new_id
						));
						// $response->imagen = 'ok';
					}else{
						// $response->imagen = 'error';
					}
				}
			}
		}

		$respuesta = 'ok';
	}

	echo $respuesta;
}else{
	echo $respuesta;	
}
?>
