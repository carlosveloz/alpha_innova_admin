<?php
require '../../config/funciones.php';
$conexion = conexion();
if (!$conexion) {
	die();
}


	



$respuesta="";


$statement = $conexion->prepare('SELECT id FROM historias WHERE (titulo_es = :titulo_es OR titulo_en = :titulo_en) AND status = 1 LIMIT 1');
$statement->execute(array(
	':titulo_es' => $_POST['titulo_es'],
	':titulo_en' => $_POST['titulo_en']
));
$resultado = $statement->fetch();

if ($resultado != false) {
	$respuesta = 'Ya existe una nota activa con este titulo.';
}

if ($respuesta == '') {


	$slug_es = $_POST['titulo_es'];
	$slug_es = quitar_acentos($slug_es);
	$slug_es = strtolower($slug_es);
	$slug_es = preg_replace("/[^a-zA-Z0-9 -]+/", "", $slug_es);
	$slug_es = str_replace(" ","-",$slug_es);

	$slug_en = $_POST['titulo_en'];
	$slug_en = quitar_acentos($slug_en);
	$slug_en = strtolower($slug_en);
	$slug_en = preg_replace("/[^a-zA-Z0-9 -]+/", "", $slug_en);
	$slug_en = str_replace(" ","-",$slug_en);

	$statement = $conexion->prepare('INSERT INTO historias (titulo_es, titulo_en, slug_es, slug_en, keywords_es, keywords_en, contenido_es, contenido_en, plaza_id, autor_id, edicion_ano, edicion_mes, status, created_at) VALUES
		(:titulo_es, :titulo_en, :slug_es, :slug_en, :keywords_es, :keywords_en, :contenido_es, :contenido_en, :plaza_id, :autor_id, :edicion_ano, :edicion_mes, 1, :created_at)');
	$statement->execute(array(
		":titulo_es" => $_POST['titulo_es'],
		":titulo_en" => $_POST['titulo_en'],
		":slug_es" => $slug_es,
		":slug_en" => $slug_en,
		":keywords_es" => $_POST['keywords_es'],
		":keywords_en" => $_POST['keywords_en'],
		":contenido_es" => $_POST['contenido_es'],
		":contenido_en" => $_POST['contenido_en'],
		":plaza_id" => $_POST['plaza_id'],
		":autor_id" => $_POST['autor_id'],
		":edicion_ano" => $_POST['edicion_ano'],
		":edicion_mes" => $_POST['edicion_mes'],
		":created_at" => $hoy
	));

	$new_id = $conexion->lastInsertId();

	$code = hash('md4', $new_id);

	$statement = $conexion->prepare('UPDATE historias set code = :code WHERE id = :id');
	$statement->execute(array(
		':code' => $code,
		':id' => $new_id			
	));

	foreach ($_POST['imagen'] as $key => $value) {

		$statement = $conexion->prepare('SELECT ruta FROM historias_imagenes WHERE id = :id');
		$statement->execute(array(
			':id' => $value
		));
		$img = $statement->fetch();

	

		$new_name = explode("/",$img['ruta']);
		$ext = explode(".",$new_name[2]);
		$extension = $ext[1];

		


		rename("../../../images/historias/".$img['ruta'],"../../../images/historias/".$new_name[0]."/".$new_name[1]."/".$slug_es."-".$value.".".$extension);


		$statement = $conexion->prepare('UPDATE historias_imagenes set nota_id = :nota_id, ruta = :ruta, descripcion_es = :descripcion_es, descripcion_en = :descripcion_en WHERE id = :id');
		$statement->execute(array(
			":nota_id" => $new_id,
			":ruta" => $new_name[0]."/".$new_name[1]."/".$slug_es."-".$value.".".$extension,
			":descripcion_es" => $_POST['descripcion_es'][$key],
			":descripcion_en" => $_POST['descripcion_es'][$key],
			":id" => $value
		));
	}

	$respuesta = 'ok';

	echo $respuesta;
}else{
	echo $respuesta;	
}
?>
